//
//  UtiHelper.swift
//  Keanu
//
//  Created by Benjamin Erhart on 05.03.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import MobileCoreServices

public class UtiHelper {

    public static let typeText = kUTTypeText as String
    public static let typeUrl = kUTTypeURL as String
    public static let typeImage = kUTTypeImage as String
    public static let typeAudio = kUTTypeAudio as String
    public static let typeMovie = kUTTypeMovie as String
    public static let typeData = kUTTypeData as String

    public static let defaultMimeType = "application/octet-stream"
    public static let jpegMimeType = mimeType(from: kUTTypeJPEG as String)

    /**
     Converts a UTI into a MIME type.

     Defaults to "application/octet-stream", if unascertainable.

     If you provide nil as uti, it will return the default MIME type, too.

     - parameter uti: The Uniform Type Identifier, optional.
     - returns: The according MIME type, the default, if none available.
    */
    public class func mimeType(from uti: String?) -> String {
        if let uti = uti,
            let mimeType = UTTypeCopyPreferredTagWithClass(uti as CFString, kUTTagClassMIMEType)?
            .takeRetainedValue() {

            return mimeType as String
        }

        return defaultMimeType
    }

    /**
     Returns the UTI of the file at the given URL.

     - parameter url: A file URL.
     - returns: an UTI or nil.
    */
    public class func uti(for url: URL) -> String? {
        return (try? url.resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }

    /**
     Returns the MIME type of the file at the given URL.

     - parameter url: A file URL.
     - returns: the MIME type of the given file.
     */
    public class func mimeType(for url: URL) -> String {
        return mimeType(from: uti(for: url))
    }
}
