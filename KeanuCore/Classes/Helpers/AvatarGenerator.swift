//
//  AvatarGenerator.swift
//  Keanu
//
//  Created by N-Pex on 12.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit

open class AvatarGenerator {
    
    static let arrayOfColors:[CGColor] = [
        UIColor(hexString: "#fff73d54").cgColor,
        UIColor(hexString: "#fffff74f").cgColor,
        UIColor(hexString: "#ffb2142f").cgColor,
        UIColor(hexString: "#ff4fcaff").cgColor,
        UIColor(hexString: "#ff86ff76").cgColor,
        UIColor(hexString: "#ffcc4317").cgColor,
        UIColor(hexString: "#ff8376ff").cgColor
    ]

    public static func roomAvatarImage(id: String, displayName: String, width: Int, height: Int) -> UIImage? {
        return AvatarGenerator.avatarImage(withBackgroundSeed: id, character: displayName.first, width: width, height: height)
    }

    public static func avatarImage(displayName: String, width: Int, height: Int) -> UIImage? {
        return AvatarGenerator.avatarImage(withBackgroundSeed: nil, character: displayName.first, width: width, height: height)
    }

    private static func avatarImage(withBackgroundSeed seed: String?, character:Character?, width: Int, height: Int) -> UIImage? {
        guard width > 0, height > 0 else {return nil}
        UIGraphicsBeginImageContext(CGSize(width: width, height: height))
        if let ctx = UIGraphicsGetCurrentContext() {
            ctx.interpolationQuality = CGInterpolationQuality.high
            
            // If a background seed is given, use that to create a pseudo-random background.
            if let seed = seed {
                // Create a pseudo-random random number generator and seed it with the identifier
                // hash. This will ensure that we get the same image every time we call it with
                // the same identifier!
                let generator = LinearCongruentialGenerator(seed: seed.javaHash())
                let range = 0.3 * Double(height)
                let halfrange = Int(range / 2)
                let a = Int(generator.random() * range) - halfrange
                let b = Int(generator.random() * range) - halfrange
                let c = Int(generator.random() * range) - halfrange
                let d = Int(generator.random() * range) - halfrange
                
                let colorTop = arrayOfColors[Int(generator.random() *  Double(arrayOfColors.count))]
                let colorMiddle = arrayOfColors[Int(generator.random() * Double(arrayOfColors.count))]
                let colorBottom = arrayOfColors[Int(generator.random() * Double(arrayOfColors.count))]
                
                
                // Middle part (just fill all)
                ctx.setFillColor(colorMiddle)
                ctx.fill(CGRect(x: 0, y: 0, width: width, height: height))
                
                let aThird = Int(0.33 * Double(height))
                let twoThirds = Int(0.66 * Double(height))
                
                // Top third
                ctx.setFillColor(colorTop)
                ctx.move(to: CGPoint(x: 0, y: 0))
                ctx.addLine(to: CGPoint(x: 0, y: aThird + a))
                ctx.addLine(to: CGPoint(x: width, y: aThird + c))
                ctx.addLine(to: CGPoint(x: width, y: 0))
                ctx.addLine(to: CGPoint(x: 0, y: 0))
                ctx.drawPath(using: .fill)
                
                // Bottom third
                ctx.setFillColor(colorBottom)
                ctx.move(to: CGPoint(x: 0, y: twoThirds + b))
                ctx.addLine(to: CGPoint(x: 0, y: height))
                ctx.addLine(to: CGPoint(x: width, y: height))
                ctx.addLine(to: CGPoint(x: width, y: twoThirds + d))
                ctx.addLine(to: CGPoint(x: 0, y: twoThirds + b))
                ctx.drawPath(using: .fill)
            }
            
            // Draw the character, if given
            if let char = character {
                let label = UILabel()
                label.textColor = UIColor.white
                label.textAlignment = .center
                if seed == nil {
                    let lightGray = UIColor(hue: 240.0/360.0, saturation: 0.02, brightness: 0.92, alpha: 1.0)
                    label.backgroundColor = lightGray
                    label.textColor = UIColor.lightGray
                }
                label.frame = CGRect(x: 0, y: 0, width: width, height: height)
                label.font = UIFont.boldSystemFont(ofSize: CGFloat(height) / 1.6)
                label.text = String(char)
                label.layer.render(in: ctx)
            }
        }
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage
    }
    
    // A LCG to create pseudo-random numbers (using a seed)
    //
    // Inspired by: https://stackoverflow.com/questions/24027216/how-do-you-generate-a-random-number-in-swift
    class LinearCongruentialGenerator {
        var lastRandom = 0.0
        let m = 139968.0
        let a = 3877.0
        let c = 29573.0
        
        public init(seed: Int) {
            let doubleSeed = abs(Double(seed))
            self.lastRandom = ((doubleSeed * a + c).truncatingRemainder(dividingBy: m))
        }
        
        public func random() -> Double {
            lastRandom = ((lastRandom * a + c).truncatingRemainder(dividingBy: m))
            return lastRandom / m
        }
        
        public func random(at: Int) -> Double {
            var ret:Double = 0
            for _ in 0..<at {
                ret = random()
            }
            return ret
        }
    }
}

// An extension to return a java-compatible hash of a string
// See for example: http://grepcode.com/file/repository.grepcode.com/java/root/jdk/openjdk/6-b14/java/lang/String.java#String.hashCode%28%29
extension String {
    func javaHash() -> Int {
        var hash:Int32 = 0
        for char in self.utf16 {
            let lhs: Int32 = Int32(31).multipliedReportingOverflow(by: hash).0
            let rhs: Int32 = Int32(UInt(char))
            hash = lhs.addingReportingOverflow(rhs).0
        }
        return Int(hash)
    }
}
