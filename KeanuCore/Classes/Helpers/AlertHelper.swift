//
//  AlertHelper.swift
//  Keanu
//
//  Created by Benjamin Erhart on 08.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit

public class AlertHelper {

    public typealias ActionHandler = (UIAlertAction) -> Void

    /**
     Creates and immediately presents a `UIAlertController`.
     Alert style is `.alert`, presentation will be animated.

     if `actions` is ommitted, it will have one action of type `.default` labeled "OK".

     - parameter controller: The `UIViewController` to present on.
     - parameter message: The alert message. Optional.
     - parameter title: The alert title. Optional, defaults to localized "Error".
     - parameter style: The alert style, defaults to `.alert`.
     - parameter actions: A list of actions. Optional, defaults to one localized "OK" default action.
    */
    public class func present(_ controller: UIViewController,
                       message: String? = nil,
                       title: String? = "Error".localize(),
                       style: UIAlertController.Style = .alert,
                       actions: [UIAlertAction]? = [defaultAction()]) {

        controller.present(build(message: message, title: title, style: style, actions: actions),
                           animated: true)
    }

    /**
     Creates a `UIAlertController`.
     Alert style is `.alert`.

     if `actions` is ommitted, it will have one action of type `.default` labeled "OK".

     - parameter message: The alert message. Optional.
     - parameter title: The alert title. Optional, defaults to localized "Error".
     - parameter actions: A list of actions. Optional, defaults to one localized "OK" default action.
     */
    public class func build(message: String? = nil,
                     title: String? = "Error".localize(),
                     style: UIAlertController.Style = .alert,
                     actions: [UIAlertAction]? = [defaultAction()]) -> UIAlertController {

        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: style)

        for action in actions ?? [] {
            alert.addAction(action)
        }

        return alert
    }
    
    /**
     - parameter title: The action's title. Optional, defaults to localized "OK".
     - parameter handler: The callback when the user tapped the action.
     - returns: A default `UIAlertAction`.
     */
    public class func defaultAction(_ title: String? = "OK".localize(),
                                    handler: ActionHandler? = nil) -> UIAlertAction {
        return UIAlertAction(title: title, style: .default, handler: handler)
    }

    /**
     - parameter title: The action's title. Optional, defaults to localized "Cancel".
     - parameter handler: The callback when the user tapped the action. Optional.
     - returns: A cancel `UIAlertAction`.
     */
    public class func cancelAction(_ title: String? = "Cancel".localize(),
                                   handler: ActionHandler? = nil) -> UIAlertAction {
        return UIAlertAction(title: title, style: .cancel, handler: handler)
    }

    /**
     - parameter title: The action's title.
     - parameter handler: The callback when the user tapped the action.
     - returns: A destructive `UIAlertAction`.
     */
    public class func destructiveAction(_ title: String?,
                                        handler: ActionHandler? = nil) -> UIAlertAction {
        return UIAlertAction(title: title, style: .destructive, handler: handler)
    }


    /**
     Add a `UITextField` to a given `UIAlertController`.

     - parameter alert: The alert controller to add the text field to.
     - parameter placeholder: The placeholder string. Defaults to `nil`.
     - parameter text: The text content to begin with. Defaults to `nil`.
     - parameter configurationHandler: A block for configuring the text field
        further. This block has no return value and takes
        a single parameter corresponding to the text field object.
        Use that parameter to change the text field properties.
    */
    public class func addTextField(
        _ alert: UIAlertController, placeholder: String? = nil,
        text: String? = nil, configurationHandler: ((UITextField) -> Void)? = nil) {

        alert.addTextField() { textField in
            textField.clearButtonMode = .whileEditing
            textField.placeholder = placeholder
            textField.text = text

            configurationHandler?(textField)
        }
    }

    /**
     Add a `UITextField` in password entry style to a given `UIAlertController`.

     - parameter alert: The alert controller to add the text field to.
     - parameter placeholder: The placeholder string. Defaults to `nil`.
     - parameter text: The text content to begin with. Defaults to `nil`.
     - parameter configurationHandler: A block for configuring the text field
         further. This block has no return value and takes
         a single parameter corresponding to the text field object.
         Use that parameter to change the text field properties.
     */
    public class func addPasswordField(
        _ alert: UIAlertController, placeholder: String? = nil,
        text: String? = nil, configurationHandler: ((UITextField) -> Void)? = nil) {

        addTextField(alert, placeholder: placeholder, text: text) { textField in
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.keyboardType = .asciiCapable
            textField.isSecureTextEntry = true

            configurationHandler?(textField)
        }
    }


    // MARK: Specialized Alerts

    /**
     Presents a specialized alert with an input field to edit a name of a friend
     or the user's own name.

     The name gets trimmed. An empty name cannot be successfully used. If the
     user tried that, an additional error alert will be displayed and the
     success callback **will not be called**.

     - parameter controller: The `UIViewController` to present on.
     - parameter name: The original name to edit.
     - parameter success: Callback, when user entered a valid name.
     - parameter newName: The valid new name for the user.
    */
    public class func presentEditNameAlert(_ controller: UIViewController, _ name: String,
                                    success: @escaping (_ newName: String) -> ()) {
        
        let alert = build(title: "Edit Name".localize(),
                          actions: [cancelAction()])

        alert.addAction(defaultAction() { action in
            if let newName = alert.textFields?.first?.text?.trimmingCharacters(in: .whitespacesAndNewlines),
                !newName.isEmpty {

                success(newName)
            }
            else {
                present(controller, message:
                    "Please don't use an empty name or a name consisting only of white space!"
                        .localize())
            }
        })

        addTextField(alert, placeholder: "Name".localize(), text: name)

        controller.present(alert, animated: true)
    }
}
