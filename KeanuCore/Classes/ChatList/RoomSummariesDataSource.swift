//
//  RoomSummariesDataSource.swift
//  Keanu
//
//  Created by N-Pex on 01.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixKit

/**
 Data source for all room summaries.
 
 Basically this handles an array of MXRoomSummary objects. The data source handles all
 underlying logic of listening to MX session changes, room adds/removes etc.
 
 Use the delegate to customize cell rendering and table view animations.
 */
open class RoomSummariesDataSource: NSObject, UITableViewDataSource {
    
    public var delegate: RoomSummariesDataSourceDelegate?
    
    // The data that is actually displayed in the table view cells.
    var roomSummaries: [MXRoomSummary] = []
    
    // Notification center listener objects
    var observers: [NSObjectProtocol] = []
    
    public override init() {
        super.init()

        let nc = NotificationCenter.default
        
        observers.append(nc.addObserver(forName: .mxRoomSummaryDidChange, object: nil, queue: .main) { (notification) in
            
            guard let delegate = self.delegate else {return}
            if let updatedSummary = notification.object as? MXRoomSummary,
                let roomSummaryIndex = self.roomSummaries.firstIndex(of: updatedSummary) {
                delegate.didChangeRows(at: [IndexPath(row: roomSummaryIndex, section: 0)])
            } else {
                // Nil for "all summaries"
                delegate.didChangeAllRows()
            }
        })
        
        observers.append(nc.addObserver(forName: .mxSessionNewRoom, object: nil, queue: .main) { (notification) in
            if let session = notification.object as? MXSession, let roomId = notification.userInfo?[kMXSessionNotificationRoomIdKey] as? String, let room = session.room(withRoomId: roomId) {
                
                // Add the room on main thread and call didChangeallRows to update table
                DispatchQueue.main.async {
                    self.onAddRoomSummary(room.summary)
                    self.resort()
                    if let delegate = self.delegate {
                        delegate.didChangeAllRows()
                    }
                }
            }
        })
        
        observers.append(nc.addObserver(forName: .mxSessionDidLeaveRoom, object: nil, queue: .main) { (notification) in
            if let roomId = notification.userInfo?[kMXSessionNotificationRoomIdKey] as? String {
                
                // Remove the room on main thread and call didChangeallRows to update table
                DispatchQueue.main.async {
                    self.onRemoveRoomSummary(roomId)
                    self.resort()
                    if let delegate = self.delegate {
                        delegate.didChangeAllRows()
                    }
                }
            }
        })
        
        observers.append(nc.addObserver(forName: .mxSessionDirectRoomsDidChange, object: nil, queue: .main) { (notification) in
            //TODO
            print("change")
        })
        
        observers.append(nc.addObserver(forName: .mxSessionStateDidChange, object: nil, queue: .main) { (notification) in
            //TODO
            if let session = notification.object as? MXSession {
                DispatchQueue.main.async {
                    self.addSessionRoomSummaries(session)
                    self.resort()
                    if let delegate = self.delegate {
                        delegate.didChangeAllRows()
                    }
                }
            }
        })
        
        // Add all current sessions
        reload()
    }
    
    deinit {
        observers.forEach({ NotificationCenter.default.removeObserver($0) })
        observers.removeAll()
    }
    
    public func reload() {
        roomSummaries.removeAll()
        if let accounts = MXKAccountManager.shared()?.activeAccounts {
            for account in accounts {
                if let session = account.mxSession {
                    addSessionRoomSummaries(session)
                }
            }
        }
        resort()
        if let delegate = self.delegate {
            delegate.didChangeAllRows()
        }
    }

    /**
     Add all room summaries for this session (if not already added)
     */
    open func addSessionRoomSummaries(_ session: MXSession) {
        for roomSummary in session.roomsSummaries() {
            onAddRoomSummary(roomSummary)
        }
    }
    
    open func onAddRoomSummary(_ roomSummary: MXRoomSummary) {
        // Never show room that should be invisible
        guard !roomSummary.hiddenFromUser else { return }
        guard !roomSummary.isConferenceUserRoom else { return }
        
        if !self.roomSummaries.contains(roomSummary) {
            self.roomSummaries.append(roomSummary)
        }
    }
    
    open func onRemoveRoomSummary(_ roomId: String) {
        for i in 0..<self.roomSummaries.count {
            let roomSummary = self.roomSummaries[i]
            if roomId == roomSummary.roomId {
                self.roomSummaries.remove(at: i)
                break
            }
        }
    }
    
    /**
     For all sessions, order invites at the top, and otherwise by last message's timestamp.
     */
    open func resort() {
        roomSummaries.sort { (room1, room2) -> Bool in
            if room1.isInvite, !room2.isInvite {
                return true
            } else if !room1.isInvite, room2.isInvite {
                return false
            }
            return room1.lastMessageOriginServerTs > room2.lastMessageOriginServerTs
        }
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return roomSummaries.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let delegate = self.delegate, let cell = delegate.createCell(at: indexPath, for: roomSummaries[indexPath.row]) {
            return cell
        }
        return UITableViewCell() // Fallback to avoid crash
    }
    
    public func roomSummary(at indexPath: IndexPath) -> MXRoomSummary? {
        if indexPath.section == 0, indexPath.row >= 0, indexPath.row < roomSummaries.count {
            return roomSummaries[indexPath.row]
        }
        return nil
    }
}


