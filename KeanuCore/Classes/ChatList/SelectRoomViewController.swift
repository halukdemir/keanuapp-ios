//
//  SelectRoomViewController.swift
//  KeanuCore
//
//  Created by N-Pex on 09.05.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK

/**
 Scene showing the current list of rooms.
 
 When a room is selected, call the given callback func. If canceled, the same callback is called with a "nil" room. It is the responsibility of the callback owner to dismiss the view controller.
 */
open class SelectRoomViewController: UITableViewController, RoomSummariesDataSourceDelegate {
    
    open var dataSource: RoomSummariesDataSource?
    open var callback: ((SelectRoomViewController, MXRoom?) -> Void)
    
    public init(callback: @escaping ((SelectRoomViewController, MXRoom?) -> Void)) {
        self.callback = callback
        super.init(style: .plain)
    }
    
    required public init?(coder: NSCoder) {
        return nil // Not supported
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel, target: self, action: #selector(done))
        
        navigationItem.title = "Chats".localize()
        
        // Register cell types
        tableView.register(AvatarAnd3LabelsCell.nib, forCellReuseIdentifier: AvatarAnd3LabelsCell.defaultReuseId)
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        dataSource = RoomSummariesDataSource()
        dataSource?.delegate = self
        tableView.dataSource = dataSource
        
        super.viewWillAppear(animated)
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        dataSource = nil
        tableView.dataSource = nil
    }
    
    
    // MARK: RoomSummariesDataSourceDelegate
    
    public func createCell(at indexPath: IndexPath, for roomSummary: MXRoomSummary) -> UITableViewCell? {
        if let chatListCell = tableView.dequeueReusableCell(withIdentifier:
            AvatarAnd3LabelsCell.defaultReuseId, for: indexPath) as? AvatarAnd3LabelsCell {
            return chatListCell.apply(roomSummary)
        }
        
        return nil
    }
    
    public func didChangeAllRows() {
        tableView.reloadData()
    }
    
    public func didChangeRows(at indexPaths:[IndexPath]) {
        tableView.reloadData()
    }
    
    
    // MARK: Actions
    
    /**
     Cancel, call the given callback with a nil room
     */
    @objc func done() {
        callback(self, nil)
    }
    
    
    // MARK: UITableViewDelegate
    
    open override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return AvatarAnd3LabelsCell.height
    }
    
    open override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let room = dataSource?.roomSummary(at: indexPath)?.room
        callback(self, room)
    }
}
