//
//  RoomSummariesDataSourceDelegate.swift
//  Keanu
//
//  Created by N-Pex on 28.09.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import MatrixSDK

public protocol RoomSummariesDataSourceDelegate {
    
    /**
     Create a table view cell for this room summary object.
     
     - parameter indexPath: The IndexPath of the object in the table view.
     - parameter roomSummary: The MXRoomSummary object to display.
     */
    func createCell(at indexPath: IndexPath, for roomSummary: MXRoomSummary) -> UITableViewCell?

    /**
     Called when all rows changed.

     You should reload your table/collection etc.
    */
    func didChangeAllRows()

    /**
     Called, when certain rows changed.

     You should reload your table/collection etc.

     - parameter indexPaths: The changed rows.
     */
    func didChangeRows(at indexPaths:[IndexPath])
}
