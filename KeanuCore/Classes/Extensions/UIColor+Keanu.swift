//
//  UIColor+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 12.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit

extension UIColor {

    /**
     Create a UIColor object from a color hex string in the format `#aarrggbb`.

     ATTENTION: The first character will be ignored, so provide a leading "#"!

     - parameter hexString: The hexadecimal color code.
     */
    public convenience init(hexString: String) {
        let scanner = Scanner(string: hexString)
        scanner.scanLocation = 1

        var rgb: UInt32 = 0

        guard scanner.scanHexInt32(&rgb) else {
            self.init(white: 1, alpha: 1)
            return
        }

        let a = CGFloat((rgb & 0xFF000000) >> 24) / 255
        let r = CGFloat((rgb & 0x00FF0000) >> 16) / 255
        let g = CGFloat((rgb & 0x0000FF00) >> 8) / 255
        let b = CGFloat(rgb & 0x000000FF) / 255

        self.init(red: r, green: g, blue: b, alpha: a)
    }

    /**
     Creates an image filled with this color.

     - parameter size: The size of the created image.
     - returns: A `UIImage` filled with this color.
    */
    public func asImage(size: CGSize = CGSize(width: 1, height: 1)) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)

        setFill()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))

        let image = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()

        return image
    }
}
