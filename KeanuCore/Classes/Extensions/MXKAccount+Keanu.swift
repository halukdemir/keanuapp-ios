//
//  MXKAccount+Keanu.swift
//  Keanu
//
//  Created by Benjamin Erhart on 30.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import MatrixKit

public enum KeanuAccountError: Error {
    case runtimeError(String)
}

extension MXKAccount {

    /**
     Shortcut for `mxCredentials?.userId` with fallback to empty string.
     */
    public var matrixId: String {
        get {
            return mxCredentials?.userId ?? ""
        }
    }

    /**
     The `userDisplayName` if not empty or the local part of the `matrixId`.
     */
    public var friendlyName: String {
        get {
            return Friend.getFriendlyName(userDisplayName, matrixId)
        }
    }
    
    //MARK: Syntactic sugar - performOnSessionReady
    
    /**
     Convenience wrapper to execute a block when session reaches "ready" state. If session is not in that state, an observer is added waiting for it to reach the ready state. In the mean time the callback and observer is stored in an associated object.
     */
    public func performOnSessionReady(_ callback:@escaping sessionReadyCallbackFunc) throws {
        guard let session = mxSession else { throw KeanuAccountError.runtimeError("mxSession needs to be set before calling this func!") }
        if session.state == MXSessionStateRunning {
            if let observer = sessionObserver {
                NotificationCenter.default.removeObserver(observer)
                sessionObserver = nil
            }
            callback(session)
        } else {
            let observer = NotificationCenter.default.addObserver(forName: Notification.Name.mxSessionStateDidChange, object: session, queue: OperationQueue.main) { (notification) in
                guard let session = notification.object as? MXSession else { return }
                if session.state == MXSessionStateRunning {
                    if let observerInfo = self.sessionObserver {
                        NotificationCenter.default.removeObserver(observerInfo.0)
                        observerInfo.1(session)
                        self.sessionObserver = nil
                    }
                }
            }
            self.sessionObserver = (observer, callback)
        }
    }
    
    private struct AssociatedKeys {
        static var sessionObserver = "sessionObserver"
    }
    
    public typealias sessionReadyCallbackFunc = (MXSession) -> Void
    private typealias sessionReadyCallbackInfo = (NSObjectProtocol, sessionReadyCallbackFunc)
    
    /**
     Associated property to store information about state observer/callback, see performOnSessionReady.
     */
    private var sessionObserver: sessionReadyCallbackInfo? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.sessionObserver) as? sessionReadyCallbackInfo
        }
        set(newValue) {
            objc_setAssociatedObject(self, &AssociatedKeys.sessionObserver, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
