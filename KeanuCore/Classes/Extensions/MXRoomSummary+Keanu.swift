//
//  MXRoomSummary+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 26.09.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import MatrixSDK
import MatrixKit

extension MXRoomSummary {

    /**
     Get the display name for a room. Default to room name or topic, but lacking
     this try to build a display name from the members in the room (or the one
     we are chatting with in the case of a one-to-one chat).
     */
    public var friendlyDisplayName: String {
        get {
            if let displayName = displayname, !displayName.isEmpty {
                return displayName
            }
            
            // TODO: Build a room name from members...
            return "TODO: Build a room name from members..."
        }
    }

    public var isInvite: Bool {
        get {
            return membership == .invite
        }
    }
    
    public var isArchived: Bool {
        get {
            return (others["isArchive"] as? NSNumber)?.boolValue ?? false
        }
        set(newValue) {
            if newValue {
                others["isArchive"] = NSNumber(booleanLiteral: true)
            } else {
                others.removeObject(forKey: "isArchive")
            }
            self.save(true)
        }
    }
    
    /**
     Return a formatted version of the last message. Clients can set a static delegate to do the actual formatting, see lastMessageFormatter.
     */
    public var lastMessageStringFormatted: String? {
        get {
            // If a result is returned from the formatter, use that. Otherwise use default handling.
            if let formatter = MXRoomSummary.lastMessageFormatter, let result = formatter(self) {
                return result
            }
            if let lastEvent = self.lastMessageEvent,
                lastEvent.isMediaAttachment(),
                let attachment = MXKAttachment(event: lastEvent, andMediaManager: mxSession.mediaManager) {
                if attachment.type == MXKAttachmentTypeVideo {
                    return "Video file".localize()
                } else if attachment.type == MXKAttachmentTypeImage {
                    return "Image file".localize()
                } else if attachment.isPdf {
                    return "PDF".localize()
                } else if let mimeType = attachment.contentInfo?["mimetype"] as? String, mimeType.starts(with: "audio/") {
                    if mimeType.contains("x-m4a") {
                        return "Voice message".localize()
                    } else {
                        return "Audio file".localize()
                    }
                } else if attachment.type == MXKAttachmentTypeFile {
                    return attachment.fileDisplayName ?? self.lastMessageString
                }
            }
            return self.lastMessageString
        }
    }
    
    private struct AssociatedKeys {
        static var lastMessageFormatter = "lastMessageFormatter"
    }

    /**
     Set a formatter to use for formatting the "last message", e.g. in a list of chats. If this callback returns a non-nil value, then use that. Otherwise, fall back on default functionality.
     */
    public static var lastMessageFormatter: ((MXRoomSummary)->String?)? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.lastMessageFormatter) as? ((MXRoomSummary)->String?)
        }
        set(newValue) {
            guard let newValue = newValue else { return }
            objc_setAssociatedObject(self, &AssociatedKeys.lastMessageFormatter, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
