//
//  Bundle+displayName.swift
//  Keanu
//
//  Created by Benjamin Erhart on 01.10.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import Foundation

public extension Bundle {

    var displayName: String {
        return object(forInfoDictionaryKey: "CFBundleDisplayName") as? String
            ?? object(forInfoDictionaryKey: kCFBundleNameKey as String) as? String
            ?? ""
    }
}
