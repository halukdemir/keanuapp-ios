//
//  MXEvent+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 15.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import MatrixSDK

extension MXEvent {
    
    /**
     Returns a list of unknown devices that cause this event to fail, if any. Returns nil if the event is not a failed sent event or if it failed because of other reason.
     */
    public var sentFailedUnknownDevices: MXUsersDevicesMap<MXDeviceInfo>? {
        if sentState == MXEventSentStateFailed,
            let error = sentError as NSError?,
            error.domain == MXEncryptingErrorDomain,
            error.code == MXEncryptingErrorUnknownDeviceCode.rawValue,
            let devices = error.userInfo[MXEncryptingErrorUnknownDeviceDevicesKey] as? MXUsersDevicesMap<MXDeviceInfo> {
            return devices
        }
        return nil
    }
    
    /**
     Check if this event failed to send because of unknown devices.
     - returns: true if the event is a failed send that failed because of unknown devices, false otherwise.
     */
    public func isFailedWithUnknownDevices() -> Bool {
        return sentFailedUnknownDevices != nil
    }
    
    /**
     Check if this event is encrypted and encryption happened before we were in the related room, i.e. we will not currently be able to decrypt it.
     
     TODO: Maybe we should implement this by having a "creation date" on MXKAccount or similar, but for now, look for the MXDecryptingErrorUnknownInboundSessionIdCode error code. Is that correct?
     */
    public func isEncrypedAndSentBeforeWeJoined() -> Bool {
        if isEncrypted, let decryptionError = decryptionError {
            let error = decryptionError as NSError
            if error.domain == MXDecryptingErrorDomain, error.code == Int(MXDecryptingErrorUnknownInboundSessionIdCode.rawValue) {
                return true
            }
        }
        return false
    }
    
    private struct AssociatedKeys {
        static var userData = "userData"
    }
    
    /**
     Associated property to store arbitrary user data for an event.
     */
    public var userData: [AnyHashable:Any]? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.userData) as? [AnyHashable:Any]
        }
        set(newValue) {
            objc_setAssociatedObject(self, &AssociatedKeys.userData, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    /**
     Helper to check if the event is an audio attachment.
     */
    public func isAudioAttachment() -> Bool {
        if let type = content["msgtype"] as? String, type == kMXMessageTypeAudio {
            return true
        }
        if let type = content["msgtype"] as? String, type == kMXMessageTypeFile, let info = content["info"] as? [String:Any], let mime = info["mimetype"] as? String, mime.starts(with: "audio/") {
            return true
        }
        return false
    }
    
    /**
     Helper to check if the event is an image attachment.
     */
    public func isImageAttachment() -> Bool {
        if let type = content["msgtype"] as? String, type == kMXMessageTypeImage {
            return true
        }
        if let type = content["msgtype"] as? String, type == kMXMessageTypeFile, let info = content["info"] as? [String:Any], let mime = info["mimetype"] as? String, mime.starts(with: "image/") {
            return true
        }
        return false
    }

}

