//
//  MXMediaManager+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 11.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import MatrixSDK

extension MXMediaManager {
    
    /**
     Return the cache path for a generated thumbnail with the given id and size.
     If the "thumbnail" folder, or the "generated" folder does not exist, they are created.

     - parameter id: The room/user ID, for which this thumbnail is going to be.
     - parameter size: The width and height of the thumbnail.
     - returns: The thumbnail path as String or nil if the directory could not
        be created or `MXMediaManager`s base cache path was nil.
     */
    class func cachePathForGeneratedThumbnail(id: String, size: Int) -> String? {
        do {
            if let cachePath = MXMediaManager.getCachePath() {
                var cacheUrl = URL(fileURLWithPath: cachePath, isDirectory: true)
            
                cacheUrl.appendPathComponent(
                    String(format: "%lu", kMXMediaManagerAvatarThumbnailFolder.hash),
                    isDirectory: true)
                cacheUrl.appendPathComponent("generated", isDirectory: true)

                let fm = FileManager.default

                if !fm.fileExists(atPath: cacheUrl.path) {
                    try fm.createDirectory(at: cacheUrl, withIntermediateDirectories: true, attributes: nil)
                }

                return cacheUrl.appendingPathComponent("\(id)_\(size)", isDirectory: false).absoluteString
            }
        } catch {
            print("[\(String(describing: type(of: self)))] Error: \(error)")
        }

        return nil
    }
}
