//
//  ChatListDataSource.swift
//  Keanu
//
//  Created by N-Pex on 28.09.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixKit
import KeanuCore

/**
 Data source to be used for chat list table view.
 
 Extends the RoomSummariesDataSource with the flag "displayArchive".
 By default, the data source filters all rooms having "isArchived" set in
 their "others" parameter.
 To invert this behavior (i.e. ONLY show rooms with isArchived), set the
 "displayArchive" property to true.
 */
open class ChatListDataSource: RoomSummariesDataSource {

    /**
     Return true if there are ANY archived rooms in any of the active accounts.
     */
    public var hasArchivedRooms: Bool {
        guard let accounts = MXKAccountManager.shared()?.activeAccounts else { return false }
        return accounts.contains(where: { (account) -> Bool in
            return account.mxSession.roomsSummaries()?.contains(where: { (roomSummary) -> Bool in
                return roomSummary.isArchived
            }) ?? false
        })
    }
    
    var displayArchive: Bool = false {
        didSet(newValue) {
            reload()
        }
    }
    
    override open func onAddRoomSummary(_ roomSummary: MXRoomSummary) {
        if displayArchive == roomSummary.isArchived {
            super.onAddRoomSummary(roomSummary)
        }
    }
}

