//
//  MyDevicesViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 26.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK
import KeanuCore

open class MyDevicesViewController: DevicesViewController {

    open weak var session: MXSession?

    private var thisDevicesId: String?

    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()

    /**
     Delete action for table list row. Deletes a device after confirmation.
     */
    private lazy var deleteAction: UITableViewRowAction = {
        let action = UITableViewRowAction(
            style: .destructive,
            title: "Delete".localize())
        { (action, indexPath) in
            if self.devices.count > indexPath.row,
                let userId = self.session?.myUser?.userId {

                self.workingOverlay.isHidden = false

                let device = self.devices[indexPath.row]

                self.session?.matrixRestClient?.getSession(toDeleteDevice: device.deviceId) { response in
                    if response.isSuccess,
                        let authSession = response.value,
                        let session = authSession.session {

                        var passwordSupported = false

                        for flow in authSession.flows {
                            if flow.type == kMXLoginFlowTypePassword || flow.stages.contains(kMXLoginFlowTypePassword) {
                                passwordSupported = true
                                break
                            }
                        }

                        if passwordSupported {
                            let alert = AlertHelper.build(
                                message: "To delete the device \"%\", please provide your password.\n"
                                    .localize(value: device.displayName),
                                title: "Delete".localize(),
                                actions: [
                                    AlertHelper.cancelAction() { _ in
                                        self.workingOverlay.isHidden = true
                                    }
                                ])

                            AlertHelper.addPasswordField(alert, placeholder: "Password".localize())

                            alert.addAction(AlertHelper.destructiveAction("Delete".localize()) { _ in
                                self.session?.matrixRestClient?.deleteDevice(
                                    device.deviceId,
                                    authParameters: [
                                        "session": session,
                                        "user": userId,
                                        "password": alert.textFields?.first?.text ?? "",
                                        "type": kMXLoginFlowTypePassword,
                                ]) { response in
                                    if response.isFailure {
                                        AlertHelper.present(self, message:
                                            "Device deletion failed: %"
                                                .localize(value: response.error?.localizedDescription ?? "Unkown server error".localize()))
                                    }
                                    else {
                                        self.reloadDevices()
                                    }

                                    self.workingOverlay.isHidden = true
                                }
                            })

                            self.present(alert, animated: true)
                        }
                        else {
                            AlertHelper.present(self, message:
                                "Cannot delete devices because password-based authentication is not supported by the server.".localize())
                            self.workingOverlay.isHidden = true
                        }
                    }
                    else {
                        AlertHelper.present(self, message:
                            "Cannot delete devices at the moment: %"
                                .localize(value: response.error?.localizedDescription ?? "Unkown server error".localize()))
                        self.workingOverlay.isHidden = true
                    }
                }
            }

            DispatchQueue.main.async {
                self.tableView.setEditing(false, animated: true)
            }
        }

        return action
    }()

    override open func viewDidLoad() {
        super.viewDidLoad()

        reloadDevices()

        navigationItem.title = session?.myUser.friendlyName
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if let session = session,
                let cell = dequeueHeaderCell(indexPath) {

                return cell.apply(session.myUser, session, countKeys())
            }
        }
        else {
            if let cell = dequeueDeviceCell(indexPath) {

                return cell.apply(device: devices[indexPath.row],
                                  delegate: self,
                                  isThisDevice: thisDevicesId != nil && devices[indexPath.row].deviceId == thisDevicesId)
            }
        }

        return super.tableView(tableView, cellForRowAt: indexPath)
    }

    override open func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        // Header and this device cannot be deleted.
        if indexPath.row == 0 {
            return []
        }

        return [deleteAction]
    }

    override open func verificationChanged(_ device: MXDeviceInfo) {
        session?.crypto?.store?.storeDevice(forUser: session?.myUser?.userId,
                                            device: device)

        super.verificationChanged(device)
        
        // If verified, accept any possible pending key requests
        if device.verified == MXDeviceVerified {
            session?.crypto?.acceptAllPendingKeyRequests(fromUser: device.userId, andDevice: device.deviceId, onComplete: nil)
        }
    }

    /**
     Sorter for `MXDeviceInfo` classes.

     - Sort order:
     - This device
     - "Unkown" (new) devices
     - Verified devices
     - Unverified devices
     - Blocked devices
     - Second sort order:
     - Ascending by device ID
     */
    override open func deviceSorter(_ a: MXDeviceInfo, _ b: MXDeviceInfo) -> Bool {
        if a.deviceId == thisDevicesId {
            return true
        }

        if b.deviceId == thisDevicesId {
            return false
        }

        return super.deviceSorter(a, b)
    }

    private func reloadDevices() {
        if let userId = session?.myUser?.userId,
            let crypto = session?.crypto {

            thisDevicesId = crypto.store?.deviceId()

            if let devices = crypto.store?.devices(forUser: userId) {
                self.devices = devices.map() { $1 }.sorted(by: deviceSorter(_:_:))
            }

            crypto.downloadKeys([userId], forceDownload: true, success: { map in
                self.devices.removeAll(keepingCapacity: true)

                for deviceId in map?.deviceIds(forUser: userId) ?? [] {
                    if let device = map?.object(forDevice: deviceId, forUser: userId) {
                        self.devices.append(device)
                    }
                }

                self.devices.sort(by: self.deviceSorter(_:_:))

                self.tableView.reloadData()
            }, failure: nil)
        }
    }
}
