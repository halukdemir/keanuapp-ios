//
//  MyProfileViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 14.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MobileCoreServices
import MatrixKit
import KeanuCore
import Eureka

open class MyProfileViewController: ProfileViewController {

    open weak var account: MXKAccount?

    open override func createForm() {
        avatarRow.view?.load(account: account)

        nameRow.value = account?.friendlyName
        nameRow.placeholder = "Your Public Name".localize()

        form
            +++ avatarRow.onCellSelection { _, _ in
                guard let account = self.account else {
                    return
                }

                AvatarPicker.showPickerFor(account: account, avatarView: self.avatarRow.view, inViewController: self)
            }

            <<< nameRow.cellUpdate { cell, row in
                row.value = self.account?.friendlyName
                cell.textField?.font = .boldSystemFont(ofSize: 25)
                }
                .onChange { row in
                    if let value = row.value {
                        self.account?.setUserDisplayName(value, success: {}) { error in
                            row.value = self.account?.friendlyName
                            AlertHelper.present(self, message: error?.localizedDescription)
                        }

                        self.friend?.name = value
                    }
            }

            <<< LabelRow() {
                $0.title = account?.matrixId
            }

            +++ MultivaluedSection(header: "Other linked IDs".localize()) {
                $0.tag = "3pids"

                // TODO: Currently unused.
                $0.addButtonProvider = { section in
                    return ButtonRow("add_row") {
                        $0.title = "Link another ID".localize()
                        }
                        .cellUpdate { cell, _ in
                            cell.textLabel?.textAlignment = .natural
                    }
                }

                $0.multivaluedRowToInsertAt = { _ in LabelRow() }
            }

            +++ ButtonRow() {
                $0.title = "Change Password".localize()
                }
                .cellUpdate { cell, _ in
                    cell.textLabel?.textAlignment = .natural
                }
                .onCellSelection { _, _ in
                    self.changePassword()
            }

            <<< shareRow.onCellSelection { cell, _ in
                if let user = self.account?.mxSession?.myUser {
                    self.present(ProfileViewController.shareProfiles([user], cell), animated: true)
                }
            }

            <<< devicesRow.onCellSelection { _, _ in
                let vc = MyDevicesViewController()
                vc.session = self.account?.mxSession

                self.navigationController?.pushViewController(vc, animated: true)
            }

            <<< ButtonRow() {
                $0.title = "Close Account".localize()
                }
                .cellUpdate { cell, _ in
                    cell.textLabel?.textAlignment = .natural
                }
                .onCellSelection { _, _ in
                    self.closeAccount()
        }

        // Override onCellSelection of MultivaluedSection's add row here, because
        // otherwise it's too early.
        (form.rowBy(tag: "add_row") as? ButtonRow)?.onCellSelection { cell, row in
            let vc = Link3pidViewController()
            vc.account = self.account

            self.navigationController?.pushViewController(vc, animated: true)
        }


        update3pids()
    }

    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        reload3pids()
    }


    // MARK: UITableViewDelegate

    open override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete,
            let cell = tableView.cellForRow(at: indexPath) as? LabelCell,
            let address = cell.row.value,
            let threePid = account?.threePIDs.first(where: { $0.address == address }) {

            let delete = AlertHelper.destructiveAction("Delete".localize()) { _ in
                self.account?.mxRestClient.remove3PID(address: threePid.address, medium: threePid.medium) { response in
                    if let error = response.error {
                        AlertHelper.present(self, message: error.localizedDescription)
                    }

                    self.reload3pids()
                }
            }

            let cancel = AlertHelper.cancelAction() { _ in
                self.reload3pids()
            }

            AlertHelper.present(
                self, message: "Are you sure you want to delete % from your account?".localize(value: threePid.address),
                title: "Delete linked ID".localize(), actions: [delete, cancel])
        }

        super.tableView(tableView, commit: editingStyle, forRowAt: indexPath)
    }


    // MARK: Public Methods

    open func changePassword() {
        let alert = AlertHelper.build(title: "Change Password".localize(),
                                      actions: [AlertHelper.cancelAction()])

        alert.addAction(AlertHelper.defaultAction() { action in
            if let oldPassword = alert.textFields?.first?.text,
                let newPassword = alert.textFields?[1].text,
                let repeatPassword = alert.textFields?.last?.text {

                if newPassword != repeatPassword {
                    AlertHelper.present(
                        self, message: "Passwords don't match!".localize())

                    return
                }

                if newPassword.isEmpty {
                    AlertHelper.present(
                        self, message: "Please don't use an empty password!".localize())

                    return
                }

                self.account?.changePassword(
                    oldPassword, with: newPassword,
                    success: {
                        AlertHelper.present(
                            self,
                            message: "Password successfully changed!".localize(),
                            title: "Success".localize())
                    }) { error in
                        AlertHelper.present(self, message: error?.localizedDescription)
                    }
            }
        })


        AlertHelper.addPasswordField(alert, placeholder: "Old Password".localize())

        AlertHelper.addPasswordField(alert, placeholder:
            "Enter new password".localize())

        AlertHelper.addPasswordField(alert, placeholder:
            "Enter new password again".localize())

        present(alert, animated: true)
    }

    open func closeAccount() {
        let alert = AlertHelper.build(message: "Close your account on the server.".localize()
            + "\n\n"
            + "ATTENTION: This is non-reversible! You will never again be able to log in, re-register or reactivate that account!".localize()
            + "\n\n"
            + "The account will also be removed from this device!".localize(),
                                      title: "Close Account".localize(),
                                      actions: [AlertHelper.cancelAction()])

        AlertHelper.addPasswordField(alert, placeholder: "Password".localize())

        alert.addAction(AlertHelper.destructiveAction("Close Account Forever".localize()) { action in
            guard let password = alert.textFields?.first?.text, !password.isEmpty else {
                return AlertHelper.present(self, message: "Please provide your password!".localize())
            }

            guard let account = self.account else {
                return
            }

            let auth: [String: Any] = [
                "type": kMXLoginFlowTypePassword,

                // This is how it needs to be.
                "user": account.matrixId,

                // This is, how it *should* be according to the "User-interactive API" documentation.
                // Since it doesn't hurt to have this here, I'll leave it for forward
                // compatibility. (Hope dies last...)
                "identifier": [
                    "type": kMXLoginIdentifierTypeUser,
                    "user": account.matrixId,
                ],

                "password": password,
            ]

            account.mxSession.deactivateAccount(withAuthParameters: auth, eraseAccount: true) { response in
                if let error = response.error {
                    return AlertHelper.present(self, message: error.localizedDescription)
                }

                MXKAccountManager.shared()?.removeAccount(account, sendLogoutRequest: false) {
                    self.navigationController?.popViewController(animated: true)
                    if MXKAccountManager.shared()?.accounts.isEmpty ?? true {
                        DispatchQueue.main.async {
                           let _ = UIApplication.shared.popToChatListViewController()
                            UIApplication.shared.startOnboardingFlow()
                        }
                    }
                }
            }
        })

        present(alert, animated: true)
    }


    // MARK: Private Methods

    private func reload3pids() {
        account?.load3PIDs(update3pids) { _ in self.update3pids() }
    }

    private func update3pids() {
        guard var section = form.sectionBy(tag: "3pids") as? MultivaluedSection else {
            return
        }

        section.removeFirst(max(0, section.count - 1))

        var i = 0

        for threePid in self.account?.threePIDs ?? [] {
            if let row = section.multivaluedRowToInsertAt?(i) {

                row.title = MyProfileViewController.label(for3PidMedium: threePid.medium)
                row.baseValue = threePid.address

                section.insert(row, at: i)

                i += 1
            }
        }
    }

    /**
     Unified place to create friendly labels for `kMX3PIDMedium` IDs.

     - parameter threePidMedium: Currently supports `kMX3PIDMediumEmail` and `kMX3PIDMediumMSISDN`.
     - returns: a user-friendly label for `kMX3PIDMedium` IDs.
    */
    class func label(for3PidMedium threePidMedium: String?) -> String {
        switch threePidMedium ?? "" {
        case kMX3PIDMediumEmail:
            return "E-Mail".localize()

        case kMX3PIDMediumMSISDN:
            return "Mobile Phone".localize()

        default:
            return "Other".localize()
        }
    }
}
