//
//  MeViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 22.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixKit
import KeanuCore

open class MeViewController: UITableViewController {

    /**
     Logout action for table list row. Logs out of an account after confirmation.
     */
    private lazy var logoutAction: UITableViewRowAction = {
        let action = UITableViewRowAction(
            style: .destructive,
            title: "Remove".localize())
        { (action, indexPath) in
            if let account = self.accountManager?.accounts[indexPath.row] {
                let title = "Remove".localize()

                let message = "Are you sure you want to remove the account \"%\"?\n"
                    .localize(value: account.friendlyName)

                let actions = [
                    AlertHelper.cancelAction(),
                    AlertHelper.destructiveAction("Remove".localize()) { action in
                        self.accountManager?.removeAccount(account, completion: {
                            // If no more accounts, start onboarding
                            if MXKAccountManager.shared()?.accounts.isEmpty ?? true {
                                DispatchQueue.main.async {
                                   let _ = UIApplication.shared.popToChatListViewController()
                                    UIApplication.shared.startOnboardingFlow()
                                }
                            }
                        })
                    }
                ]

                AlertHelper.present(self, message: message, title: title, actions: actions)
            }

            self.endEditing()
        }

        return action
    }()

    /**
     Disable action for table list row. Disables an account.
     */
    private lazy var disableAction: UITableViewRowAction = {
        let action = UITableViewRowAction(
            style: .normal,
            title: "Disable".localize())
        { (action, indexPath) in
            if let account = self.accountManager?.accounts[indexPath.row] {
                account.isDisabled = true
            }

            self.endEditing()
        }

        return action
    }()

    /**
     Enable action for table list row. Enables an account.
     */
    private lazy var enableAction: UITableViewRowAction = {
        let action = UITableViewRowAction(
            style: .normal,
            title: "Enable".localize())
        { (action, indexPath) in
            if let account = self.accountManager?.accounts[indexPath.row] {
                account.isDisabled = false
            }

            self.endEditing()
        }

        return action
    }()

    /**
     Edit action for table list row. User can edit the name of a friend.
     */
    private lazy var editAction: UITableViewRowAction = {
        let action = UITableViewRowAction(
            style: .normal,
            title: "Edit Name".localize())
        { (action, indexPath) in
            if let account = self.accountManager?.accounts[indexPath.row] {

                AlertHelper.presentEditNameAlert(self, account.friendlyName) { newName in
                    account.setUserDisplayName(newName, success: {
                        self.tableView.reloadData()
                    }, failure: { error in
                        AlertHelper.present(self, message: error?.localizedDescription)
                    })
                }
            }

            self.endEditing()
        }

        return action
    }()

    open var accountManager: MXKAccountManager?

    override open func viewDidLoad() {
        super.viewDidLoad()

        accountManager = MXKAccountManager.shared()

        tableView.register(AvatarAnd3LabelsCell.nib, forCellReuseIdentifier: AvatarAnd3LabelsCell.defaultReuseId)

        let nc = NotificationCenter.default

        nc.addObserver(self, selector: #selector(refresh),
                       name: .mxkAccountManagerDidAddAccount, object: nil)

        nc.addObserver(self, selector: #selector(refresh),
                       name: .mxkAccountManagerDidRemoveAccount, object: nil)

        nc.addObserver(self, selector: #selector(refresh),
                       name: .mxSessionStateDidChange, object: nil)
        
        // If only one account (and it is active) show the account profile view controller by default. In that case an "edit" button is added to show the list (to get to the add/remove operations)
        if accountManager?.accounts.count == 1,
            let account = accountManager?.accounts.first,
            !account.isDisabled {

            navigationController?.pushViewController(createProfileViewController(account), animated: false)
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accountManager?.accounts.count ?? 0
    }

    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let account = accountManager?.accounts[indexPath.row],
            let cell = tableView.dequeueReusableCell(withIdentifier:
            AvatarAnd3LabelsCell.defaultReuseId, for: indexPath) as? AvatarAnd3LabelsCell {

            return cell.apply(account)
        }

        return UITableViewCell()
    }

    
    // MARK: UITableViewDelegate

    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }

    override open func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        return accountManager?.accounts[indexPath.row].isDisabled ?? true
            ? [enableAction, logoutAction]
            : [disableAction, logoutAction, editAction]
    }

    override open func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return accountManager?.accounts[indexPath.row].isDisabled ?? true
            ? nil : indexPath
    }

    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let account = accountManager?.accounts[indexPath.row] {
            pushProfile(account)
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }


    // MARK: Actions

    @IBAction func addAccount() {
        if let vc = AddAccountViewController.instantiate() {
            present(vc, animated: true)
        }
    }


    // MARK: Notifications

    @objc func refresh() {
        // Give the UITableView#setEditing animation some time before reloading.
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.tableView.reloadData()
        }
    }


    // MARK: Private Methods
    
    /**
     Performs segue to the `ProfileViewController`.
     */
    private func pushProfile(_ account: MXKAccount) {
        navigationController?.pushViewController(createProfileViewController(account), animated: true)
    }

    /**
     Closes any edit buttons on rows, animated.
    */
    private func endEditing() {
        DispatchQueue.main.async {
            self.tableView.setEditing(false, animated: true)
        }
    }

    /**
     Create a MyProfileViewController that we can push. If there is only one
     account (and it is active) show the VC without a back button and instead
     add an "edit" button to show the whole list (so we can add/delete).
     */
    open func createProfileViewController(_ account: MXKAccount) -> UIViewController {
        let profileVc = MyProfileViewController()
        profileVc.account = account

        if accountManager?.accounts.count == 1,
            let account = accountManager?.accounts.first,
            !account.isDisabled {

            profileVc.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit) { item in
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        return profileVc
    }
}
