//
//  String+Keanu.swift
//  Keanu
//
//  Created by Benjamin Erhart on 19.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import Foundation

extension String {

    /**
     Splits string in even pieces of given length.

     Taken from [Stack Overflow](https://stackoverflow.com/questions/32212220/how-to-split-a-string-into-substrings-of-equal-length)

     - parameter length: The length per piece.
     - returns: An array of subsequences, split from this collection’s elements.
    */
    public func split(by length: Int) -> [Substring] {
        var startIndex = self.startIndex
        var results = [Substring]()

        while startIndex < self.endIndex {
            let endIndex = self.index(startIndex, offsetBy: length, limitedBy: self.endIndex) ?? self.endIndex
            results.append(self[startIndex..<endIndex])
            startIndex = endIndex
        }

        return results
    }
    
    /**
     Generate a random alphanumeric string of the given length.
     
     Taken from [Stack Overflow](https://stackoverflow.com/questions/26845307/generate-random-alphanumeric-string-in-swift)
     */
    static func randomAlphanumericString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }
    
    static func randomNumericString(length: Int) -> String {
        let letters = "0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }

}
