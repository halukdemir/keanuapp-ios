//
//  Array+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 02.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

extension Array {
    
    /**
     Convenience method to add optionals to an array (makes code look cleaner). Only add if optional has a value.
    */
    public mutating func appendOptional(_ newElement: Element?) {
        if let element = newElement {
            self.append(element)
        }
    }
    
    /**
     Convenience method to add an optional array to an array (makes code look cleaner). Only add if optional has a value.
     */
    public mutating func appendOptional(contentsOf seq: [Element]?) {
        if let seq = seq {
            self.append(contentsOf: seq)
        }
    }
}
