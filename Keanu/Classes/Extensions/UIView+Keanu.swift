//
//  UIView+Keanu.swift
//  Keanu
//
//  Created by Benjamin Erhart on 14.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit

/**
 This extension provides some helper methods to hide/show `UIView`s.
 
 They are not only working on the `#isHidden` property, but also on height
 constraints, which will be modified, if existing or added anew, if not.
 
 Please note: This currently works properly only with horizontally designed
 layouts. For best effect, make all views dependent on their top neighbor's
 bottom and give height constraints to all necessary elements.
 
 You will be annoyed by a lot of constraint-break warnings, if the child elements
 also contain height constraints. They should be unproblematic. However, it is
 unclear how to avoid them without going through the hassle and remove all of
 them beforehand and restore them afterwards. (Which asks the question of the
 data structure to use for an adventure like that...)
 */
extension UIView {
    
    /**
     Helper struct to be able to store the original height of the view.
     
     Extensions are not allowed to have properties, so we need to work around
     that.
    */
    struct AssociatedKeys {
        static var height: UInt8 = 0
    }

    /**
     Hides
     
     - parameter animated: If the change (if any) shall be animated or not.
    */
    func hide(animated: Bool = false) {
        
        // Store the original height only the first time.
        if objc_getAssociatedObject(self, &AssociatedKeys.height) as? CGFloat == nil {
            objc_setAssociatedObject(self, &AssociatedKeys.height,
                                     bounds.size.height,
                                     .OBJC_ASSOCIATION_RETAIN)
        }
        
        if animated {
            animateHideShow(hide: true) { finished in
                self.setEqualHeightConstraint(0)
                self.animateLayout()
            }
        }
        else {
            setEqualHeightConstraint(0)
            isHidden = true
        }
    }
    
    /**
     Sets `isHidden` to `false`, and if `#hide` was called before, also sets
     a height constraint to the original (stored) height.
     
     - parameter animated: If the change (if any) shall be animated or not.
    */
    func show(animated: Bool = false) {
        if let height = objc_getAssociatedObject(self, &AssociatedKeys.height) as? CGFloat {
            setEqualHeightConstraint(height)
            
            if animated {
                animateLayout() { finished in
                    self.animateHideShow(hide: false)
                }
            }
            else {
                isHidden = false
            }
        }
        else {
            if animated {
                animateHideShow(hide: false)
            }
            else {
                isHidden = false
            }
        }
    }
    
    /**
     Sets the view hidden or shown, depending on the given toggle value.
     
     - parameter toggle: If true, view will be shown, if false view will be hidden.
     - parameter animated: If the change (if any) shall be animated or not.
    */
    func toggle(_ toggle: Bool, animated: Bool = false) {
        if toggle {
            show(animated: animated)
        }
        else {
            hide(animated: animated)
        }
    }
    
    
    // MARK: Private Methods
    
    /**
     Searches for an existing constraint which has an equal relation.
     Updates that with the given value.
     
     If one cannot be found, creates a new one.
     
     Deletes all other constraints, which constrain the height!
     
     - parameter value: The new height value.
    */
    private func setEqualHeightConstraint(_ value: CGFloat) {
        let equalHeightConstraint = constraints.filter() {
            $0.firstAttribute == .height && $0.relation == .equal
            }.first
        
        constraints.filter() { $0.firstAttribute == .height }.forEach() {
            if $0 != equalHeightConstraint {
                $0.isActive = false
            }
        }
        
        if let equalHeightConstraint = equalHeightConstraint {
            equalHeightConstraint.constant = value
        }
        else {
            heightAnchor.constraint(equalToConstant: value).isActive = true
        }
    }
    
    /**
     - returns: The oldest parent in the view hirarchy, if attached to any.
    */
    private func getTopView() -> UIView? {
        var view = superview
        
        while view?.superview != nil {
            view = view?.superview
        }
        
        return view
    }
    
    /**
     Call `#layoutIfNeeded` on the oldest parent of this view, if it is attached,
     to any.
     
     - parameter completion: A completion callback to call after the end of the
        animation or directly after, if there is no animation to do.
    */
    private func animateLayout(_ completion: ((Bool) -> Void)? = nil) {
        if let top = getTopView() {
            UIView.animate(withDuration: 0.25,
                           animations: { top.layoutIfNeeded() },
                           completion: completion)
        }
        else {
            if let completion = completion {
                completion(true)
            }
        }
    }
    
    /**
     Animate this view with a cross-dissolve transition to hidden/unhidden state,
     depending on the `hide` parameter, but only, if it's actually a change.
     
     - parameter hide: Set true to hide, false to unhide.
     - parameter completion: A completion callback to call after the end of the
        animation or directly after, if there is no animation to do.
     */
    private func animateHideShow(hide: Bool, _ completion: ((Bool) -> Void)? = nil) {
        if isHidden != hide {
            UIView.transition(with: self,
                              duration: 0.25,
                              options: .transitionCrossDissolve,
                              animations: { self.isHidden = hide },
                              completion: completion)
        }
        else {
            completion?(true)
        }
    }
    
    @IBInspectable
    public var useTintAsBackground: Bool
    {
        set {
            backgroundColor = newValue ? tintColor : UIColor.clear
        }
        
        get {
            return backgroundColor == tintColor
        }
    }
    
    @IBInspectable
    public var backgroundNamedColor: String?
    {
        get {
            return nil
        }
        
        set {
            if let val = newValue {
                if #available(iOS 11.0, *) {
                    if let col = UIColor(named: val) ?? UIColor(named: val, in: Bundle(for: BaseAppDelegate.self), compatibleWith: nil) {
                        backgroundColor = col
                    }
                }
            }
        }
    }
}
