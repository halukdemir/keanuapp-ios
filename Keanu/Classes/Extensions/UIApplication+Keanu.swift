//
//  UIApplication+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 20.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixKit
import KeanuCore

extension UIApplication {

    /**
     Start the scene flow from `Onboarding.storyboard`.
     */
    open func startOnboardingFlow() {
        keyWindow?.rootViewController = AddAccountViewController.storyboard
            .instantiateInitialViewController()
    }

    /**
     Start the scene flow from `Main.storyboard`.

     - parameter forwardIfNoFriends: Forward the user to the `AddFriendsViewController`
       if she doesn't have any friends, yet. Optional, defaults to `false`.
     */
    open func startMainFlow(forwardIfNoFriends: Bool = false) {
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)

        _ = RoomManager.shared

        MXKAccountManager.shared().prepareSessionForActiveAccounts()

        keyWindow?.rootViewController = UIStoryboard.overriddenOrLocal(name: "Main")
            .instantiateInitialVCWithDelegate()

        PushManager.shared.setupPush()

        // If the user doesn't have any friends, forward her to the `AddFriendsViewController`.
        if forwardIfNoFriends, let firstAccount = MXKAccountManager.shared()?.activeAccounts.first {
            do {
                // Need to wait for a session before checking if we have friends
                try firstAccount.performOnSessionReady({ (session) in
                    if FriendsManager.shared.friends.count < 1 {
                        DispatchQueue.main.async {
                            _ = self.openAddFriendsViewController()
                        }
                    }
                })
            } catch {}
        }
    }

    /**
     Regardless of current UI state, try to pop back to displaying the ChatListViewController.
     - warning: This function does a few assumtions of the UI in order to work: the root view controller of the window MUST be a UITabBarController and the FIRST child view controller of this tab controller MUST be the ChatListViewController.
     - returns: The ChatListViewController if found, otherwise nil.
     */
    open func popToChatListViewController() -> ChatListViewController? {
        if let tabBarController = keyWindow?.rootViewController as? UITabBarController {
            
            // If we are presenting, close the presented VC first
            if let presented = tabBarController.presentedViewController {
                presented.dismiss(animated: false, completion: nil)
            }
            
            // Select the first tab, should contain the ChatListViewController.
            tabBarController.selectedIndex = 0

            if let navC = tabBarController.selectedViewController as? UINavigationController {

                // Pop to root, which should be ChatListViewController.
                navC.popToRootViewController(animated: false)

                return navC.topViewController as? ChatListViewController
            }
        }
        return nil
    }

    /**
     Regardless of current UI state, try to show the "Add friend" view controller.

     - parameter callback: An optional callback that will be called with the
       opened `AddFriendViewController`, or nil if the open failed for some reason.
     - returns: true on success, otherwise false.
     */
    open func openAddFriendsViewController(_ callback: ((AddFriendViewController?)->())? = nil) -> Bool {
        if let chatVc = UIApplication.shared.popToChatListViewController(),
            // Present the Friends.storyboard
            let friendsVc = ChooseFriendsViewController.instantiate(chatVc) {
            
            chatVc.present(friendsVc, animated: false)
            
            if let chooseFriendsVc = friendsVc.topViewController as? ChooseFriendsViewController {
                chooseFriendsVc.pushAddFriend()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    callback?(friendsVc.topViewController as? AddFriendViewController)
                }
                return true
            }
        }
        return false
    }
    
    /**
     Join a given room.

     If you want to accept an invitation, one account will know about that room
     and that account is used.

     If you want to join a (public) room, which you didn't get a personal invitation
     for, (but maybe a join link), use the `uninvited` option.

     - parameter roomId: The room ID or room alias to join.
     - parameter uninvited: Use the first account,
        if we can't find an account which knows this room already.
     - parameter callback: Callback, when processing is done.
     - parameter success: true, if successful, false, if not.
     - parameter room: The `MXRoom` object representing the room that was joined.
     */
    open func joinRoom(_ roomId: String, uninvited: Bool = false, callback: ((_ success: Bool, _ room: MXRoom?) -> ())?) {
        let manager = MXKAccountManager.shared()

        var account = manager?.accountKnowingRoom(withRoomIdOrAlias: roomId)

        if account == nil && uninvited {
            account = manager?.activeAccounts.first
        }

        if let account = account,
            let session = account.mxSession {

            session.joinRoom(roomId) { response in
                callback?(response.isSuccess, response.value)
            }
        }
        else {
            callback?(false, nil)
        }
    }

    /**
     Decline the invitation to the given room.
     */
    open func declineRoomInvite(roomId: String, callback: ((Bool) -> ())?) {
        if let account = MXKAccountManager.shared()?.accountKnowingRoom(withRoomIdOrAlias: roomId),
            let session = account.mxSession,
            let room = session.room(withRoomId: roomId) {
            // Call leave to decline
            self.leaveRoom(room: room, callback: { room, success in
                callback?(success)
            })
        }
        else {
            callback?(false)
        }
    }

    /**
     Open the room with the given room id the UI, if found.
     - parameter roomId: The room id of the room to open.
     */
    open func openRoom(roomId: String) {
        // Already open?
        guard RoomManager.shared.currentlyViewedRoomId != roomId else { return }

        if let account = MXKAccountManager.shared()?.accountKnowingRoom(withRoomIdOrAlias: roomId),
            let session = account.mxSession,
            let room = session.room(withRoomId: roomId),
            let chatListViewController = self.popToChatListViewController() {
            chatListViewController.openRoom(room)
        }
    }
    
    /**
     Open a/the room with the given friend in the UI.
     - parameter friend: The friend we want to open a room to. If a 1:1 chat already exists, reuse that, otherwise create a new room with the friend.
     */
    open func openRoom(friend: Friend) {
        if let chatListViewController = self.popToChatListViewController() {
            // Start a 1:1 chat the same as if the user had chosen the
            // friend in the friend's list.
            chatListViewController.friendsChosen([friend])
        }
    }
    
    /**
     Open a/the room with the given Matrix id in the UI.
     - parameter matrixId: The user we want to open a room to. If a 1:1 chat already exists, reuse that, otherwise create a new room with the friend.
     - parameter encrypted: Whether or not to make a new room ecnrypted (if created). Defaults to true.
     */
    open func openRoom(matrixId: String, encrypted: Bool = true) {
        if let chatListViewController = self.popToChatListViewController() {
            let friend = FriendsManager.shared.getOrCreate(MXUser(userId: matrixId))
            chatListViewController.openRoom([friend], encrypted: encrypted)
        }
    }
    
    /**
     Create an empty room that you can later add friends to.
     */
    open func createEmptyRoom(session:MXSession) {
        if let chatListViewController = self.popToChatListViewController() {
            chatListViewController.createEmptyRoom(session: session)
        }
    }
    
    /**
     Iterative function to give admin rights to all room members in an array. When this is done, leave the group.
     - parameter room: The room we are leaving.
     - parameter index: The current index into the array of members.
     - parameter members: The members we are giving admin rights to.
     - parameter callback: This callback is invoked when we are done, the bool value indicating success or not.
     */
    private func makeAdminAndLeave(room:MXRoom, index:Int, members:[MXRoomMember]?, callback:@escaping ((Bool)->())) {
        if let members = members, index < members.count {
            let member = members[index]
            room.setPowerLevel(ofUser: member.userId, powerLevel: 100) { (response) in
                if response.isSuccess {
                    self.makeAdminAndLeave(room: room, index: index + 1, members: members, callback: callback)
                } else {
                    callback(false)
                }
            }
            return
        }
        // Do leave!
        room.leave { (response) in
            room.mxSession.store.deleteRoom(room.roomId)
            callback(response.isSuccess)
        }
    }
    
    /**
     Leave the given room. If we are the last admin in the room, show the "transfer admin" view controller, so we can pick other member(s) to give admin rights to.
     */
    open func leaveRoom(room: MXRoom, callback:((MXRoom, Bool)->())?) {
        guard let myUser = room.myUser, let summary = room.summary, let membersCount = summary.membersCount else {
            callback?(room, false)
            return
        }
        
        if room.isAdmin(userId: myUser.userId), membersCount.members > 1 {
            // Pass on our admin rights? Check if there are still members in the room (joined or invited) and if none of them are admins. In that case we are the last to leave, so need to give admin rights to someone.
            var members:[MXRoomMember] = []
            room.dangerousSyncState.members.members.forEach { (member) in
                if member.userId != myUser.userId, (member.membership == .join || member.membership == .invite), !room.isAdmin(userId: member.userId) {
                    members.append(member)
                }
            }
            if members.count > 0 {
                // Sort the members list

                RoomMembersDataSource.sortMembers(members: &members, inRoom: room)

                let vc = TransferAdminViewController(room: room)
                vc.setMembers(members)
                vc.delegate = TransferAdminViewControllerDelegateObject(
                    didNotSelectMembers: { (viewController) in
                        viewController.dismiss(animated: true, completion: {
                            // Canceled
                            callback?(room, false)
                        })
                },
                    didSelectMembers: { (members:[MXRoomMember], viewController) in
                        viewController.dismiss(animated: true, completion: {
                            // One or more members picked
                            self.makeAdminAndLeave(room: room, index: 0, members: members, callback: { (success) in
                                callback?(room, success)
                            })
                        })
                })
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                if let tabBarController = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController {
                    (tabBarController.presentedViewController ?? tabBarController).present(vc, animated: true, completion: nil)
                    return
                }
            }
        }
        room.leave { (response) in
            room.mxSession.store.deleteRoom(room.roomId)
            callback?(room, response.isSuccess)
        }
    }
    
    /**
     Forward the given event (contents) to another room. A view controller for selecting a room will be presented from the root view controller.
     - parameter presentingViewController: optional view controller to present the "select room" view controller on. If not set, default to the root tab bar view controller.
     - parameter event: The event to send to another room.
     */
    open func forwardEvent(presentingViewController:UIViewController?, event: MXEvent) {
        let vc = SelectRoomViewController { (_ viewController:SelectRoomViewController, _ room:MXRoom?) in
            viewController.dataSource = nil
            if let room = room {
                var localEcho:MXEvent?
                room.sendMessage(withContent: event.content, localEcho: &localEcho) { (response) in
                }
                RoomDataSource.storeLocalEcho(room: room, localEcho: localEcho)
            }
            // Done, close
            viewController.navigationController?.dismiss(animated: true, completion: nil)
        }
        let navController = UINavigationController(rootViewController: vc)
        let presenter = presentingViewController ?? keyWindow?.rootViewController
        presenter?.present(navController, animated: true, completion: nil)
    }
}
