//
//  CALayer+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 15.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit

extension CALayer {

    @IBInspectable
    public var borderUIColor: UIColor? {
        get {
            return borderColor != nil ? UIColor(cgColor: borderColor!) : nil
        }
        set(newValue) {
            borderColor = newValue?.cgColor
        }
    }
}
