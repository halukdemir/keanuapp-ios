//
//  UIViewController+Keanu.swift
//  AFNetworking
//
//  Created by N-Pex on 07.05.19.
//

import Foundation

extension UIViewController {
    /**
     Adds a `UITapGestureRecognizer` to the root `UIView` which hides the
     soft keyboard, if one is shown, so users can hide the keyboard this way.
     */
    public func hideKeyboardOnOutsideTap() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    /**
     Hides the soft keyboard, if one is shown.
     */
    @objc public func dismissKeyboard() {
        view.endEditing(true)
    }
}
