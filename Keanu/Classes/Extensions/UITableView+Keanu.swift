//
//  UITableView+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 02.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit

extension UITableView {
    
    /**
     Return true if the height of the content is equal or taller than the viewport, i.e. the table view is "filled".
     */
    open var viewportIsFilled: Bool {
        get {
            return contentSize.height >= frame.height
        }
    }
    
    /**
     Helper to return the last index path in a UITableView
     
     - Returns: the last valid IndexPath in the UITableView, or nil
     */
    open var lastIndexPath: IndexPath? {
        if self.numberOfSections > 0 {
            for sectionIndex in (0..<self.numberOfSections).reversed() {
                if self.numberOfRows(inSection: sectionIndex) > 0 {
                    return IndexPath.init(item: self.numberOfRows(inSection: sectionIndex)-1, section: sectionIndex)
                }
            }
        }
        return nil
    }
}
