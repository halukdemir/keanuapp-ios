//
//  TransferAdminViewController.swift
//  Keanu
//
//  Created by N-Pex on 26.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK

protocol TransferAdminViewControllerDelegate {
    func didSelectMembers(_ members: [MXRoomMember], from viewController:UIViewController)
    func didNotSelectMembers(from viewController:UIViewController)
}

class TransferAdminViewControllerDelegateObject: NSObject, TransferAdminViewControllerDelegate {
    var didNotSelectMembersF:((UIViewController)->())?
    var didSelectMembersF:(([MXRoomMember],UIViewController)->())?

    func didNotSelectMembers(from viewController: UIViewController) {
        didNotSelectMembersF?(viewController)
    }
    
    func didSelectMembers(_ members: [MXRoomMember], from viewController: UIViewController) {
        didSelectMembersF?(members, viewController)
    }

    init(
        didNotSelectMembers:((UIViewController)->())? = nil,
        didSelectMembers:(([MXRoomMember],UIViewController)->())? = nil
        ) {
        self.didNotSelectMembersF = didNotSelectMembers
        self.didSelectMembersF = didSelectMembers
    }
}

class TransferAdminViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    public var delegate:TransferAdminViewControllerDelegate?
    
    public var room:MXRoom?
    public var members:[MXRoomMember] = []
    private var selected:[MXRoomMember] = []
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var leaveButton: UIButton!
    @IBOutlet weak var checkedAccessoryView: UIView!

    @objc init(room: MXRoom?) {
        super.init(nibName: String(describing: type(of: self)), bundle: Bundle(for: type(of: self)))
        self.room = room
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 58
        tableView.estimatedSectionHeaderHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        tableView.rowHeight = UITableView.automaticDimension
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(RoomMemberCell.nib, forCellReuseIdentifier: RoomMemberCell.defaultReuseId)
        self.leaveButton?.isEnabled = (self.members.count == 0)
    }
    
    public func setMembers(_ members:[MXRoomMember]) {
        self.members.removeAll()
        self.selected.removeAll()
        self.members.append(contentsOf: members)
        self.leaveButton?.isEnabled = (self.members.count == 0)
    }
    
    @IBAction func didPressCancel(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didNotSelectMembers(from: self)
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didPressLeave(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didSelectMembers(self.selected, from: self)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.members.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let member = self.members[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: RoomMemberCell.defaultReuseId, for: indexPath)
        if let cell = cell as? RoomMemberCell, let room = self.room {
            cell.render(member: member, room: room)
        }
        cell.accessoryType = (self.selected.contains(member) ? .checkmark : .none)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let member = self.members[indexPath.row]
        if let idx = selected.firstIndex(of: member) {
            selected.remove(at: idx)
        } else {
            selected.append(member)
        }
        self.leaveButton.isEnabled = (self.members.count == 0 || selected.count > 0)
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}
