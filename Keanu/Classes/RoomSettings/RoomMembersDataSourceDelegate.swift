//
//  RoomMembersDataSourceDelegate.swift
//  Keanu
//
//  Created by N-Pex on 19.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import MatrixSDK

public protocol RoomMembersDataSourceDelegate {
    
    /**
     Create a table view cell for this room member object.
     
     - Parameter at: The IndexPath of the object in the table view.
     - Parameter for: The MXRoomMember object to display.
     */
    func createCell(at indexPath:IndexPath, for roomMember:MXRoomMember) -> UITableViewCell?
    
    func didChangeAllRows()
    func didChangeRows(at indexPaths:[IndexPath])
}
