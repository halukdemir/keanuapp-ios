//
//  RoomSettingsViewController.swift
//  Keanu
//
//  Created by N-Pex on 18.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK
import KeanuCore

public protocol RoomSettingsViewControllerDelegate {

    func didLeaveRoom(_ roomSettingsViewController: RoomSettingsViewController) -> Void

    func didArchiveRoom(_ roomSettingsViewController: RoomSettingsViewController) -> Void
}

/// Cell identifiers from the Rooms.storyboard
public enum StoryboardCellIdentifier: String {
    case name = "cellRoomName"
    case topic = "cellRoomTopic"
    case encryption = "cellRoomEncryption"
    case share = "cellRoomShare"
    case addFriends = "cellRoomAddFriends"
    case mute = "cellRoomMute"
    case membersLabel = "cellRoomMembers"
    case leave = "cellRoomLeave"
}

open class RoomSettingsViewController: UIViewController {

    private static let shareLink = Config.inviteLinkFormat

    public var delegate: RoomSettingsViewControllerDelegate? = nil
    
    @IBOutlet open weak var tableView: UITableView!
    @IBOutlet weak var largeAvatarView: AvatarView!
    
    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    let disabledCellAlphaValue: CGFloat = 0.5
    
    // For matching navigation bar and avatar
    var navigationBarShadow: UIImage?
    var navigationBarBackground: UIImage?
    var topBounceView: UIView?
    
    var notificationToken: NSObjectProtocol? = nil
    
    open var allHeaderRows = [StoryboardCellIdentifier]()
    open var headerRows = [StoryboardCellIdentifier]()
    open var footerRows = [StoryboardCellIdentifier]()
    
    open var crownImage: UIImage?
    
    open var room: MXRoom? {
        didSet {
            didChangeRoom(from: oldValue, to: room)
        }
    }
    open var roomMembersDataSource: RoomMembersDataSource?
    open var observers = [NSObjectProtocol]()
    
    deinit {
        observers.forEach({ NotificationCenter.default.removeObserver($0) })
        observers.removeAll()
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        allHeaderRows = [.name, .topic, .encryption, .addFriends,
                         .share, .mute, .membersLabel]
        
        headerRows = allHeaderRows
        
        footerRows = [.leave]
        
        // Add observers
        observers.append(NotificationCenter.default.addObserver(forName: .mxRoomSummaryDidChange, object: nil, queue: .main) { notification in
            if let updatedSummary = notification.object as? MXRoomSummary,
                let thisRoom = self.room,
                updatedSummary.roomId == thisRoom.roomId {

                self.updateUIBasedOnOwnRole()
            }
        })
        
        // Register cell types
        tableView.register(RoomMemberCell.nib, forCellReuseIdentifier: RoomMemberCell.defaultReuseId)
        
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 58
        tableView.estimatedSectionHeaderHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.dataSource = self
        tableView.delegate = self
        
        didChangeRoom(from: nil, to: room)
        updateUIBasedOnOwnRole()
    }
    
    func updateUIBasedOnOwnRole() {
        guard let room = room else {
            return
        }
    
        headerRows = allHeaderRows
        
        if !room.canSendInvites() {
            headerRows.removeAll { $0 == .addFriends || $0 == .share }
        }
        
        // If encrypted already, remove the toggle.
        if room.summary.isEncrypted {
            headerRows.removeAll { $0 == .encryption }
        }
        
        // Update the header section
        tableView.reloadSections(IndexSet(arrayLiteral: 0), with: .none)
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tableView {
            // Adjust the frame of the overscroll view.
            if let topBounceView = topBounceView {
                topBounceView.frame = CGRect(x: 0, y: 0,
                                             width: tableView.frame.size.width,
                                             height: tableView.contentOffset.y)
            }
        }
    }
    
    open func didChangeRoom(from: MXRoom?, to: MXRoom?) {
        guard self.isViewLoaded else {
            return
        }
        
        roomMembersDataSource?.delegate = nil
        roomMembersDataSource = nil
        largeAvatarView.image = nil
        
        if let room = to {
            roomMembersDataSource = RoomMembersDataSource(room: room)
            roomMembersDataSource?.section = 1
            roomMembersDataSource?.delegate = self

            largeAvatarView.load(for: room.summary)
        }

        updateUIBasedOnOwnRole()
    }
    
    open func didUpdateState() {
        tableView?.reloadData()
    }
    
    open func createHeaderCell(type: StoryboardCellIdentifier, at indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: type.rawValue) else {
            return UITableViewCell()
        }

        switch type {
        case .name, .topic:
            cell.textLabel?.text = type == .name
                ? room?.summary.displayname
                : room?.summary.topic

            var isOnline = false
            var canModify = false

            if let room = room, let session = room.mxSession, session.myUser != nil {
                isOnline = (session.state == MXSessionStateRunning)
                canModify = type == .name
                    ? room.canModifyName()
                    : room.canModifyTopic()
            }

            if canModify {
                cell.accessoryView?.alpha = isOnline ? 1 : disabledCellAlphaValue
                cell.accessoryView?.isUserInteractionEnabled = isOnline
            }
            else {
                cell.accessoryView = nil
            }

            cell.contentView.alpha = isOnline ? 1 : disabledCellAlphaValue
            cell.selectionStyle = .none

            break

        case .encryption:
            let encryptionSwitch = UISwitch()
            encryptionSwitch.isEnabled = false

            if let room = room, let session = room.mxSession, session.myUser != nil {
                encryptionSwitch.setOn(room.summary.isEncrypted, animated: false)

                if room.canEnableEncryption()
                    && (session.state == MXSessionStateRunning)
                    && !room.summary.isEncrypted {

                    encryptionSwitch.addTarget(self, action: #selector(self.didEnableEncryption(_:)), for: .valueChanged)
                    encryptionSwitch.isEnabled = true
                }
            }

            cell.accessoryView = encryptionSwitch
            cell.isUserInteractionEnabled = true
            cell.selectionStyle = .none

            break

        case .addFriends, .share:
            let canSendInvites = room?.mxSession?.myUser != nil && room?.canSendInvites() ?? false

            cell.isUserInteractionEnabled = canSendInvites
            cell.contentView.alpha = canSendInvites ? 1 : disabledCellAlphaValue

            break

        case .mute:
            let muteSwitch = UISwitch()

            if let room = self.room {
                refreshNotificationSwitch(muteSwitch, room: room, animated: false)
            }

            muteSwitch.addTarget(self, action: #selector(self.didChangeNotificationSwitch(_:)), for: .valueChanged)

            cell.accessoryView = muteSwitch
            cell.isUserInteractionEnabled = true
            cell.selectionStyle = .none

            break

        default:
            break
        }

        return cell
    }
    
    open func createFooterCell(type: StoryboardCellIdentifier, at indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier:type.rawValue) ?? UITableViewCell()
    }
    
    open func didSelectHeaderCell(type: StoryboardCellIdentifier) {
        switch type {
        case .addFriends:
            addMoreFriends()

            break

        case .share:
            if let index = headerRows.firstIndex(of: .share),
                let cell = tableView.cellForRow(at: IndexPath(item: index, section: 0)) {
                shareInviteLink(sourceView: cell)
            }

            break

        default:
            break
        }
    }
    
    open func didSelectFooterCell(type: StoryboardCellIdentifier) {
        switch type {
        case .leave:
            if self.room != nil {
                AlertHelper.present(
                    self,
                    message: "Your room chat history will be wiped away. To keep these chats, archive the room instead.".localize(),
                    title: "Leave room?".localize(),
                    actions: [
                        AlertHelper.defaultAction("Archive".localize()) { action in
                            self.archiveRoom()
                        },
                        AlertHelper.defaultAction("Leave".localize()) { action in
                            self.leaveRoom()
                        },
                        AlertHelper.cancelAction()
                    ])

                return
            }
            break

        default:
            break
        }
    }
    
    @IBAction func didPressEditName(_ sender: Any) {
        guard let room = room else {
            return
        }

        let alert = AlertHelper.build(title: "Change room name".localize(),
                                      actions: [AlertHelper.cancelAction()])

        alert.addAction(AlertHelper.defaultAction() { action in
            if let newName = alert.textFields?.first?.text {
                room.setName(newName) { response in
                    // TODO - handle failure. Success will be picked up by the "updated room summary" event.
                }
            }
        })

        AlertHelper.addTextField(alert, placeholder: room.summary.displayname)

        present(alert, animated: true)
    }
    
    @IBAction func didPressEditTopic(_ sender: Any) {
        guard let room = room else {
            return
        }

        let alert = AlertHelper.build(title: "Change room topic".localize(),
                                      actions: [AlertHelper.cancelAction()])

        alert.addAction(AlertHelper.defaultAction() { action in
            if let newTopic = alert.textFields?.first?.text {
                room.setTopic(newTopic, completion: { response in
                    // TODO - handle failure. Success will be picked up by the "updated room summary" event.
                })
            }
        })

        AlertHelper.addTextField(alert, placeholder: room.summary.topic)

        present(alert, animated: true)
    }
    
    @objc func didEnableEncryption(_ sender: UIControl!) {
        guard let encryptionSwitch = sender as? UISwitch, let room = room else {
            return
        }

        if encryptionSwitch.isOn {
            room.enableEncryption(withAlgorithm: kMXCryptoMegolmAlgorithm) { response in
                // TODO - handle failure. Success will be picked up by the "updated room summary" event.
            }
        }
    }
    
    private func refreshNotificationSwitch(_ uiswitch: UISwitch, room: MXRoom, animated: Bool) {
        uiswitch.setOn(room.notificationLevel != .None, animated: animated)
    }

    @objc func didChangeNotificationSwitch(_ sender: UISwitch!) {
        if let room = room {
            if sender.isOn {
                room.notificationLevel = .All
            } else {
                room.notificationLevel = .None
            }
        }
    }
    
    /*
     Invite some more friends to this room.
     */
    private func addMoreFriends() {
        if let friendsVc = ChooseFriendsViewController.instantiate(self),
            let chooseVC = friendsVc.topViewController as? ChooseFriendsViewController {
            chooseVC.selectNewlyAddedFriends = true
            present(friendsVc, animated: true)
        }
    }

    private func handleError(_ error: Error) {
        workingOverlay.isHidden = true

        AlertHelper.present(self, message: error.localizedDescription)
    }


    /**
     Step 1 of setting everything up to actually be able to share an invite link.

     Retrieves the room's canonical alias. If not available, tries to create one.

     If successful, moves to step 2: `#configRoom`.

     - parameter sourceView: The view, which triggered this. Needed for iPad.
    */
    private func shareInviteLink(sourceView: UIView) {
        guard let room = room, let roomId = room.roomId else {
            return
        }

        workingOverlay.message = "Preparing public invitation..."
        workingOverlay.isHidden = false

        // Check, if this room maybe has a friendly alias. If so, use that.
        room.mxSession.matrixRestClient.canonicalAlias(ofRoom: roomId) { response in

            // We *need* a "canonical alias" for cross-server joins, so create one.
            if response.isFailure {

                // Since aliases also need to be ASCII, we don't try to sanitize
                // the room name, as that might just contain far-out UTF-8 characters.
                // Instead, we just reuse the romm ID, because that will definitely
                // be a valid alias. It's not pretty, but the only other viable
                // solution is to show a lot of UI with sophisticated assistance,
                // so the user is able to create a valid alias themselves.
                // That's a lot of work for such a feature...
                let alias = roomId.replacingOccurrences(
                    of: "!", with: "#", options: [],
                    range: Range(uncheckedBounds: (lower: roomId.startIndex, upper: roomId.index(after: roomId.startIndex))))

                room.addAlias(alias) { response in
                    if let error = response.error {
                        return self.handleError(error)
                    }

                    room.setCanonicalAlias(alias) { response in
                        if let error = response.error {
                            return self.handleError(error)
                        }

                        self.configRoom(alias, sourceView)
                    }
                }

                return
            }

            self.configRoom(response.value!, sourceView)
        }
    }

    /**
     Step 2 of setting everything up to actually be able to share an invite link.

     Checks, if the join rule of the room is set to public. If not, tries to set
     it to.

     Also checks, if guest access is disabled, and tries to set it so, if not,
     but continues in both cases, because not important for functionality, just
     a security feature.

     If successful, moves to step 3: `#showShareSheet`.

     - parameter alias: The canonical found or created for the current room.
     - parameter sourceView: The view, which triggered this. Needed for iPad.
    */
    private func configRoom(_ alias: String, _ sourceView: UIView) {
        guard let room = room, let roomId = room.roomId else {
            return
        }

        let id = alias.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? roomId

        guard let shareUrl = URL(string: String(format: RoomSettingsViewController.shareLink, id)) else {
            // We're a bit lazy here, because this actually shouldn't happen.
            // It would be an app config issue, when then URL would be broken.
            self.workingOverlay.isHidden = true

            return
        }

        room.mxSession.matrixRestClient.joinRule(ofRoom: roomId) { response in
            if let error = response.error {
                return self.handleError(error)
            }

            if response.value ?? .invite == .public {
                // Great! No need to change anything, where we might not have
                // the permission to.
                return self.showShareSheet(shareUrl, sourceView)
            }

            // Set room to public, so users with link can actually join...
            room.setJoinRule(.public) { response in

                // This might fail, when we don't have enough permission.
                // Of course it would be nicer to check before we even show
                // the option, but, alas, that's complicated.

                if let error = response.error {
                    return self.handleError(error)
                }

                // ...but not guests!
                room.setGuestAccess(.forbidden) { response in

                    // We ignore errors here. Guest access is forbidden by
                    // default anyway, so it's quite likely, that it's already
                    // the way we want it. And if not, and we can't change it,
                    // then that doesn't have an impact on this feature, also.

                    self.showShareSheet(shareUrl, sourceView)
                }
            }
        }
    }

    /**
     Step 3 of setting everything up to actually be able to share an invite link.

     Shows the share sheet with the created share URL.

     - parameter shareUrl: The URL to join the room, which should be shared.
     - parameter sourceView: The view, which triggered this. Needed for iPad.
    */
    private func showShareSheet(_ shareUrl: URL, _ sourceView: UIView) {
        workingOverlay.isHidden = true

        let vc = UIActivityViewController(activityItems: [shareUrl], applicationActivities: [ShowQrActivity()])

        vc.popoverPresentationController?.sourceView = sourceView
        vc.popoverPresentationController?.sourceRect = sourceView.bounds

        self.present(vc, animated: true)
    }
}

// MARK: RoomMembersDataSourceDelegate
extension RoomSettingsViewController: RoomMembersDataSourceDelegate {

    public func createCell(at indexPath: IndexPath, for roomMember: MXRoomMember) -> UITableViewCell? {
        guard let room = room, let cellData = roomMembersDataSource?.roomMember(at: indexPath) else {
            return nil
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: RoomMemberCell.defaultReuseId, for: indexPath)

        if let cell = cell as? RoomMemberCell {
            cell.render(member: cellData, room: room)
        }

        return cell
    }
    
    public func didChangeAllRows() {
        tableView?.reloadSections(IndexSet(arrayLiteral: 1), with: .automatic)
    }
    
    public func didChangeRows(at indexPaths: [IndexPath]) {
        tableView?.reloadSections(IndexSet(arrayLiteral: 1), with: .automatic)
    }
}

// MARK: UITableViewDataSource
extension RoomSettingsViewController: UITableViewDataSource {
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return headerRows.count

        case 2:
            return footerRows.count

        default:
            return roomMembersDataSource?.tableView(tableView, numberOfRowsInSection:section) ?? 0
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            return createHeaderCell(type: headerRows[indexPath.row], at: indexPath)

        case 2:
            return createFooterCell(type: footerRows[indexPath.row], at: indexPath)

        default:
            return roomMembersDataSource?.tableView(tableView, cellForRowAt: indexPath) ?? UITableViewCell()
        }
    }
}

// MARK: UITableViewDelegate
extension RoomSettingsViewController: UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 0 {
            return didSelectHeaderCell(type: headerRows[indexPath.row])
        }
        else if indexPath.section == 2 {
            return didSelectFooterCell(type: footerRows[indexPath.row])
        }
        
        guard let room = room,
            let myUser = room.myUser,
            let dataSource = roomMembersDataSource,
            let member = dataSource.roomMember(at: indexPath),
            // Tapping ourselves is a no-op.
            member.userId != myUser.userId else {

            return
        }
        
        let alert = AlertHelper.build(title: nil, style: .actionSheet, actions: nil)

        if room.canMakeUserAdmin(userId: member.userId) {
            // Action to make someone an admin for a room.
            alert.addAction(AlertHelper.defaultAction("Make admin".localize())
            { action in
                self.makeAdmin(member: member)
            })
        }
        
        if room.canMakeUserModerator(userId: member.userId) {
            // Action to make someone a moderator for a room.
            alert.addAction(AlertHelper.defaultAction("Make moderator".localize())
            { action in
                self.makeModerator(member: member)
            })
        }

        // Action to view profile.
        alert.addAction(AlertHelper.defaultAction("View profile".localize())
        { action in
            self.viewMemberProfile(member: member)
        })

        if room.canUnmakeUserModerator(userId: member.userId) {
            // Action to unmake someone a moderator for a room.
            alert.addAction(AlertHelper.defaultAction("Remove as moderator".localize())
            { action in
                self.unmakeModerator(member: member)
            })
        }
        
        if room.canKickUser(userId: member.userId) {
            // Action to kick someone from a room.
            alert.addAction(AlertHelper.destructiveAction("Kick".localize())
            { action in
                self.kickMember(member: member)
            })
        }
        
        if alert.actions.count == 1 {
            viewMemberProfile(member: member)
        }
        else {
            alert.addAction(AlertHelper.cancelAction())

            if let popoverController = alert.popoverPresentationController,
                let cell = tableView.cellForRow(at: indexPath) {

                popoverController.sourceView = cell
                popoverController.sourceRect = cell.bounds
            }

            present(alert, animated: true)
        }
    }
}

// MARK: Room actions
extension RoomSettingsViewController {

    open func leaveRoom() {
        guard let room = room else {
            return
        }

        // Activity indicator when leaving room.
        workingOverlay.message = "Leaving room".localize()
        workingOverlay.isHidden = false

        UIApplication.shared.leaveRoom(room: room) { room, success in
            self.workingOverlay.isHidden = true

            if success,
                let delegate = self.delegate {

                delegate.didLeaveRoom(self)
            }
        }
    }
    
    open func archiveRoom() {
        delegate?.didArchiveRoom(self)
    }
    
    open func makeAdmin(member: MXRoomMember) {
        room?.setPowerLevel(ofUser: member.userId, powerLevel: 100) { response in
        }
    }
    
    open func makeModerator(member: MXRoomMember) {
        room?.setPowerLevel(ofUser: member.userId, powerLevel: 50) { response in
        }
    }
    
    open func viewMemberProfile(member: MXRoomMember) {
        let profileVc = ProfileViewController()
        profileVc.friend = FriendsManager.shared.getOrCreate(member, room?.mxSession)

        navigationController?.pushViewController(profileVc, animated: true)
    }
    
    open func unmakeModerator(member: MXRoomMember) {
        room?.setPowerLevel(ofUser: member.userId, powerLevel: 0) { response in
        }
    }
    
    open func kickMember(member: MXRoomMember) {
        room?.kickUser(member.userId, reason: "") { response in
            // TODO - Handle error and maybe allow editing of a reason?
        }
    }
}

// MARK: ChooseFriendsDelegate
extension RoomSettingsViewController: ChooseFriendsDelegate {

    public func friendsChosen(_ friends: [Friend]) {
        // Invite!
        if let room = room, room.canSendInvites() {
            for friend in friends {
                let invitee = MXRoomInvitee.userId(friend.matrixId)
                room.invite(invitee) { reponse in
                }
            }
        }
    }
}

