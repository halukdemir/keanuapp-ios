//
//  RoomMembersDataSource.swift
//  Keanu
//
//  Created by N-Pex on 19.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixKit
import KeanuCore

/**
 Data source for all room members.
 
 Use the delegate to customize cell rendering and table view animations.
 */
open class RoomMembersDataSource: NSObject, UITableViewDataSource {
    
    public var delegate:RoomMembersDataSourceDelegate?
    public var section:UInt = 0
    
    /**
     The members data that is displayed by this data source.
     */
    var roomMembers:[MXRoomMember] = []
    
    /**
     Mapping from userId to MXRoomMember. Used when updating list to see if a member is added etc.
     */
    var roomMemberMapping:[String:MXRoomMember] = [:]
    
    var room:MXRoom
    var roomListener:Any?
    
    // Notification center listener objects
    open var observers:[NSObjectProtocol] = []
    
    init(room:MXRoom) {
        self.room = room
        super.init()
        
        // Add listener to room events, so we can dynamically update the table view when members are joined/invited/kicked etc.
        room.liveTimeline { (timeline) in
            if let timeline = timeline {
                let eventTypes:[MXEventType] = [.roomMember, .roomThirdPartyInvite, .roomPowerLevels]
                self.roomListener = timeline.listenToEvents(eventTypes, { (event:MXEvent, direction:MXTimelineDirection, state:MXRoomState) in
                    guard direction == MXTimelineDirection.forwards else {return}
                    switch event.eventType {
                    case __MXEventTypeRoomMember:
                        guard let userId = event.stateKey else {return}
                        
                        // Note: use timeline.state, not state. The "state" param contains the state BEFORE the event.
                        if let member = timeline.state.members.member(withUserId: userId) {
                            self.handleMember(member: member)
                            self.resort()
                            if let delegate = self.delegate {
                                delegate.didChangeAllRows()
                            }
                        }
                        break
                    case __MXEventTypeRoomPowerLevels:
                        if let delegate = self.delegate {
                            self.resort()
                            delegate.didChangeAllRows()
                        }
                        break
                    case __MXEventTypeRoomThirdPartyInvite:
                        //TODO - How to handle these?
                        break
                    default: break
                    }
                })
            }
        }
        
        // Load room members, if needed
        reloadData()
    }
    
    open func reloadData() {
        room.members({ (loadedRoomMembers) in
            self.roomMembers.removeAll()
            loadedRoomMembers?.members.forEach({ (member) in
                self.handleMember(member: member)
            })
            self.resort()
            if let delegate = self.delegate {
                delegate.didChangeAllRows()
            }
        }, lazyLoadedMembers: { (lazyLoadedRoomMembers) in
            lazyLoadedRoomMembers?.members.forEach({ (member) in
                self.handleMember(member: member)
            })
            self.resort()
            if let delegate = self.delegate {
                delegate.didChangeAllRows()
            }
        }, failure: { (error) in
            // TODO
        })
    }
    
    /**
     Do the actual adding of a new member/removal of old member from the data set, based on the membership for the room member. First remove any old data (if present) then add the new.
     */
    open func handleMember(member:MXRoomMember) {
        let allowedMemberships:[MXMembership] = [MXMembership.join, MXMembership.invite]
        
        // Remove previous data for this user/member
        if let previouslyAddedMember = self.roomMemberMapping[member.userId], let index = self.roomMembers.firstIndex(of: previouslyAddedMember) {
            self.roomMembers.remove(at: index)
            self.roomMemberMapping.removeValue(forKey: member.userId)
        }
        
        // Add the new data (based on the new membership value)
        guard allowedMemberships.contains(member.membership) else {return}
        self.roomMembers.append(member)
        self.roomMemberMapping[member.userId] = member
    }
    
    deinit {
        self.observers.forEach { NotificationCenter.default.removeObserver($0) }
        room.liveTimeline { (timeline) in
            if let timeline = timeline, let listener = self.roomListener {
                timeline.removeListener(listener)
            }
            self.roomListener = nil
        }
    }
    
    /**
     Order first by admins,moderators,others, then by display name.
     */
    open func resort() {
        RoomMembersDataSource.sortMembers(members: &roomMembers, inRoom: self.room)
    }
    
    /**
     Order first by admins, moderators, others, then by friendly name.

     - parameter members: Array of room members to sort.
     - parameter room: The room, the members are in.
     */
    open class func sortMembers(members: inout [MXRoomMember], inRoom room: MXRoom) {
        members.sort { (member1, member2) -> Bool in
            let user1Admin = room.isAdmin(userId: member1.userId)
            let user2Admin = room.isAdmin(userId: member2.userId)
            switch (user1Admin, user2Admin) {
            case (true, false):
                return true
            case (false, true):
                return false
            default: break
            }

            let user1Moderator = room.isModerator(userId: member1.userId)
            let user2Moderator = room.isModerator(userId: member2.userId)
            switch (user1Moderator, user2Moderator) {
            case (true, false):
                return true
            case (false, true):
                return false
            default: break
            }

            // BUGFIX: Using `member2.displayname` directly, can lead to an
            // unexpected nil runtime exception. Additionally, we want to use the
            // friendly name (which can be user-defined), as much as possible
            // anyway.
            return FriendsManager.shared.getOrCreate(member1).name
                .compare(FriendsManager.shared.getOrCreate(member2).name) == .orderedAscending
        }
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return roomMembers.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let delegate = self.delegate, let cell = delegate.createCell(at: indexPath, for: roomMembers[indexPath.row]) {
            return cell
        }
        return UITableViewCell() // Fallback to avoid crash
    }
    
    public func roomMember(at indexPath: IndexPath) -> MXRoomMember? {
        if indexPath.section == self.section, indexPath.row >= 0, indexPath.row < roomMembers.count {
            return roomMembers[indexPath.row]
        }
        return nil
    }
}


