//
//  AuthenticationViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 12.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import Eureka
import MatrixKit
import AFNetworking
import KeanuCore

open class AuthenticationViewController: FormViewController, RecaptchaAndTermsViewControllerDelegate {
    
    // MARK: Constants
    
    static let defaultHomeServerUrl = "https://\(Config.defaultHomeServer)"
    static let defaultIdServerUrl = "https://\(Config.defaultIdServer)"

    
    // MARK: public properties
    
    /**
     Safe some boilerplate by reuse of `MXKAuthenticationViewControllerDelegate`.
     Let's see, if that will do. :-)
    */
    open var delegate: MXKAuthenticationViewControllerDelegate?

    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    // MARK: private properties
    
    private let authType: MXKAuthenticationType
    private var client: MXRestClient?
    private var currentOp: MXHTTPOperation?
    private var authSession: MXAuthenticationSession?

    private var nextStage: MXLoginFlowType? {
        if let next = authSession?.flows?.first?.stages?.filter({ !(authSession?.completed?.contains($0) ?? false) }).first {
            return MXLoginFlowType(identifier: next)
        }

        return nil
    }

    private var recaptchaSecret: String?
    private var termsAccepted = false
    
    /**
     Since we are trimming the user name, we remember what the user actually entered and use that as the display name for the account.
     */
    private var userEnteredName: String?

    /**
     The username.
     
     This is basically a proxy for `form.rowBy(tag: "username")` which also trims.
     */
    private var username: String {
        get {
            if let username = (form.rowBy(tag: "username") as? AccountRow)?.value {
                let allowedCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789=_-./"
                var name = username.filter { allowedCharacters.contains($0) }
                if name.count == 0 {
                    // No characters left, generate a random one!
                    name = generateUserName()
                }
                return name
            }
            
            return ""
        }
        set {
            if let username = form.rowBy(tag: "username") as? AccountRow {
                username.value = newValue
                username.reload()
            }
        }
    }

    /**
     The password.
     
     This is a proxy for `form.rowBy(tag: "password")`.
     */
    private var password: String {
        get {
            if let password = (form.rowBy(tag: "password") as? PasswordRow)?.value {
                return password
            }
            
            return ""
        }
        set {
            if let password = form.rowBy(tag: "password") as? PasswordRow {
                password.value = newValue
                password.reload()
            }
        }
    }
    
    private var homeServer: URL {
        get {
            if let homeServer = (form.rowBy(tag: "home_server") as? URLRow)?.value {
                return homeServer
            }
            
            return URL(string: AuthenticationViewController.defaultHomeServerUrl)!
        }
        set {
            if let homeServer = form.rowBy(tag: "home_server") as? URLRow {
                homeServer.value = newValue
                homeServer.reload()
            }
        }
    }

    private var idServer: URL {
        get {
            if let idServer = (form.rowBy(tag: "id_server") as? URLRow)?.value {
                return idServer
            }
            
            return URL(string: AuthenticationViewController.defaultIdServerUrl)!
        }
        set {
            if let idServer = form.rowBy(tag: "id_server") as? URLRow {
                idServer.value = newValue
                idServer.reload()
            }
        }
    }

    private let checkRowValidity = { (cell: BaseCell, row: BaseRow) in
        cell.backgroundColor = row.isValid ? .white : .orange
    }


    public init(_ type: MXKAuthenticationType) {
        self.authType = type
        super.init(style: .grouped)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        self.authType = MXKAuthenticationType(rawValue: UInt32(aDecoder.decodeInt32(forKey: "type")))
        
        super.init(coder: aDecoder)
    }
    
    override open func encode(with aCoder: NSCoder) {
        aCoder.encode(Int32(self.authType.rawValue), forKey: "type")
        
        super.encode(with: aCoder)
    }
    
    
    // MARK: UIViewController

    override open func viewDidLoad() {
        super.viewDidLoad()
        
        form
        +++ Section()
            
            <<< LabelRow("errors") {
                $0.hidden = true
            }
            .cellUpdate() { cell, row in
                cell.textLabel?.textColor = .orange
                cell.textLabel?.numberOfLines = 0
            }
            
            <<< AccountRow("username") {
                $0.placeholder = "User Name".localize()
                $0.add(rule: RuleRequired())
            }
            .onCellHighlightChanged() { _, row in
                // Check username availability on registration.
                if self.authType == MXKAuthenticationTypeRegister {
                    if !self.username.isEmpty {
                        self.client?.isUserNameInUse(self.username) { used in
                            if used {
                                self.showError("This username is already taken.".localize())
                            }
                            else {
                                self.showError(false)
                            }
                        }
                    }
                }
            }
            .onRowValidationChanged(checkRowValidity)
            
        if authType == MXKAuthenticationTypeRegister {
            form.first!.footer = HeaderFooterView(stringLiteral:
                "Think of a unique nickname that you don't use anywhere else and doesn't contain personal information."
                    .localize())
            
            form
            +++ Section(footer:
                "Make sure your password is a unique password you don't use anywhere else."
                    .localize())
        }
        
        form.last!
            <<< PasswordRow("password") {
                $0.placeholder = "Password".localize()
                $0.add(rule: RuleRequired())
                
                if authType == MXKAuthenticationTypeRegister {
                    $0.add(rule: RuleMinLength(minLength: 8))
                }
            }
            .cellSetup() { cell, row in
                cell.accessoryType = .detailButton
            }
            .onRowValidationChanged(checkRowValidity)
        
        if authType == MXKAuthenticationTypeRegister {
            form.last!
                <<< PasswordRow("confirm") {
                    $0.placeholder = "Confirm".localize()
                    $0.add(rule: RuleRequired())
                    $0.add(rule: RuleEqualsToRow(form: form, tag: "password"))
                    
                    if authType == MXKAuthenticationTypeRegister {
                        $0.add(rule: RuleMinLength(minLength: 8))
                    }
                }
                .cellSetup() { cell, row in
                    cell.accessoryType = .detailButton
                }
                .onRowValidationChanged(checkRowValidity)
        }

        form
        +++ Section()
        <<< SwitchRow("advanced_options") {
                $0.title = "Show Advanced Options".localize()
                $0.value = false
            }
            .onChange() { _ in self.restartSession() }
   
        form
        +++ Section("Server".localize()) {
            $0.hidden = "$advanced_options == false"
        }
            <<< URLRow("home_server") {
                $0.title = "Home Server".localize()
                $0.placeholder = AuthenticationViewController.defaultHomeServerUrl
                $0.value = URL(string: AuthenticationViewController.defaultHomeServerUrl)
                $0.add(rule: RuleURL())
            }
            .onCellHighlightChanged() { _,_  in self.restartSession() }

            <<< SwitchRow("customIdServer") {
                $0.title = "Use Custom Identity Server".localize()
                $0.value = false
            }

            <<< URLRow("id_server") {
                $0.title = "Identity Server".localize()
                $0.placeholder = AuthenticationViewController.defaultIdServerUrl
                $0.value = URL(string: AuthenticationViewController.defaultIdServerUrl)
                $0.hidden = "$customIdServer == false"
                $0.add(rule: RuleURL())
            }
            .onCellHighlightChanged() { _,_  in self.restartSession() }

        if authType == MXKAuthenticationTypeRegister {
            navigationItem.title = "Sign Up".localize()
        } else {
            navigationItem.title = "Sign In".localize()
        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
                                                            target: self,
                                                            action: #selector(onDone))
    }

    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        restartSession()
    }
    

    // MARK: UITableViewDelegate
    
    /**
     The accessory button should only be on the password fields, so we assume that one was tapped.
     
     Toggles password visibility on tap.
     */
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? PasswordCell {
            cell.textField.isSecureTextEntry = !cell.textField.isSecureTextEntry
        }
    }

    
    // MARK: RecaptchaAndTermsViewControllerDelegate
    
    /**
     User returned successfully from the Recaptcha solving and terms accepting.
     
     We can directly progress with sign in/up.
     
     - parameter secret: The secret from the solved Recaptcha.
    */
    func success(_ secret: String?) {
        recaptchaSecret = secret
        termsAccepted = true
        
        onDone()
    }

    
    // MARK: Actions

    /**
     The user pressed the "Sign In"/"Sign Up" button, which should have been
     possible only, if it is enabled because all credential fields are
     sufficiently filled.
    */
    @objc func onDone() {
        guard form.validate().isEmpty,
            let nextStage = nextStage else {
            return
        }

        print("[AuthenticationViewController] #onDone: nextStage=\(nextStage)")

        var auth = [String: Any]()

        switch nextStage {
        case .password:
            auth["type"] = kMXLoginFlowTypePassword
            auth["identifier"] = [
                "type": kMXLoginIdentifierTypeUser,
                "user": username,
            ]
            auth["password"] = password

        case .recaptcha:
            if (recaptchaSecret ?? "").isEmpty {
                return showRecaptchaAndTerms()
            }

            auth["type"] = kMXLoginFlowTypeRecaptcha
            auth["response"] = recaptchaSecret ?? ""

        case .dummy:
            auth["type"] = kMXLoginFlowTypeDummy

        case .other(kMXLoginFlowTypeTerms):
            if !termsAccepted {
                return showRecaptchaAndTerms()
            }

            auth["type"] = kMXLoginFlowTypeTerms

        default:
            return showUnsupportedError()
        }
        
        // Save the actual username entered, so we can use that as display name
        if let username = (form.rowBy(tag: "username") as? AccountRow)?.value {
            userEnteredName = username.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        let callback = opCompletion() { (data: [String: Any]) in

            // For an unkown reason MXJSONModel.model(fromJSON:) isn't exposed to Swift anymore.
            if let loginResponse = MXLoginResponse.models(fromJSON: [data])?.first as? MXLoginResponse {
                self.onSuccesfulSignIn(MXCredentials(
                    loginResponse: loginResponse,
                    andDefaultCredentials: self.client?.credentials))
            }
        }
        
        if authType == MXKAuthenticationTypeLogin {
            currentOp?.cancel()
            
            if auth.keys.contains("password") {
                auth["initial_device_display_name"] = generateDeviceName()
            }
            
            currentOp = client?.login(parameters: auth, completion: callback)
        }
        else if authType == MXKAuthenticationTypeRegister {
            currentOp?.cancel()
            
            auth["session"] = authSession?.session ?? ""
            
            let parameters: [String : Any] = [
                "auth": auth,
                "username": username,
                "password": password,
                "bind_email": false,
                "bind_msisdn": false,
                "initial_device_display_name": generateDeviceName()
            ]
            
            currentOp = client?.register(parameters: parameters, completion: callback)
        }
    }
    
    
    // MARK: Private methods
    
    /**
     We don't want to expose the device name (UIDevice.name) so generate a random one to use when registering with Matrix.
     */
    private func generateDeviceName() -> String {
        return Bundle.main.displayName + "-" + String.randomAlphanumericString(length: 8)
    }

    /**
     If the user enters only non-alpha characters, generate a random username.
     */
    private func generateUserName() -> String {
        let username = Bundle.main.displayName + "-" + String.randomNumericString(length: 6)

        // Return filtered one (app name can contain illegal chars!)
        let allowedCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789=_-./"
        return username.filter { allowedCharacters.contains($0) }
    }

    
    /**
     Starts a new session with the currently set home server.
     
     You should call this on scene appearance or when the user changes the
     home server.
    */
    private func restartSession() {
        var changed = false
        
        if homeServer.absoluteString != client?.homeserver {
            changed = true
            client = MXRestClient(homeServer: homeServer, unrecognizedCertificateHandler: nil)
        }
        
        if idServer.absoluteString != client?.identityServer {
            changed = true
            client?.identityServer = idServer.absoluteString
        }

        if changed {
            currentOp?.cancel()
            authSession = nil
            showError(false)

            let callback = opCompletion() { (session: MXAuthenticationSession) in
                self.authSession = self.filterAuthSession(session)
                
                if self.authSession == nil {
                    self.showUnsupportedError()
                }
            }

            if authType == MXKAuthenticationTypeLogin {
                currentOp = client?.getLoginSession(completion: callback)
            }
            else if authType == MXKAuthenticationTypeRegister {
                currentOp = client?.getRegisterSession(completion: callback)
            }
        }
    }
    
    /**
     Encapsulates network request handling.
     
     - Shows "working" overlay as long as the request goes.
     - Displays errors to the user in a `LabelRow` above the username.
     - Calls through to your provided success callback, if there is no error
       and there actually is a returned object.
     - If there is a 401 error, that's most probably due to a multi-stage authentication, so if that happens,
       the next stage request is triggered instead of anything else.
     
     - parameter success: Callback in case of successful request.
     - parameter value: The data returned by the server.
     - returns: Completion callback suitable for some `MXRestClient` methods.
    */
    private func opCompletion<T>(_ success: @escaping (_ value: T) -> Void)
        -> ((_ response: MXResponse<T>) -> Void) {
            
        workingOverlay.isHidden = false

        return { response in
            self.currentOp = nil

            let userInfo = (response.error as NSError?)?.userInfo

            // We get 401's when the authentication is multi-stage and we didn't complete all stages.
            if (userInfo?[AFNetworkingOperationFailingURLResponseErrorKey] as? HTTPURLResponse)?.statusCode == 401 {
                if let data = userInfo?[MXHTTPClientErrorResponseDataKey] as? [AnyHashable: Any],
                    let session = MXAuthenticationSession(fromJSON: data) {

                    self.authSession = self.filterAuthSession(session)

                    if self.nextStage != nil {

                        // Do next stage.
                        DispatchQueue.main.async { self.onDone() }

                        return
                    }
                }
            }

            self.workingOverlay.isHidden = true
            
            if response.isFailure || response.error != nil || response.value == nil {
                if response.error != nil {
                    self.showError(response.error!)
                }
                else if response.value == nil {
                    self.showError("Server didn't return anything!".localize())
                }
                else {
                    self.showError()
                }
            }
            else {
                success(response.value!)
            }
        }
    }

    /**
     Adds an account to the account manager, if not already in it.
     
     Informs the delegate about successful sign in.
     
     - parameter credentials: The `MXCredentials` as returned by the server.
     */
    private func onSuccesfulSignIn(_ credentials: MXCredentials) {
        print("[\(String(describing: type(of: self)))] credentials accessToken=\(credentials.accessToken ?? "nil") deviceId=\(credentials.deviceId ?? "nil") homeServer=\(credentials.homeServer ?? "nil") homeServerName=\(credentials.homeServerName() ?? "nil") userId=\(credentials.userId ?? "nil")")
        
        if let manager = MXKAccountManager.shared() {
            // Sanity check: check whether the user is not already logged in with this ID.
            if manager.account(forUserId: credentials.userId) == nil {
                // Report the new account in account manager.
                if let account = MXKAccount(credentials: credentials) {
                    account.identityServerURL = client?.identityServer
                    
                    manager.addAccount(account, andOpenSession: true)

                    // Use whatever the user entered as display name
                    if let displayName = userEnteredName, !displayName.isEmpty {
                        do {
                            // Need to wait for a session before setting display name!
                            try account.performOnSessionReady({ _ in
                                account.setUserDisplayName(displayName, success: nil, failure:nil)
                            })
                        } catch {}
                    }
                }
            }
            
            // The delegate should probably dismiss us now.
            delegate?.authenticationViewController(nil, didLogWithUserId: username)
        }
    }

    /**
     Shows an error, explaining that this app doesn't support that server's authentication process.
     */
    private func showUnsupportedError() {
        let tx1: String

        if self.authType == MXKAuthenticationTypeLogin {
            tx1 = "This app currently doesn't support this server's sign in process."
                .localize()
        }
        else {
            tx1 = "This app currently doesn't support this server's sign up process."
                .localize()
        }

        let tx2 = "Please choose another server!".localize()

        showError("\(tx1)\n\(tx2)")
    }
    
    /**
     Shows an `Error`s localized description to the user.
     
     - parameter error: The occured `Error` to show.
    */
    private func showError(_ error: Error) {
        showError(error.localizedDescription, true)
    }
    
    /**
     Shows an error text to the user.
     
     - parameter error: The error text to show.
    */
    private func showError(_ error: String) {
        showError(error, true)
    }
    
    /**
     Shows or hides an error to the user.
     
     If `toggle` is true, a standard text will be displayed.
     
     - parameter toggle: Show if true, hide if false.
    */
    private func showError(_ toggle: Bool) {
        showError(nil, toggle)
    }
    
    /**
     Shows an error text to the user, if `toggle` is true.
     
     - parameter error: The error text to show. If `nil`, a standard text will
        be displayed. Default is `nil`.
     - parameter toggle: Show if true, hide if false. Default is `true`.
     */
    private func showError(_ error: String? = nil, _ toggle: Bool = true) {
        if let errors = form.rowBy(tag: "errors") {
            if toggle {
                errors.title = error ?? "No details available.".localize()
                errors.reload()
                errors.hidden = false
            }
            else {
                errors.hidden = true
            }

            errors.evaluateHidden()
        }

    }
    
    /**
     Shows `RecaptchaAndTermsViewController` modaly.
    */
    private func showRecaptchaAndTerms() {
        let homeServer = client?.homeserver
        let recaptchaKey = (recaptchaSecret ?? "").isEmpty
            ? (authSession?.params?[kMXLoginFlowTypeRecaptcha] as? [String: String])?["public_key"]
            : nil
        let terms = termsAccepted
            ? nil
            : MXLoginTerms(fromJSON: authSession?.params[kMXLoginFlowTypeTerms] as? [AnyHashable : Any])

        present(RecaptchaAndTermsViewController(homeServer, recaptchaKey, terms, delegate: self), animated: true)
    }

    /**
     - parameter type: An array of `MXLoginFlowType`s as `String`s.
     - returns: if a given list of flow types is supported by this implementation or not.
     */
    private func isSupported(_ types: [String]?) -> Bool {
        if let types = types {
            for type in types {
                if !isSupported(type) {
                    return false
                }
            }
            
            
            return true
        }
        
        return false
    }
    
    /**
     - parameter type: A `MXLoginFlowType` as `String`.
     - returns: if a given flow type is supported by this implementation or not.
     */
    private func isSupported(_ type: String) -> Bool {
        switch MXLoginFlowType(identifier: type) {
        case .dummy, .password, .recaptcha, .other(kMXLoginFlowTypeTerms):
            return true
        default:
            print("[AuthenticationViewController] Login flow type \"\(type)\" is not supported.")
            return false
        }
    }

    /**
     - parameter authSession: An `MXAuthenticationSession` object, hopefully
     containing information about the login flows the server supports.
     - returns: `nil`, if no flow is supported or the server didn't provide a
     list of supported flows, the `authSession` object if all the flows are
     supported, or a copy of the `authSession` object which only contains the
     supported flows.
     */
    private func filterAuthSession(_ authSession: MXAuthenticationSession) -> MXAuthenticationSession? {
        
        var flows = [MXLoginFlow]()
        
        for flow in authSession.flows {
            if let type = flow.type {
                if isSupported(type) {
                    if flow.stages == nil || flow.stages.isEmpty {
                        flow.stages = [type]
                        flows.append(flow)
                    }
                    else {
                        if isSupported(flow.stages) {
                            flows.append(flow)
                        }
                    }
                }
            }
            else {
                if isSupported(flow.stages) {
                    flows.append(flow)
                }
            }
        }
        
        if flows.isEmpty {
            return nil
        }
        
        if flows.count == authSession.flows.count {
            return authSession
        }
        
        let cleanedSession = MXAuthenticationSession()
        cleanedSession.completed = authSession.completed
        cleanedSession.session = authSession.session
        cleanedSession.params = authSession.params
        cleanedSession.flows = flows
        
        return cleanedSession
    }
}
