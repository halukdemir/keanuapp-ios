//
//  RecaptchaViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 30.09.19.
//  Copyright © 2018 - 2019 Guardian Project. All rights reserved.
//

import UIKit
import MatrixKit

/**
 `UIViewController` which only purpose it is to show a Recaptcha in a `WKWebView`.
 */
class RecaptchaViewController: UIViewController {

    private let homeServer: String
    private let publicKey: String

    init( _ homeServer: String, _ publicKey: String) {
        self.homeServer = homeServer
        self.publicKey = publicKey

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        homeServer = coder.decodeObject(forKey: "homeServer") as! String
        publicKey = coder.decodeObject(forKey: "publicKey") as! String

        super.init(coder: coder)
    }

    override func encode(with coder: NSCoder) {
        coder.encode(homeServer, forKey: "homeServer")
        coder.encode(publicKey, forKey: "publicKey")

        super.encode(with: coder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Solve Recaptcha".localize()

        let recaptchaWv = MXKAuthenticationRecaptchaWebView(frame: view.bounds)
        recaptchaWv.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(recaptchaWv)
        recaptchaWv.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        recaptchaWv.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        recaptchaWv.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        recaptchaWv.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        recaptchaWv.openRecaptchaWidget(withSiteKey: publicKey,
                                         fromHomeServer: homeServer)
        { response in
            if let secret = response,
                let navC = self.navigationController as? RecaptchaAndTermsViewController {

                navC.secret = secret
            }

            self.navigationController?.popViewController(animated: true)
        }
    }
}
