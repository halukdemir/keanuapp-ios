//
//  LeftAlignedCollectionViewFlowLayout.swift
//  Keanu
//
//  Created by Benjamin Erhart on 25.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit

/**
 Taken from [StackOverflow](https://stackoverflow.com/questions/22539979/left-align-cells-in-uicollectionview)
*/
public class LeftAlignedCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    override public func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributes = super.layoutAttributesForElements(in: rect)
        
        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0

        attributes?.forEach { layoutAttribute in
            if layoutAttribute.frame.origin.y >= maxY {
                leftMargin = sectionInset.left
            }
            
            layoutAttribute.frame.origin.x = leftMargin
            
            leftMargin += layoutAttribute.frame.width + minimumInteritemSpacing
            maxY = max(layoutAttribute.frame.maxY , maxY)
        }
        
        return attributes
    }
}
