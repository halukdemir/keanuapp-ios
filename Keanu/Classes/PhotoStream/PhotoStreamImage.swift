//
//  PhotoStreamImage.swift
//  Keanu
//
//  Created by N-Pex on 14.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import INSPhotoGallery
import MatrixKit
import KeanuCore

/**
 A class representing an image in the photo stream. TODO - needs to be polished.
 */
open class PhotoStreamImage: NSObject, INSPhotoViewable {
    public var image: UIImage?
    public var thumbnailImage: UIImage?
    let event: MXEvent
    let session: MXSession
    let attachment: MXKAttachment?
    private var operationQueue: OperationQueue?
    private var loadOperation: Operation?
    
    public var attributedTitle: NSAttributedString?
    
    public init(event:MXEvent,session:MXSession,operationQueue:OperationQueue) {
        self.event = event
        self.session = session
        self.attachment = MXKAttachment(event: event, andMediaManager: session.mediaManager)
        self.operationQueue = operationQueue
        super.init()
        
        let caption = NSMutableAttributedString()
        if let friend = FriendsManager.shared.get(event.sender) {
            caption.append(NSAttributedString(string: friend.name))
            caption.append(NSAttributedString(string: "\n"))
            let range = NSRange(location: 0, length: caption.length)
            let paragraph = NSMutableParagraphStyle()
            paragraph.alignment = .center
            caption.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.paragraphStyle: paragraph], range: range)
        }
        let timestampString = Formatters.format(date: date())
        caption.append(NSAttributedString(string: timestampString, attributes: [NSAttributedString.Key.foregroundColor : UIColor.white]))
        self.attributedTitle = caption
    }
    
    public func date() -> Date {
        return Date(timeIntervalSince1970: TimeInterval(event.ageLocalTs / 1000))
    }
    
    func releaseImages() {
        thumbnailImage = nil
        loadOperation?.cancel()
        loadOperation = nil
    }
    
    private func getCachedThumbnail() -> UIImage? {
        return self.thumbnailImage ?? self.attachment?.getCachedThumbnail()
    }
    
    public func loadImage(withPriority priority: Operation.QueuePriority, completion: @escaping (UIImage?, Error?) -> ()) {
        
        if let previousLoad = loadOperation, !previousLoad.isFinished {
            previousLoad.cancel()
        }
        loadOperation = BlockOperation(block: {
            self.attachment?.getImage({ (attachment: MXKAttachment?, image: UIImage?) in
                guard !(self.loadOperation?.isCancelled ?? true) else { return }
                DispatchQueue.main.async {
                    completion(image, nil)
                }
            }) { (attachment: MXKAttachment?, error: Error?) in
                guard !(self.loadOperation?.isCancelled ?? true) else { return }
                DispatchQueue.main.async {
                    completion(nil, error)
                }
            }
        })
        if let loadOperation = loadOperation {
            loadOperation.queuePriority = priority
            operationQueue?.addOperation(loadOperation)
        }
    }
    
    public func loadThumbnailImageWithSizeAndCompletionHandler(_ size:CGSize, completion: @escaping (UIImage?, Error?) -> ()) {
        
        let doneLoading:(() -> Bool) = {() in
            if let thumbnail = self.getCachedThumbnail() {
                DispatchQueue.main.async {
                    completion(thumbnail, nil)
                }
                return true
            }
            return false
        }
        if !doneLoading() {
            loadImage(withPriority: .normal, completion: { (image, error) in
                if let image = image {
                    self.thumbnailImage = image.aspectFill(size: size)
                }
                if !doneLoading() {
                    DispatchQueue.main.async {
                        completion(nil, nil)
                    }
                }
            })
        }
    }
    
    // MARK: INSPhotoViewable
    public func loadImageWithCompletionHandler(_ completion: @escaping (UIImage?, Error?) -> ()) {
        loadImage(withPriority: .high, completion: completion)
    }
    
    public func loadThumbnailImageWithCompletionHandler(_ completion: @escaping (UIImage?, Error?) -> ()) {
        completion(getCachedThumbnail(), nil)
    }
    
    public var isDeletable: Bool {
        return false
    }
}
