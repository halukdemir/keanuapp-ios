//
//  PhotoStreamViewController.swift
//  Keanu
//
//  Created by N-Pex on 18.0.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import UIKit
import INSPhotoGallery
import MatrixSDK

open class PhotoStreamViewController: UICollectionViewController, PhotosViewControllerDelegate {
    
    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    private var assetGridThumbnailSize:CGSize = CGSize()
    private var room: MXRoom?
    private var photoStreamHandler:PhotoStreamHandler?
    @IBOutlet weak var noPhotosView: UILabel!
    
    /**
     Instantiate a photo stream view controller for the given room. If room is nil, load photos from ALL rooms.
     */
    class func instantiate(room: MXRoom?) -> UIViewController? {
        
        if let navC = UIStoryboard.overriddenOrLocal(name: String(describing: self))
            .instantiateInitialViewController() as? UINavigationController {
            
            if let streamVc = navC.topViewController as? PhotoStreamViewController {
                streamVc.room = room
                streamVc.photoStreamHandler = PhotoStreamHandler()
            }
            
            return navC
        }
        return nil
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        noPhotosView.isHidden = true
        photoStreamHandler?.fetchImagesAsync(room: room, initialEvent: nil, onStart: {
            self.workingOverlay.message = "Loading... tap to cancel".localize()
            self.workingOverlay.tapHandler = {
                self.workingOverlay.isHidden = true
                self.photoStreamHandler?.cancelFetching()
                self.navigationController?.popViewController(animated: true)
            }
            self.workingOverlay.isHidden = false
        }, onEnd: { (images, initialImage) in
            self.noPhotosView.isHidden = (images.count > 0)
            self.workingOverlay.isHidden = true
            self.collectionView?.reloadData()
        })
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Determine the size of the thumbnails to request from the PHCachingImageManager
        let scale:CGFloat = UIScreen.main.scale
        let cellSize:CGSize = (self.collectionViewLayout as! UICollectionViewFlowLayout).itemSize;
        assetGridThumbnailSize = CGSize(width: cellSize.width * scale, height: cellSize.height * scale);
    }
    
    // Mark - UICollectionViewDataSource
    
    open override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let numImages = photoStreamHandler?.images.count ?? 0
        return numImages
    }
    
    open override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:PhotoStreamCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! PhotoStreamCell
        if let photo = photoStreamHandler?.images[indexPath.item] {
            cell.populateWithPhoto(photo)
        }
        return cell
    }
    
    open override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = self.collectionView?.cellForItem(at: indexPath)  as! PhotoStreamCell
        if let photoStreamHandler = photoStreamHandler {
            let initialPhoto = photoStreamHandler.images[indexPath.item]
            photoStreamHandler.initialImage = initialPhoto
            let browser = PhotosViewController(photoStreamHandler: photoStreamHandler, referenceView: cell.imageView)
            browser.delegate = self
            browser.referenceViewForPhotoWhenDismissingHandler = { photo in
                if let index = photoStreamHandler.images.firstIndex(where: {$0 === photo}) {
                    let indexPath = IndexPath(item: index, section: 0)
                    return (collectionView.cellForItem(at: indexPath) as? PhotoStreamCell)?.imageView
                }
                return nil
            }
            self.present(browser, animated: true, completion: nil)
        }
    }
    
    public func didDeletePhoto(photo: PhotoStreamImage) {
        if let photoStreamHandler = self.photoStreamHandler, let index = photoStreamHandler.images.firstIndex(where: {$0 === photo}) {
            photoStreamHandler.images.remove(at: index)
            let indexPath = IndexPath(item: index, section: 0)
            collectionView?.deleteItems(at: [indexPath])
        }
    }
    
    @IBAction func done() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
}
