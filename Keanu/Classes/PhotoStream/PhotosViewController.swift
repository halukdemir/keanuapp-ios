//
//  PhotosViewController.swift
//  Keanu
//
//  Created by N-Pex on 14.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import INSPhotoGallery
import MatrixSDK
import MatrixKit

/**
 A delegate protocol for the PhotosViewController.
 */
public protocol PhotosViewControllerDelegate {
    func didDeletePhoto(photo:PhotoStreamImage)
}

/**
 A view controller that shows all photos that have been downloaded for a room. You can swipe through the images and also zoom etc.
 */
open class PhotosViewController: INSPhotosViewController {
    open var delegate: PhotosViewControllerDelegate?

    /**
     Hold on to the photo stream handler with a strong reference, so it is not released!
     */
    var photoStreamHandler: PhotoStreamHandler?
    
    convenience init(photoStreamHandler: PhotoStreamHandler, referenceView: UIView?) {
        self.init(photos: photoStreamHandler.images, initialPhoto: photoStreamHandler.initialImage, referenceView: referenceView)
        self.photoStreamHandler = photoStreamHandler
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        super.deletePhotoHandler = {
            (photo: INSPhotoViewable?) -> Void in
            if let currentPhoto = photo as? PhotoStreamImage {
                currentPhoto.releaseImages()
                //TODO - delete
                if let delegate = self.delegate {
                    delegate.didDeletePhoto(photo: currentPhoto)
                }
            }
        }
        if let overlayView = PhotoOverlayView.nib
            .instantiate(withOwner: nil, options: nil)[0] as? INSPhotosOverlayViewable {
            
            self.overlayView = overlayView
        }
    }
}

