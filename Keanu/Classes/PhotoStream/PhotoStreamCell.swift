//
//  PhotoStreamCell.swift
//  Keanu
//
//  Created by N-Pex on 18.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import UIKit

open class PhotoStreamCell: UICollectionViewCell {
    @IBOutlet weak var imageView:UIImageView!
    private var photo:PhotoStreamImage?
    
    override open func prepareForReuse() {
        super.prepareForReuse()
        self.imageView.image = nil
        photo?.releaseImages()
        self.photo = nil
    }
    
    open func populateWithPhoto(_ photo: PhotoStreamImage) {
        self.photo = photo
        photo.loadThumbnailImageWithSizeAndCompletionHandler(imageView.bounds.size, completion: { (image, error) in
            self.imageView.image = image
        })
    }
}
