//
//  TestEventGenerator.swift
//  Keanu
//
//  Created by N-Pex on 03.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import MatrixSDK

/**
 A development class to simplify testing of different UI stuff, mainly the room view controller. Will start a timer and regularly generate a random MXEvent and add that to the given timeline (or rather, just call the listeners on the timeline).
 
 The JSON test data for the events are taken from here: https://github.com/matrix-org/matrix-doc/tree/master/event-schemas/examples
 */

public class TestEventGenerator {
    
    var timeline:MXEventTimeline
    var timer : Timer?
    
    init(timeline:MXEventTimeline) {
        self.timeline = timeline
        
        //TEMP TEMP TEMP just for testing
        let state = MXRoomState(roomId: "1337", andMatrixSession: nil, andDirection: true)
        self.timeline.setState(state)
        self.timeline.perform(#selector(cloneState(_:)), with: __MXTimelineDirectionForwards)
        startTimer()
    }
    
    deinit {
        stopTimer()
    }
    
    @objc func cloneState(_ direction:__MXTimelineDirection) {
        
    }
    
    func startTimer()
    {
        guard timer == nil else { return }
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerFired(timer:)), userInfo: nil, repeats: true)
    }
    
    func stopTimer()
    {
        guard timer != nil else { return }
        timer!.invalidate()
        timer = nil
    }
    
    @objc func timerFired(timer:Timer?) {
        let jsonArray:[String] = ["""
{
        "$ref": "core/room_event.json",
        "type": "m.room.message",
        "content": {
            "body": "This is an example text message",
            "msgtype": "m.text",
            "format": "org.matrix.custom.html",
            "formatted_body": "<b>This is an example text message</b>"
        }
        }
""", """
{
"$ref": "core/state_event.json",
"type": "m.room.name",
"state_key": "",
"content": {
"name": "The room name"
}
}
""", """
{
"$ref": "core/state_event.json",
"state_key": "@alice:example.org",
"type": "m.room.member",
"content": {
"membership": "join",
"avatar_url": "mxc://example.org/SEsfnsuifSDFSSEF#auto",
"displayname": "Alice Margatroid"
}
}
""", """
{
  "$ref": "core/state_event.json",
  "type": "m.room.topic",
  "state_key": "",
  "content": {
    "topic": "A room topic"
  }
}
""", """
{
  "$ref": "core/state_event.json",
  "state_key": "example.org",
  "type": "m.room.aliases",
  "content": {
    "aliases": ["#somewhere:example.org", "#another:example.org"]
  }
}
""", """
{
"$ref": "core/state_event.json",
"type": "m.room.encryption",
"state_key": "",
"content": {
"algorithm": "m.megolm.v1.aes-sha2",
"rotation_period_ms": 604800000,
"rotation_period_msgs": 100
}
}
""", """
{
"$ref": "core/room_edu.json",
"type": "m.typing",
"content": {
"user_ids": ["@alice:matrix.org", "@bob:example.com"]
}
}
"""
        ]
        
        let randomIndex = Int.random(in: 0 ..< jsonArray.count)
        if let data = jsonArray[randomIndex].data(using: String.Encoding.utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]

                if let json = json,
                    // For an unkown reason MXEvent.model(fromJSON:) isn't exposed to Swift anymore.
                    let event = MXEvent.models(fromJSON: [json])?.first as? MXEvent {

                    self.timeline.notifyListeners(event, direction: __MXTimelineDirectionForwards)
                }
            } catch {
                print("Something went wrong")
            }
        }
    }
}

