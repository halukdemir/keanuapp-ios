//
//  AttachmentPicker.swift
//  Keanu
//
//  Created by N-Pex on 15.10.18.
//  Copyright © 2018 GuardianProject. All rights reserved.
//

import UIKit
import MatrixSDK

open class AttachmentPicker: UIView {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    @IBOutlet open weak var actionsStackView:UIStackView!
    public var defaultHeight:CGFloat = 100
    
    /**
     We create a prototype for action buttons in IB and bind it here. Then, when adding actions, we create copies of this prototype, so that all actions get the same look. Note: don't make this weak, or we'll lose it on clearActions()!
     */
    @IBOutlet open var actionButtonPrototype:UIButton!
    
    private var buttonActionMap:[UIControl:(() -> Void)] = [:]
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        defaultHeight = self.frame.size.height
        clearActions() // Remove design time stuff
    }
    
    open func clearActions() {
        actionsStackView.subviews.forEach({ $0.removeFromSuperview() })
        buttonActionMap.removeAll()
    }
    
    open func actionCount() -> Int {
        return actionsStackView.arrangedSubviews.count
    }
    
    open func actions() -> [UIButton] {
        return actionsStackView.arrangedSubviews.compactMap({ (view) -> UIButton? in
            return view as? UIButton
        });
    }
    
    open func addAction(_ label:String, at: Int? = nil, callback:(() -> Void)?) -> UIButton? {
        guard label.count > 0,
            let callback = callback,
            let actionButtonPrototype = actionButtonPrototype else {
            return nil
        }
        
        // Can't just do UIButton.copy, so use a workaround inspired by this: https://medium.com/@superpeteblaze/ios-quick-tip-how-to-duplicate-a-uibutton-using-swift-2952b0d5ce28
        let archivedButton = NSKeyedArchiver.archivedData(withRootObject: actionButtonPrototype)
        guard let button = NSKeyedUnarchiver.unarchiveObject(with: archivedButton) as? UIButton else { return nil }
        button.titleLabel?.font = actionButtonPrototype.titleLabel?.font
        button.layer.cornerRadius = actionButtonPrototype.layer.cornerRadius
        button.layer.borderColor = actionButtonPrototype.layer.borderColor
        button.layer.borderWidth = actionButtonPrototype.layer.borderWidth
        
        buttonActionMap[button] = callback
        button.setTitle(label, for: .normal)
        button.addTarget(self, action: #selector(actionTapped(sender:)), for:.touchDown)
        if let at = at {
         actionsStackView.insertArrangedSubview(button, at: at)
        } else {
            actionsStackView.addArrangedSubview(button)
        }
        return button
    }
    
    @objc open func actionTapped(sender:Any?) {
        if let control = sender as? UIControl, let action = buttonActionMap[control] {
            action()
        }
    }
}
