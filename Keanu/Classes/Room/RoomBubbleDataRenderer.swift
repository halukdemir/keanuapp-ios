//
//  RoomBubbleDataRenderer.swift
//  Keanu
//
//  Created by N-Pex on 16.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

/**
 This protocol must be implemented by all table view cells displaying bubble data.
 */
public protocol RoomBubbleDataRenderer {
    func render(roomBubbleData:RoomBubbleData, delegate:RoomBubbleDataRendererDelegate?)
}
