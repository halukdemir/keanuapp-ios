//
//  BubbleLayer.swift
//  Keanu
//
//  Created by N-Pex on 04.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import CoreFoundation

public enum BubbleViewType {
    case none
    case incoming
    case outgoing
    case incomingMask
    case outgoingMask
}

extension UIView {
    
    public var bubbleLayer:BubbleLayer? {
        get {
            if let sublayers = self.layer.sublayers {
                for layer in sublayers {
                    if let layer = layer as? BubbleLayer {
                        return layer
                    }
                }
            }
            return nil
        }
    }
    
    /**
     Set a chat bubble layer as background.
     - Parameter type: The bubble type to use.
     */
    public func setBubbleView(_ type:BubbleViewType) {
        // Remove old bubble layer, if any
        if let oldBubble = self.bubbleLayer {
            oldBubble.removeFromSuperlayer()
        }
        if type != .none {
            let bubbleLayer = BubbleLayer(view:self, type:type)
            self.layer.insertSublayer(bubbleLayer, at: 0)
        }
    }
    
    func setBubbleMask(_ type:BubbleViewType) {
        if type == .none {
            self.layer.mask = nil
            return
        }
        let bubbleLayer = BubbleLayer(view:self, type:type)
        self.layer.mask = bubbleLayer
    }
}

/**
 Implements a bubble view as a sublayer of a given target view.
 
 Layer path from here: https://medium.com/@dima_nikolaev/creating-a-chat-bubble-which-looks-like-a-chat-bubble-in-imessage-the-advanced-way-2d7497d600ba
 */
public class BubbleLayer: CAShapeLayer {
    var view:UIView
    var type:BubbleViewType
    
    public override init(layer: Any) {
        let layer = layer as! BubbleLayer
        self.view = layer.view
        self.type = layer.type
        super.init(layer: layer)
        typeUpdated()
        self.needsDisplayOnBoundsChange = true
        self.frame = view.frame
    }
    
    init(view:UIView,type:BubbleViewType) {
        self.view = view
        self.type = type
        super.init()
        typeUpdated()
        self.needsDisplayOnBoundsChange = true
        self.frame = view.frame
    }
    
    private func typeUpdated() {
        switch type {
        case .incoming:
            let color = UIColor.white
            self.fillColor = color.cgColor
            self.strokeColor = color.cgColor
            self.lineWidth = CGFloat(0.0)
            break
        case .outgoing:
            let color = UIColor.white
            self.fillColor = color.cgColor
            self.strokeColor = color.cgColor
            self.lineWidth = CGFloat(0.0)
            break
        case .incomingMask, .outgoingMask:
            self.fillColor = UIColor.white.cgColor
            self.strokeColor = UIColor.black.cgColor
            self.lineWidth = CGFloat(2.0)
            break
        default:
            break
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func layoutSublayers() {
        super.layoutSublayers()
        if let superlayer = superlayer {
            self.frame = superlayer.bounds
        }
        updatePath()
    }
    
    public func updatePath() {
        // Need to make sure the whole path is inside frame. Since lineWidth/2 will stick out to the left, right, top and bottom, we just subtract lineWidth from both width and height.
        let width = frame.size.width - lineWidth
        let height = frame.size.height - lineWidth
        
        switch type {
        case .incoming, .incomingMask:
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: 22, y: height))
            bezierPath.addLine(to: CGPoint(x: width - 17, y: height))
            bezierPath.addCurve(to: CGPoint(x: width, y: height - 17), controlPoint1: CGPoint(x: width - 7.61, y: height), controlPoint2: CGPoint(x: width, y: height - 7.61))
            bezierPath.addLine(to: CGPoint(x: width, y: 17))
            bezierPath.addCurve(to: CGPoint(x: width - 17, y: 0), controlPoint1: CGPoint(x: width, y: 7.61), controlPoint2: CGPoint(x: width - 7.61, y: 0))
            bezierPath.addLine(to: CGPoint(x: 21, y: 0))
            bezierPath.addCurve(to: CGPoint(x: 4, y: 17), controlPoint1: CGPoint(x: 11.61, y: 0), controlPoint2: CGPoint(x: 4, y: 7.61))
            bezierPath.addLine(to: CGPoint(x: 4, y: height - 11))
            bezierPath.addCurve(to: CGPoint(x: 0, y: height), controlPoint1: CGPoint(x: 4, y: height - 1), controlPoint2: CGPoint(x: 0, y: height))
            bezierPath.addLine(to: CGPoint(x: -0.05, y: height - 0.01))
            bezierPath.addCurve(to: CGPoint(x: 11.04, y: height - 4.04), controlPoint1: CGPoint(x: 4.07, y: height + 0.43), controlPoint2: CGPoint(x: 8.16, y: height - 1.06))
            bezierPath.addCurve(to: CGPoint(x: 22, y: height), controlPoint1: CGPoint(x: 16, y: height), controlPoint2: CGPoint(x: 19, y: height))
            bezierPath.close()
            // Offset by half line width, so everything fits
            let transform = CGAffineTransform(translationX: lineWidth / 2, y: lineWidth / 2)
            bezierPath.apply(transform)
            path = bezierPath.cgPath
            break
        case .outgoing, .outgoingMask:
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: width - 22, y: height))
            bezierPath.addLine(to: CGPoint(x: 17, y: height))
            bezierPath.addCurve(to: CGPoint(x: 0, y: height - 17), controlPoint1: CGPoint(x: 7.61, y: height), controlPoint2: CGPoint(x: 0, y: height - 7.61))
            bezierPath.addLine(to: CGPoint(x: 0, y: 17))
            bezierPath.addCurve(to: CGPoint(x: 17, y: 0), controlPoint1: CGPoint(x: 0, y: 7.61), controlPoint2: CGPoint(x: 7.61, y: 0))
            bezierPath.addLine(to: CGPoint(x: width - 21, y: 0))
            bezierPath.addCurve(to: CGPoint(x: width - 4, y: 17), controlPoint1: CGPoint(x: width - 11.61, y: 0), controlPoint2: CGPoint(x: width - 4, y: 7.61))
            bezierPath.addLine(to: CGPoint(x: width - 4, y: height - 11))
            bezierPath.addCurve(to: CGPoint(x: width, y: height), controlPoint1: CGPoint(x: width - 4, y: height - 1), controlPoint2: CGPoint(x: width, y: height))
            bezierPath.addLine(to: CGPoint(x: width + 0.05, y: height - 0.01))
            bezierPath.addCurve(to: CGPoint(x: width - 11.04, y: height - 4.04), controlPoint1: CGPoint(x: width - 4.07, y: height + 0.43), controlPoint2: CGPoint(x: width - 8.16, y: height - 1.06))
            bezierPath.addCurve(to: CGPoint(x: width - 22, y: height), controlPoint1: CGPoint(x: width - 16, y: height), controlPoint2: CGPoint(x: width - 19, y: height))
            bezierPath.close()
            // Offset by half line width, so everything fits
            let transform = CGAffineTransform(translationX: lineWidth / 2, y: lineWidth / 2)
            bezierPath.apply(transform)
            path = bezierPath.cgPath
            break
        default:
            path = nil
            break
        }
    }
}
