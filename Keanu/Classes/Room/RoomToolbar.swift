//
//  RoomToolbar.swift
//  Keanu
//
//  Created by N-Pex on 02.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit

public protocol RoomToolbarDelegate: class {
    func didUpdateText(textView:UITextView)
}

open class RoomToolbar: UIView, UITextViewDelegate {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    open weak var delegate:RoomToolbarDelegate? = nil
    
    @IBOutlet open weak var attachmentButton:UIButton?
    @IBOutlet open weak var textInputView:UITextView!
    @IBOutlet open weak var sendButton:UIButton!
    @IBOutlet open weak var microphoneButton:UIButton!
    @IBOutlet open weak var placeholderLabel:UILabel!
    
    open var inputPlaceholder:String? {
        didSet {
            placeholderLabel?.text = inputPlaceholder
        }
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        textInputView.delegate = self
        placeholderLabel.text = inputPlaceholder
        placeholderLabel.isHidden = !textInputView.text.isEmpty
        sendButton.isHidden = textInputView.text.isEmpty
        microphoneButton.isHidden = !textInputView.text.isEmpty
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        placeholderLabel?.isHidden = !textInputView.text.isEmpty
        if let delegate = self.delegate {
            delegate.didUpdateText(textView: textView)
        }
    }
    
    public func clearInput() {
        sendButton.isHidden = true
        microphoneButton.isHidden = false
        textInputView.text = nil
        textInputView.undoManager?.removeAllActions()
        textViewDidChange(textInputView)
    }
    
    public func setInput(text:String) {
        textInputView.text = text
        textInputView.undoManager?.removeAllActions()
        textViewDidChange(textInputView)
    }
}

