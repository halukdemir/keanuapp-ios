//
//  StoryEditorViewController.swift
//  Keanu
//
//  Created by N-Pex on 06-09-19.
//

import MatrixSDK
import MatrixKit
import RichEditorView

public protocol StoryEditorViewControllerDelegate: class {
    func onSaveStory(fileName:String, content:String)
}

open class StoryEditorViewController: BaseViewController, StoryAddMediaViewControllerDelegate, RichEditorToolbarDelegate {
    
    /**
     Instantiate the `StoryEditorViewController`from the "StoryEditorViewController" storyboard.
     */
    class func instantiate(session: MXSession, delegate: StoryEditorViewControllerDelegate?) -> UINavigationController? {
        if let navVc = UIStoryboard.overriddenOrLocal(name: "StoryEditorViewController")
            .instantiateInitialVCWithDelegate() as? UINavigationController,
            let vc = navVc.viewControllers[0] as? StoryEditorViewController {
            vc.session = session
            vc.delegate = delegate
            return navVc
        }
        return nil
    }
    
    public weak var delegate: StoryEditorViewControllerDelegate?
    public weak var session: MXSession?
    
    @IBOutlet weak var titleView: UITextField!
    @IBOutlet weak var editor: RichEditorView!
    @IBOutlet weak var toolbarView: RichEditorToolbar!
    
    /**
     Safari can't play audio and video directly from uploaded url on the matrix server. We want the preview
     to work in the editor, so in the editor we use local cache paths. When saving the story HTML, however,
     we want to use the uploaded https url:s, so we apply a regex using the mapping below!
     */
    private var urlMap:[String:String] = [:]
    
    private var uploadOperation: MXMediaLoader?
    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        let image = UIImage(named: "ic_add", in: Bundle.mxk_bundle(for: StoryEditorViewController.self) , compatibleWith: .none)
        let addImageOption = RichEditorOptionItem(image: image, title: "+") { toolbar in
            if let picker = StoryAddMediaViewController.instantiate(delegate:self) {
                self.navigationController?.pushViewController(picker, animated: true)
            }
            return
        }
        var options:[RichEditorOption] = [
            RichEditorOptions.link,
            RichEditorOptions.bold,
            RichEditorOptions.italic,
            //RichEditorOptions.clear,
            RichEditorOptions.undo,
            //RichEditorOptions.redo,
            //RichEditorOptions.subscript,
            //RichEditorOptions.superscript,
            //RichEditorOptions.strike,
            //RichEditorOptions.underline,
            //RichEditorOptions.textColor,
            //RichEditorOptions.textBackgroundColor,
            RichEditorOptions.header(1),
            RichEditorOptions.header(2),
            //RichEditorOptions.header(3),
            //RichEditorOptions.header(4),
            //RichEditorOptions.header(5),
            //RichEditorOptions.header(6),
            //RichEditorOptions.indent,
            //RichEditorOptions.outdent,
            RichEditorOptions.orderedList,
            RichEditorOptions.unorderedList,
            RichEditorOptions.alignLeft,
            RichEditorOptions.alignCenter,
            RichEditorOptions.alignRight,
            //RichEditorOptions.Image,
        ]
        options.insert(addImageOption, at: 0)
        toolbarView.options = options
        toolbarView.editor = editor
        toolbarView.delegate = self
    }
    
    public func didSelectMedia(_ media: [MediaInfo]) {
        self.editor?.focus()
        for m in media {
            addMedia(media: m)
        }
    }
    
    private func addMedia(media: MediaInfo) {
        guard let session = session,
            let uploader = MXMediaManager.prepareUploader(
                withMatrixSession: session, initialRange: 0, andRange: 1) else { return }
        
        var data:Data? = nil
        if media.isImage {
            if let image = MXKTools.forceImageOrientationUp(media.image)?.reduce(to: CGSize(width: 480, height: 480)) {
                data = image.pngData()
            }
        } else if media.isAudio, let url = media.url {
            do {
                data = try Data(contentsOf: url)
            } catch {}
        } else if media.isVideo, let url = media.url {
            do {
                data = try Data(contentsOf: url)
            } catch {}
        }
        
        guard data != nil else { return }
       
        workingOverlay.message = "Uploading... tap to cancel".localize()
        workingOverlay.tapHandler = {
            self.uploadOperation?.cancel()
            self.uploadOperation = nil
        }
        workingOverlay.isHidden = false
        uploadOperation = uploader
        
        uploader.uploadData(
            data,
            filename: nil, mimeType: media.mime,
            success: { matrixUrl in
                self.workingOverlay.isHidden = true
                if let matrixUrl = matrixUrl,
                    let url = session.mediaManager.url(ofContent: matrixUrl) {
                    self.editor?.runJS("RE.prepareInsert();")
                    if media.isImage {
                        let alt = ""
                        let html = "<img src=\"\(url)?jpg\" alt=\"\(alt)\" style=\"max-width: 100%; width: auto; height:auto\"/>"
                        let jsInsert = "RE.insertHTML('\(html)');"
                        self.editor?.runJS(jsInsert)
                    } else if media.isAudio {
                        do {
                            var cacheUrl = url
                            if let cachePath = MXMediaManager.cachePath(forMatrixContentURI: matrixUrl, andType: "audio/x-m4a", inFolder: nil) {
                                let dest = URL(fileURLWithPath: cachePath)
                                try data?.write(to: dest)
                                cacheUrl = dest.absoluteString
                                self.urlMap[cacheUrl] = url
                            }
                            let html = "<audio controls=true src=\"\(cacheUrl)\"></audio>"
                            let jsInsert = "RE.insertHTML('\(html)');"
                            self.editor?.runJS(jsInsert)
                        } catch {
                            print(error)
                        }
                    } else if media.isVideo {
                        do {
                            var cacheUrl = url
                            if let cachePath = MXMediaManager.cachePath(forMatrixContentURI: matrixUrl, andType: "video/mpeg", inFolder: nil) {
                                let dest = URL(fileURLWithPath: cachePath)
                                try data?.write(to: dest)
                                cacheUrl = dest.absoluteString
                                self.urlMap[cacheUrl] = url
                            }
                            let html = "<video width=\"320\" height=\"240\" controls=true src=\"\(cacheUrl)\" /></video>"
                            let jsInsert = "RE.insertHTML('\(html)');"
                            self.editor?.runJS(jsInsert)
                        } catch {
                            print(error)
                        }
                    }
                }
        }, failure: { error in
            self.workingOverlay.isHidden = true
            print("Error " + (error?.localizedDescription ?? "unknown"))
        })
    }
    
    public func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
        // Open a dialog to insert link
        let alertController = UIAlertController(title: "https://...".localize(), message: "", preferredStyle: .alert)
        
        //the confirm action taking the inputs
        let confirmAction = UIAlertAction(title: "OK".localize(), style: .default) { (_) in
            if let link = alertController.textFields?[0].text {
                self.editor.insertLink(link, title: link)
            }
        }
        
        //the cancel action doing nothing
        let cancelAction = UIAlertAction(title: "Cancel".localize(), style: .cancel) { (_) in }
        
        //adding textfields to our dialog box
        alertController.addTextField { (textField) in
            //textField.placeholder = "Enter Name"
        }
        
        //adding the action to dialogbox
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        //finally presenting the dialog box
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction override public func dismiss() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction public func save() {
        guard editor.contentHTML.count > 0 else { return }
        
        var title:String? = nil
        if let editText = titleView.text, editText.count > 0 {
            title = editText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        }
        let titleString = title ?? "story\(Date().timeIntervalSince1970)"
        
        var content = editor.contentHTML
        
        // Make sure to use real url:s on server
        if let replacer = MappedSourceReplacer(urlMap: urlMap) {
            content = replacer.stringByReplacingMatches(in: content, options: [], range: NSMakeRange(0, content.count), withTemplate: "")
        }
        delegate?.onSaveStory(fileName: titleString.appending(".html"), content: content)
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    class MappedSourceReplacer: NSRegularExpression {
        private let urlMap:[String:String]
        
        init?(urlMap:[String:String]) {
            self.urlMap = urlMap
            do {
                try super.init(pattern: "src=\"([^\"]*)\"", options: [.caseInsensitive])
            } catch {
                return nil
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func replacementString(for result: NSTextCheckingResult, in string: String, offset: Int, template templ: String) -> String {
            
            if result.numberOfRanges == 2 {
                var range = result.range(at: 1)
                range.location += offset
                let s = (string as NSString).substring(with: range)
                if let url = urlMap[s] {
                    return "src=\"\(url)\""
                }
            }
            var range = result.range(at: 0)
            range.location += offset
            return (string as NSString).substring(with: range)
        }
    }
}

