//
//  RoomViewController.swift
//  Keanu
//
//  Created by N-Pex on 02.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixKit
import KeanuCore
import MaterialComponents.MaterialSnackbar
import QuickLook

open class RoomViewController: TableViewControllerWithToolbar, RoomToolbarDelegate, UINavigationControllerDelegate, RoomDataSourceDelegate {
    static let kMessageSentDateShow: UInt = 5 * 60 * 1000 // Milliseconds
    
    /**
    How much data that is paged into the tableView. It is given as a fraction of the tableView height, so a value of 0.3 means have 30% more data scrollable.
    */
    static let kOffscreenBuffer: CGFloat = 0.5
    
    /**
     Change these NIBs to use different toolbars!
     */
    public static var toolbarNibNormalMode: UINib = RoomToolbar.nib
    public static var toolbarNibStoryMode: UINib = StoryToolbar.nib
    
    open var room: MXRoom? {
        didSet {
            if isViewLoaded {
                didChangeRoom(from: oldValue, to: room)
            }
        }
    }
    
    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    open var roomListener: Any?
    open var observers: [NSObjectProtocol] = []
    open var dataSource: RoomDataSource?
    
    open var typingIndicator: TypingIndicator?
    open var attachmentPicker: AttachmentPicker?
    open var attachmentPickerCloseTapRecognizer: UITapGestureRecognizer?

    /**
     If we have not joined the room we are viewing, we show a "join room" view.
     */
    open var joinRoomView: JoinRoomView?

    /**
     Optional delegate for populating the attachment picker options
     */
    open var attachmentPickerDelegate: RoomViewControllerAttachmentPickerDelegate?
    
    open var isUpdating: Bool = false
    open var viewingLastMessage: Bool = true

    open var storyViewController: StoryViewController? = nil
    open var isStoryMode: Bool = false
    open var storyModeButton: UIButton?
    
    /**
     A bar button item that is shown as a warning when there are unknown devices present in an encrypted room.
     */
    open var unknownDevicesBarButtonItem: UIBarButtonItem?
    
    /**
     The current string to show for unknown devices in the room, containing things like number of unknown devices and the number of users that have unknown devices. This string is shown in an alert when the user taps the warning button in the nav bar.
     */
    open var unknownDevicesWarningString: String?

    /**
     The attachment item used for QuickLook.
    */
    private var attachmentItem: AttachmentItem?

    private lazy var storyModeStringAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: "MaterialIcons-Regular", size: 24)!,
            .foregroundColor: UIColor.systemGray]


    /**
     Instantiate the `RoomViewController` from the "Room" storyboard.

     This is a complete storyboard with `UINavigationController`.

     - parameter room: The room you want to open.
     - returns: The `UINavigationController` containing the `RoomViewController` as its top view controller.
     */
    class func instantiate(_ room: MXRoom? = nil) -> UINavigationController? {
        if  let navC = UIStoryboard.overriddenOrLocal(name: "Room")
            .instantiateInitialVCWithDelegate() as? UINavigationController {

            if let rVc = navC.topViewController as? RoomViewController {
                rVc.room = room
            }

            return navC
        }

        return nil
    }

    
    /**
     If we have an outstanding network operation that can be canceled, this property will hold on to that.
     */
    private var currentOp: MXHTTPOperation?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.keyboardDismissMode = .onDrag
        
        // Exchange "Edit" for a system info button style right bar button
        let infoButton = UIButton(type: .infoLight)
        infoButton.addTarget(self, action: #selector(didPressInfoButton), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: infoButton)

        // Create the story mode "play" button
        storyModeButton = UIButton(type: .system)
        if let storyModeButton = storyModeButton {
            storyModeButton.addTarget(self, action: #selector(didPressStoryModeButton), for: .touchUpInside)
            updateStoryModeButtonTitle()
            self.navigationItem.rightBarButtonItems?.append(UIBarButtonItem(customView: storyModeButton))
        }
        
        // Create the "unknown devices" warning button item
        let unknownDevicesButton = UIButton(type: .system)
        unknownDevicesButton.addTarget(self, action: #selector(didPressUnknownDevicesButton), for: .touchUpInside)
        unknownDevicesButton.setAttributedTitle(
            NSAttributedString(string: "", attributes: storyModeStringAttributes),
            for: .normal)
        unknownDevicesBarButtonItem = UIBarButtonItem(customView: unknownDevicesButton)
        
        tableView.scrollsToTop = false
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 2 // Arbitrary small value, needs to be smaller than the actual height of rows for correct layout when paging backwards.
        tableView.estimatedSectionHeaderHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        updateToolbar()
        
        // Load typing indicator
        guard let typingIndicator = TypingIndicator.nib.instantiate(withOwner: nil, options: nil)[0] as? TypingIndicator else {
            return
        }

        self.typingIndicator = typingIndicator

        // Load attachment picker
        guard let attachmentPicker = AttachmentPicker.nib.instantiate(withOwner: nil, options: nil)[0] as? AttachmentPicker else {
            return
        }

        self.attachmentPicker = attachmentPicker
        self.attachmentPickerCloseTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeAttachmentPicker))
        if let recognizer = self.attachmentPickerCloseTapRecognizer {
            recognizer.isEnabled = false
            self.view.addGestureRecognizer(recognizer)
        }
        
        // Register cell types
        self.tableView.register(IncomingMessageCell.nib, forCellReuseIdentifier: IncomingMessageCell.defaultReuseId)
        self.tableView.register(OutgoingMessageCell.nib, forCellReuseIdentifier: OutgoingMessageCell.defaultReuseId)
        self.tableView.register(PagingCell.nib, forCellReuseIdentifier: PagingCell.defaultReuseId)
        self.tableView.register(NewMembersCell.nib, forCellReuseIdentifier: NewMembersCell.defaultReuseId)
        self.tableView.register(StatusEventCell.nib, forCellReuseIdentifier: StatusEventCell.defaultReuseId)
        self.tableView.register(UnknownDeviceCell.nib, forCellReuseIdentifier: UnknownDeviceCell.defaultReuseId)

        didChangeRoom(from: nil, to: room)
        //let titleView = self.titleView()
        //self.refreshTitleView(titleView: titleView)
        //self.navigationItem.titleView = titleView
    }
    
    open func updateToolbar() {
        DispatchQueue.main.async {
            let nib = self.isStoryMode ? RoomViewController.toolbarNibStoryMode : RoomViewController.toolbarNibNormalMode
            guard let toolbar = nib.instantiate(withOwner: nil, options: nil)[0] as? RoomToolbar else {
                return
            }
            self.toolbarView = toolbar
            toolbar.delegate = self
            
            // If story mode toolbar, set delegate
            (toolbar as? StoryToolbar)?.storyDelegate = self.storyViewController
            
            // Call "didUpdateText" to initialize the controls to their proper values.
            self.didUpdateText(textView: toolbar.textInputView)
        }
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObservers()
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //tryToMarkAllMessagesAsRead()
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        saveCurrentMessageText()
        removeObservers()
    }
    
    override func dismiss() {
        super.dismiss()

        // We are being popped, reset
        self.room = nil
    }

    open override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag, completion: completion)

        // We are being popped, reset
        self.room = nil
    }
    
//    open override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.transform = CGAffineTransform(translationX: 130, y: 0)
//    }
    
//    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return self.tableView.frame.height
//    }
    
    func createEmptyRoom(session: MXSession) {
        // Just create a room with no current friends in it
        createRoom(withFriends: [], session: session)
    }
    
    func createRoom(withFriends friends: [Friend], session: MXSession, encrypted: Bool = true) {
        workingOverlay.message = "Creating room... tap to cancel".localize()
        workingOverlay.tapHandler = {
            if let currentOp = self.currentOp, !currentOp.isCancelled {
                currentOp.cancel()
                self.dismiss()
            }
        }
        workingOverlay.isHidden = false
        
        var params:[String:Any] = [:]
        params["visibility"] = "private"
        params["invite"] = friends.map { $0.matrixId }
        params["is_direct"] = NSNumber(value: (friends.count == 1))
        var initialState:[[String:Any]] = [
            ["type": kMXEventTypeStringRoomHistoryVisibility,
             "content": ["history_visibility": kMXRoomHistoryVisibilityJoined]
            ],
            ["type": kMXEventTypeStringRoomGuestAccess,
             "content": ["guest_access": kMXRoomGuestAccessForbidden]
            ],
            ["type": kMXEventTypeStringRoomJoinRules,
             "content": ["join_rule": kMXRoomJoinRuleInvite]
            ]
        ]
        if encrypted {
            initialState.append(["type": kMXEventTypeStringRoomEncryption,
                                 "content": ["algorithm": kMXCryptoMegolmAlgorithm]])
        }
        params["initial_state"] = initialState
        params["preset"] = "private_chat"
            
        currentOp = session.createRoom(parameters: params) { (createRoomResponse)  in
            self.currentOp = nil
            self.workingOverlay.isHidden = true
            if createRoomResponse.isSuccess, let response = createRoomResponse.value {
                if let room = session.room(withRoomId: response.roomId) {
                    self.room = room

                    // If we created a new 1:1 room with a user, it's now our friend
                    // and we need to keep the reference to the room, since that is
                    // expensive to evaluate and therefore won't be done automatically.
                    if friends.count == 1,
                        let friend = friends.first,
                        friend.privateRoom == nil {

                        friend.privateRoom = room
                        FriendsManager.shared.add(friend)
                    } else if friends.count == 0 {
                        // If we created an empty room, open settings
                        self.showSettings()
                    }
                }
            } else {
                // Failed
                let message = MDCSnackbarMessage()
                message.duration = 3
                message.text = "Failed to create room".localize()
                message.completionHandler = { userInitiated in
                    self.navigationController?.popViewController(animated: true)
                }
                MDCSnackbarManager.show(message)
            }
        }
    }
    
    open func updateDataSource() {
        if let room = self.room {
            if isStoryMode
            {
                self.dataSource = StoryDataSource(room: room)
                self.dataSource?.delegate = self

                // Story mode
                self.storyViewController = StoryViewController.instantiate(dataSource: self.dataSource)
                if let storyViewController = self.storyViewController {
                    storyViewController.renderDelegate = self
                    addChild(storyViewController)
                    self.view.addSubview(storyViewController.view)
                    storyViewController.didMove(toParent: self)
                    storyViewController.view.autoPinEdge(.top, to: .top, of: tableView, withOffset: 0)
                    storyViewController.view.autoPinEdge(.bottom, to: .bottom, of: tableView, withOffset: 0)
                    storyViewController.view.autoPinEdge(.leading, to: .leading, of: tableView, withOffset: 0)
                    storyViewController.view.autoPinEdge(.trailing, to: .trailing, of: tableView, withOffset: 0)
                    updateToolbar()
                }
            } else {
                self.dataSource = RoomDataSource(room: room)
                self.dataSource?.delegate = self
                self.tableView.dataSource = self.dataSource
                self.tableView.delegate = self.dataSource
                updateToolbar()
            }
        } else {
            self.tableView.dataSource = nil
            self.tableView.delegate = nil
            updateToolbar()
        }
    }
    
    open func reloadData() {
        if let storyViewController = storyViewController {
            storyViewController.reloadData()
        } else {
            self.tableView.reloadData()
        }
    }
    
    open func didChangeRoom(from: MXRoom?, to: MXRoom?, useStoryMode: Bool = false) {
        self.dataSource?.invalidate()
        self.dataSource = nil
        if let oldRoom = from, let roomListener = self.roomListener {
            oldRoom.removeListener(roomListener)
        }
        roomListener = nil
        self.title = nil
        unknownDevicesUpdate(unknownDevices: 0, unknownDeviceUsers: 0) // Reset

        // Remove old story mode view, if any
        if let storyViewController = self.storyViewController {
            storyViewController.view.removeFromSuperview()
            storyViewController.removeFromParent()
            storyViewController.didMove(toParent: nil)
            storyViewController.dataSource = nil
            self.storyViewController = nil
        }
        
        joinRoomView?.removeFromSuperview()
        navigationItem.rightBarButtonItem?.isEnabled = true
        isStoryMode = useStoryMode
        
        if let room = to {
            self.title = room.summary.friendlyDisplayName
            updateDataSource()

            // Add listener
            roomListener = room.listen(toEventsOfTypes: [kMXEventTypeStringRoomEncryption]) { (event, direction, state) in
                if let event = event, event.isState() {
                    self.didUpdateState()
                }
            }
            
            if room.summary.membership == .invite {
                navigationItem.rightBarButtonItem?.isEnabled = false
                showJoinRoomView()
            }
        } else {
            updateDataSource()
        }
        reloadData()
        didUpdateState()
        updateStoryModeButtonTitle()
        
        // Tell the app delegate which room is currently open. Need this to decide when to show in app notifications.
        RoomManager.shared.currentlyViewedRoomId = to?.roomId
    }

    open func addObservers() {
//        self.observers.append(
//            observe(\.tableView.bounds, options: [.new]) { (tableView, change) in
//                if let storyViewController = self.storyViewController {
//                    storyViewController.view.frame = self.tableView.frame
//                }
//        })
    }
    
    open func removeObservers() {
        for observer in self.observers {
            NotificationCenter.default.removeObserver(observer)
        }
        observers.removeAll()
    }
    
    var toolbar:RoomToolbar? {
        return self.toolbarView as? RoomToolbar
    }
    
    open override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !isUpdating, !isStoryMode {
            let scrolledToBottom = tableView.contentOffset.y >= (tableView.contentSize.height - tableView.frame.size.height)
            viewingLastMessage = scrolledToBottom
            DispatchQueue.main.async {
                self.checkIfPagingNeeded()
            }
        }
    }
    
    open func checkIfPagingNeeded() {
        // Need to page in more data?
        if let dataSource = dataSource, !dataSource.isPaging {
            if isStoryMode {
                if let storyViewController = self.storyViewController, storyViewController.needsForwardsPaging(),
                    dataSource.canPageForwards {
                    dataSource.pageForwards()
                }
                
                // Otherwise, for story mode, we only do that when you scroll
                return
            }
            let offsetY = self.tableView.contentOffset.y
            let contentHeight = self.tableView.contentSize.height
            if dataSource.canPageBackwards, (!tableView.viewportIsFilled || offsetY < (self.tableView.frame.height * RoomViewController.kOffscreenBuffer)) {
                dataSource.pageBackwards()
            } else if offsetY > (contentHeight - self.tableView.frame.height * (1 + RoomViewController.kOffscreenBuffer)), dataSource.canPageForwards {
                dataSource.pageForwards()
            }
        }
    }
    
    public func didUpdateText(textView: UITextView) {
        //Check if the text state changes from having some text to some or vice versa
        let hasText = textView.text.count > 0
        //if hasText != self.state.hasText  {
        //    self.state.hasText = hasText
        //    self.didUpdateState()
        //}
        
        //Everytime the textview has text and a notification comes through we are 'typing' otherwise     we are done typing
        if (hasText) {
            self.isTyping()
        } else {
            self.didFinishTyping()
        }
        if isStoryMode {
            if let room = room, let myUser = room.myUser, room.isAdmin(userId: myUser.userId) {
                self.toolbar?.sendButton.isHidden = !hasText
                self.toolbar?.microphoneButton.isHidden = hasText
            } else {
                // Not an admin, so we don't show the "add media" button. Instead, always show the send button, but only enable it if we have text.
                // Update - for now, disable sending if you are not admin!
                self.toolbar?.sendButton.isEnabled = false //hasText
                self.toolbar?.sendButton.isHidden = false
                self.toolbar?.microphoneButton.isHidden = true
            }
        } else {
            self.toolbar?.sendButton.isEnabled = hasText
            self.toolbar?.sendButton.isHidden = !hasText
            self.toolbar?.microphoneButton.isHidden = hasText
        }
        
        // Scroll to bottom when typing
        if hasText, !isStoryMode, let last = tableView.lastIndexPath {
            tableView.scrollToRow(at: last, at: .top, animated: true)
        }
    }

    @objc open func didPressInfoButton() {
        showSettings()
    }

    @objc open func didPressStoryModeButton() {
        didChangeRoom(from: self.room, to: self.room, useStoryMode: !isStoryMode)
    }
    
    open func showSettings() {
        performSegue(withIdentifier: "showRoomSettings", sender: self)
    }
    
    @objc open func didPressUnknownDevicesButton() {
        guard let alertMessage = unknownDevicesWarningString else { return }
        let alert = AlertHelper.build(message: alertMessage, title: "Unknown devices".localize(), style: .alert, actions: [AlertHelper.defaultAction()])
        present(alert, animated: true)
    }
    
    open func didUpdateState() {
        updateInputPlaceholder()
        //toolbar?.sendButton.isEnabled = self.state.hasText
    }
    
    open func updateStoryModeButtonTitle() {
        storyModeButton?.setAttributedTitle(
            NSAttributedString(string: isStoryMode ? "" : "", attributes: storyModeStringAttributes),
            for: .normal)
    }
    
    open func updateInputPlaceholder() {
        room?.state({ (state:MXRoomState?) in
            DispatchQueue.main.async {
                self.toolbar?.inputPlaceholder = "Send a message"
            }
        })
    }
    
    func isTyping() {
        room?.sendTypingNotification(typing: true, timeout: 10, completion: { (response) in
        })
    }
    
    func didFinishTyping() {
        room?.sendTypingNotification(typing: false, timeout: 0, completion: { (response) in
        })
    }
    
    open var showTypingIndicator: Bool = false {
        didSet {
            guard !isStoryMode else {return}
            let scrollToLast = viewingLastMessage
            if showTypingIndicator, let typingIndicator = self.typingIndicator {
                tableView.tableFooterView = typingIndicator
            } else {
                UIView.beginAnimations(nil, context: nil)
                tableView.tableFooterView = nil
                UIView.commitAnimations()
            }
            if scrollToLast, let last = tableView.lastIndexPath {
                tableView.scrollToRow(at: last, at: .top, animated: true)
            }
        }
    }
    
    func saveCurrentMessageText() {
    }
    
    func restoreCurrentMessageText() {
        
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        guard let text = toolbar?.textInputView.text else { return; }
        toolbar?.clearInput()
        var localEcho:MXEvent?
        if let room = room {
            room.sendTextMessage(text, localEcho: &localEcho, completion: { (response) in
                //TODO - handler error
            })
            self.dataSource?.insertLocalEcho(event: localEcho, roomState: room.dangerousSyncState)
        }
    }

    /**
     In a story mode, the "add media" button has been pressed. Open up the StoryAddMediaViewController.
     */
    @IBAction func addStoryMediaButtonPressed(_ sender: Any) {
        if isStoryMode {
            if let picker = StoryAddMediaViewController.instantiate(delegate:self) {
                self.navigationController?.pushViewController(picker, animated: true)
            }
        }
    }
        
    @IBAction func microphoneButtonPressed(_ sender: Any) {
        // Close keyboard
        self.view.endEditing(true)
        
        AudioRecorder.showIn(view: self.view, success: { (url:URL) in
            self.sendAudio(audioUrl: url)
        })
    }
    
    open override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        // Transitioning to room settings view controller. Set room and add ourselves as delegate.
        if let roomSettingsViewController = segue.destination as? RoomSettingsViewController {
            roomSettingsViewController.room = self.room
            roomSettingsViewController.delegate = self
        }
    }
    
    open func showJoinRoomView() {
        if joinRoomView == nil {
            joinRoomView = JoinRoomView.nib.instantiate(withOwner: nil, options: nil)[0] as? JoinRoomView
        }
        if let joinRoomView = joinRoomView {
            var roomName = ""
            if let room = self.room {
                roomName = room.summary.friendlyDisplayName
                joinRoomView.room = room
            }
            joinRoomView.titleLabel.text = String(format: NSLocalizedString("You have been invited to the room ´%@´.", comment: "Title for join room view shown in room view controller"), roomName)
            
            joinRoomView.acceptButtonCallback = {() -> Void in
                guard let room = self.room else { return }
                DispatchQueue.main.async {
                UIApplication.shared.joinRoom(room.roomId) { success, room in
                        if success {
                            UIView.animate(withDuration: 0.5, animations: {
                                joinRoomView.alpha = 0.0
                            }, completion: { (success) in
                                // Re-initialize the room
                                self.didChangeRoom(from: self.room, to: room)
                            })
                        }
                    }
                }
            }

            joinRoomView.declineButtonCallback = {() -> Void in
                if let room = self.room {
                    UIApplication.shared.declineRoomInvite(roomId: room.roomId) { (success) in
                        self.dismiss(animated: true)
                    }
                }
            }
            
            self.view.addSubview(joinRoomView)
            joinRoomView.autoPinEdgesToSuperviewEdges()
            joinRoomView.becomeFirstResponder()
        }
    }

    //MARK: RoomDataSourceDelegate
    private func updateTableView(deleted: [IndexPath]?, added: [IndexPath]?, changed: [IndexPath]?, live: Bool) {
        UITableView.setAnimationsEnabled(false)
        tableView.beginUpdates()
        if let deleted = deleted, deleted.count > 0 {
            self.tableView.deleteRows(at: deleted, with: .none)
        }
        if let changed = changed, changed.count > 0 {
            self.tableView.reloadRows(at: changed, with: .none)
        }
        if let added = added, added.count > 0 {
            self.tableView.insertRows(at: added, with: (live ? .bottom : .none))
        }
        tableView.endUpdates()
        UITableView.setAnimationsEnabled(true)
    }
    
    open func didChange(deleted: [IndexPath]?, added: [IndexPath]?, changed: [IndexPath]?, live: Bool, disableScroll: Bool) {
        if let storyViewController = storyViewController {
            //TODO
            storyViewController.didChange(deleted: deleted, added: added, changed: changed, live: live, disableScroll: disableScroll)
            return
        }
        
        let scrollToBottom = viewingLastMessage || !tableView.viewportIsFilled
        
        isUpdating = true
        tableView.showsVerticalScrollIndicator = false
        let oldScrollEnabled = tableView.isScrollEnabled
        tableView.isScrollEnabled = false
        
        let oldOffset = tableView.contentOffset
        
        var deletedCellsHeight: CGFloat = 0
        var addedCellsHeight: CGFloat = 0
        var changedCellsOldHeight: CGFloat = 0
        var changedCellsNewHeight: CGFloat = 0
        
        deleted?.forEach {
            deletedCellsHeight += tableView.rectForRow(at: $0).height
        }
        // Store the old heights for the cells
        changed?.forEach {
            changedCellsOldHeight += tableView.rectForRow(at: $0).height
        }
        
        if let firstDeleted = deleted?.first,
            let firstAdded = added?.first,
            firstDeleted == firstAdded,
            (added?.count ?? 0) == 1,
            (deleted?.count ?? 0) == 1,
            (changed?.count ?? 0) == 0 {
            // Actually a change, not a remove/add.
            updateTableView(deleted: nil, added: nil, changed: [firstAdded], live: live)
        } else {
            updateTableView(deleted: deleted, added: added, changed: changed, live: live)
        }
        
        tableView.setNeedsLayout()
        tableView.layoutIfNeeded()
        if !live {
            changed?.forEach {
                let offset = added?.count ?? 0
                let newIndexPath = IndexPath(row: $0.row + offset, section: $0.section)
                changedCellsNewHeight += tableView.rectForRow(at: newIndexPath).height
            }
            
            added?.forEach {
                addedCellsHeight += tableView.rectForRow(at: $0).height
            }
            
            var delta: CGFloat = 0
            delta = addedCellsHeight - deletedCellsHeight + changedCellsNewHeight - changedCellsOldHeight
            let newOffset = CGPoint(x: oldOffset.x, y: oldOffset.y + delta)
            if !disableScroll {
                tableView.setContentOffset(newOffset, animated: false)
            }
        }

        tableView.showsVerticalScrollIndicator = true
        tableView.isScrollEnabled = oldScrollEnabled
        isUpdating = false

        if scrollToBottom, !disableScroll {
            if !self.tableView.viewportIsFilled {
                self.tableView.setContentOffset(CGPoint.zero, animated: false)
            } else if let last = self.tableView.lastIndexPath {
                self.tableView.scrollToRow(at: last, at: .top, animated: live)
            }
        }
    }

    open func pagingStarted(direction: MXTimelineDirection) {
        let indexPath = IndexPath(row: 0, section: dataSource?.sectionHeader ?? 0)
        didChange(deleted: nil, added: [indexPath], changed: nil, live: false, disableScroll: false)
    }
    
    open func pagingDone(direction: MXTimelineDirection, newIndexPaths:[IndexPath], changedIndexPaths:[IndexPath], success: Bool, scrollToIndexPath: IndexPath? = nil) {
        let indexPath = IndexPath(row: 0, section: dataSource?.sectionHeader ?? 0)
        didChange(deleted: [indexPath], added: newIndexPaths, changed: changedIndexPaths, live: direction == .forwards, disableScroll: scrollToIndexPath != nil)
        
        // Scroll to a certain message (e.g. the last read one)?
        if let scrollToIndexPath = scrollToIndexPath {
            self.tableView.scrollToRow(at: scrollToIndexPath, at: .top, animated: false)
        }
        if success {
            DispatchQueue.main.async {
                // Check if we need more data to fill the screen/buffer
                self.checkIfPagingNeeded()
            }
        }
    }
    
    open func onTypingUsersChange(_ room: MXRoom) {
        DispatchQueue.main.async {
            if let typingIndicator = self.typingIndicator {
                typingIndicator.populate(room)
                self.showTypingIndicator = !typingIndicator.isHidden
            }
        }
    }
    
    open func createCell(at indexPath: IndexPath, for roomBubbleData: RoomBubbleData) -> UITableViewCell? {
        switch roomBubbleData.bubbleType {
        case .message, .messageEncrypted:
            let cell = tableView.dequeueReusableCell(withIdentifier: roomBubbleData.isIncoming ? IncomingMessageCell.defaultReuseId : OutgoingMessageCell.defaultReuseId, for: indexPath)
            if let renderCell = cell as? RoomBubbleDataRenderer {
                renderCell.render(roomBubbleData: roomBubbleData, delegate:self)
            }
            return cell
        case .roomNameChange, .roomTopicChange:
            let cell = tableView.dequeueReusableCell(withIdentifier: StatusEventCell.defaultReuseId, for: indexPath)
            if let renderCell = cell as? RoomBubbleDataRenderer {
                renderCell.render(roomBubbleData: roomBubbleData, delegate:self)
            }
            return cell
        case .newMember:
            let cell = tableView.dequeueReusableCell(withIdentifier: NewMembersCell.defaultReuseId, for: indexPath)
            if let renderCell = cell as? RoomBubbleDataRenderer {
                renderCell.render(roomBubbleData: roomBubbleData, delegate:self)
            }
            return cell
        case .devicesAdded:
            let cell = tableView.dequeueReusableCell(withIdentifier: UnknownDeviceCell.defaultReuseId, for: indexPath)
            if let renderCell = cell as? RoomBubbleDataRenderer {
                renderCell.render(roomBubbleData: roomBubbleData, delegate:self)
            }
            return cell
        case let .custom(ident, renderer):
            let cell = tableView.dequeueReusableCell(withIdentifier: ident, for: indexPath)
            if let renderer = renderer {
                // An explicit renderer was given when the bubble data was created. Use that!
                renderer(cell, roomBubbleData, self)
            } else if let renderCell = cell as? RoomBubbleDataRenderer {
                renderCell.render(roomBubbleData: roomBubbleData, delegate:self)
            }
            return cell
        default:
            return nil
        }
    }

    public func createCollectionViewCell(at indexPath: IndexPath, for roomBubbleData: RoomBubbleData) -> UICollectionViewCell? {
        if let storyViewController = self.storyViewController {
            return storyViewController.createCollectionViewCell(at: indexPath, for: roomBubbleData)
        }
        return nil
    }
    
    open func createCell(type: RoomBubbleDataType, at indexPath: IndexPath) -> UITableViewCell? {
        switch type {
        case .paging:
            let cell = tableView.dequeueReusableCell(withIdentifier: PagingCell.defaultReuseId, for: indexPath)
            return cell
        default:
            return nil
        }
    }
    
    public func unknownDevicesUpdate(unknownDevices: Int, unknownDeviceUsers: Int) {
        guard let unknownDevicesBarButtonItem = unknownDevicesBarButtonItem else { return }
        if unknownDevices == 0 {
            // Reset warning string and remove bar button item, if present
            unknownDevicesWarningString = nil
            self.navigationItem.rightBarButtonItems = self.navigationItem.rightBarButtonItems?.filter({ (item) -> Bool in
                return item != unknownDevicesBarButtonItem
            })
        } else {
            // Store the warning string, formatted
            if unknownDeviceUsers == 1 {
                if unknownDevices == 1 {
                    unknownDevicesWarningString = "1 user has an unknown device".localize()
                } else {
                    unknownDevicesWarningString = "1 user has % unknown devices".localize(value: "\(unknownDevices)")
                }
            } else {
                unknownDevicesWarningString = "% users have % unknown devices".localize(values: "\(unknownDeviceUsers)", "\(unknownDevices)")
            }
            
            // Make sure bar button item is added, if not already
            if !(self.navigationItem.rightBarButtonItems ?? []).contains(unknownDevicesBarButtonItem) {
                var newItems = (self.navigationItem.rightBarButtonItems ?? [])
                newItems.append(unknownDevicesBarButtonItem)
                self.navigationItem.rightBarButtonItems = newItems
            }
        }
    }
    
    func sendImage(image:UIImage?) {
        // Reduce size to 1080 x 1080, if bigger.
        if let room = room,
            let image = image?.reduce(),
            let data = image.jpegData() {
            
            var localEcho: MXEvent?
            
            // Send the image. TODO - add an interim step, where you can cancel sending.
            room.sendImage(data: data, size: image.size,
                           mimeType: UtiHelper.jpegMimeType, thumbnail: nil,
                           localEcho: &localEcho)
            { response in
                //TODO - handle response
            }
            
            dataSource?.insertLocalEcho(event: localEcho,
                                        roomState: room.dangerousSyncState)
        }
    }
    
    func sendAudio(audioUrl: URL?) {
        if let room = self.room,
            let audioUrl = audioUrl {
            var localEcho:MXEvent?
            room.sendAudioFile(localURL: audioUrl, mimeType: "audio/x-m4a", localEcho: &localEcho) { (response) in
                do {
                    // TODO - Only remove on success?
                    try FileManager.default.removeItem(at: audioUrl)
                } catch {
                    print(error)
                }
            }
            self.dataSource?.insertLocalEcho(event: localEcho, roomState: room.dangerousSyncState)
        }
    }
    
    func sendVideo(videoUrl: URL?) {
        if let room = room,
            let videoUrl = videoUrl {
            var localEcho: MXEvent?
            room.sendVideo(localURL: videoUrl,
                           thumbnail: UIImage.thumbnailForVideo(at: videoUrl),
                           localEcho: &localEcho)
            { response in
                //TODO - handle response
            }
            
            dataSource?.insertLocalEcho(event: localEcho,
                                        roomState: room.dangerousSyncState)
        }
    }
    
    func sendFile(url: URL?, mimeType:String) {
        guard let url = url else { return }
        let deleteTempFile = {
            do {
                try FileManager.default.removeItem(at: url)
            } catch {}
        }
        
        if let room = room {
            var localEcho: MXEvent?
            room.sendFile(localURL: url, mimeType: mimeType, localEcho: &localEcho) { response in
                // TODO - Only remove on success?
                deleteTempFile()
            }
            dataSource?.insertLocalEcho(event: localEcho,
                                        roomState: room.dangerousSyncState)
        } else {
            deleteTempFile()
        }
    }
}

extension RoomViewController : UIPopoverPresentationControllerDelegate {
    public func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
        popoverPresentationController.sourceView = toolbar?.attachmentButton
    }
}

// MARK: Attachment picking
extension RoomViewController: UIImagePickerControllerDelegate {
    
    @objc public func closeAttachmentPicker() {
        if let attachmentPicker = self.attachmentPicker, let toolbar = self.toolbar {
            UIView.animate(withDuration: 0.3, animations: {
                attachmentPicker.frame.origin.y = toolbar.frame.origin.y
                attachmentPicker.frame.size.height = 0
            }) { (success) in
                self.attachmentPickerCloseTapRecognizer?.isEnabled = false
                attachmentPicker.removeFromSuperview()
            }
        }
    }
    
    @IBAction open func attachButtonPressed(_ sender: Any) {
        if let attachmentPicker = self.attachmentPicker, let toolbar = self.toolbar {
            
            attachmentPicker.clearActions()
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let _ = attachmentPicker.addAction("") {
                    self.closeAttachmentPicker()
                    
                    // Show camera
                    let imagePicker = UIImagePickerController()
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .camera
                    imagePicker.mediaTypes = ["public.image", "public.movie"]
                    imagePicker.delegate = self
                    self.present(imagePicker, animated: true)
                }
            }
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let _ = attachmentPicker.addAction("") {
                    self.closeAttachmentPicker()
                    
                    // Show library
                    let imagePicker = UIImagePickerController()
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .photoLibrary
                    imagePicker.mediaTypes = ["public.image", "public.movie"]
                    imagePicker.delegate = self
                    self.present(imagePicker, animated: true)
                }
            }

            // Add story editor
            let _ = attachmentPicker.addAction("") {
                self.closeAttachmentPicker()
                if let session = self.room?.mxSession, let storyEditor = StoryEditorViewController.instantiate(session: session, delegate: self) {
                    //self.navigationController?.pushViewController(storyEditor, animated: true)
                    self.present(storyEditor, animated: true, completion: nil)
                }
            }

            // Add stuff from delegate
            attachmentPickerDelegate?.addActions(attachmentPicker: attachmentPicker, room: room)
            
            // Bail out if nothing to show
            guard attachmentPicker.actionCount() != 0 else { return }
            
            if attachmentPicker.superview == nil {
                self.view.addSubview(attachmentPicker)
            }
            attachmentPicker.frame.origin.y = toolbar.frame.origin.y
            attachmentPicker.frame.size.width = self.view.bounds.width
            attachmentPicker.frame.size.height = 0
            UIView.animate(withDuration: 0.3, animations: {
                attachmentPicker.frame.origin.y = toolbar.frame.origin.y - attachmentPicker.defaultHeight
                attachmentPicker.frame.size.height = attachmentPicker.defaultHeight
            })
            self.attachmentPickerCloseTapRecognizer?.isEnabled = true
        }
    }

    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo
        info: [UIImagePickerController.InfoKey : Any]) {

        picker.dismiss(animated: true)

        guard let _ = room,
            let mediaType = info[.mediaType] as? String
        else {
            return
        }
        
        if mediaType == UtiHelper.typeImage {
            sendImage(image: (info[.editedImage] as? UIImage ?? info[.originalImage] as? UIImage))
        } else if mediaType == UtiHelper.typeMovie {
            sendVideo(videoUrl: info[.mediaURL] as? URL)
        }
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}

extension RoomViewController: RoomBubbleDataRendererDelegate, QLPreviewControllerDataSource, QLPreviewControllerDelegate {

    func viewFriendProfile(friend: Friend) {
        let profileVc = ProfileViewController()
        profileVc.friend = friend
        self.navigationController?.pushViewController(profileVc, animated: true)
    }
    
    public func didTapIncomingAvatar(roomBubbleData: RoomBubbleData, view: UIView) {
        guard let firstEvent = roomBubbleData.events.first else {return}

        var actions:[UIAlertAction] = []
        
        let viewProfileAction:((UIAlertAction?) -> Void) = { [weak self] _ in
            if let self = self, let senderId = firstEvent.sender, let friend = FriendsManager.shared.get(senderId) {
                self.viewFriendProfile(friend: friend)
            }
        }
        
        // Action to view profile.
        actions.append(AlertHelper.defaultAction("View profile".localize(), handler: viewProfileAction))
        
        // Action to forward a message.
        actions.append(AlertHelper.defaultAction("Forward".localize())
        { [weak self] action in
            guard let self = self else { return }
            UIApplication.shared.forwardEvent(presentingViewController: self, event: firstEvent)
        })
        actions.append(AlertHelper.cancelAction())
        let alert = AlertHelper.build(message: nil, title: nil, style: .actionSheet, actions: actions)
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = view
            popoverController.sourceRect = view.bounds
            popoverController.permittedArrowDirections = []
        }
        present(alert, animated: true)
    }
    
    public func didTapOutgoingAvatar(roomBubbleData: RoomBubbleData, view: UIView) {
        guard let firstEvent = roomBubbleData.events.first else {return}
        
        var actions:[UIAlertAction] = []
        
        let viewProfileAction:((UIAlertAction?) -> Void) = { [weak self] _ in
            if let self = self, let myUser = self.room?.myUser {
                self.viewFriendProfile(friend: FriendsManager.shared.getOrCreate(myUser))
            }
        }
        
        // Action to view profile.
        actions.append(AlertHelper.defaultAction("View profile".localize(), handler: viewProfileAction))
        
        if firstEvent.sentState == MXEventSentStateFailed {
            // Action to delete a failed message.
            actions.append(AlertHelper.defaultAction("Delete message".localize())
            { [weak self] action in
                guard let self = self else { return }
                self.dataSource?.deleteOutgoingEvent(event: firstEvent)
            })
            // Action to retry sending a failed message.
            actions.append(AlertHelper.defaultAction("Retry".localize())
            { [weak self] action in
                guard let self = self else { return }
                self.dataSource?.resendMessage(event: firstEvent)
            })
        } else if firstEvent.sentState == MXEventSentStateSent {
            // Action to resend a message.
            actions.append(AlertHelper.defaultAction("Resend".localize())
            { [weak self] action in
                guard let self = self else { return }
                self.dataSource?.resendMessage(event: firstEvent)
            })
            
            // Action to forward a message.
            actions.append(AlertHelper.defaultAction("Forward".localize())
            { [weak self] action in
                guard let self = self else { return }
                UIApplication.shared.forwardEvent(presentingViewController: self, event: firstEvent)
            })
        } else {
            // Action to cancel a message.
            actions.append(AlertHelper.defaultAction("Cancel".localize())
            { [weak self] action in
                guard let self = self else { return }
                self.dataSource?.cancelMessage(event: firstEvent)
            })
        }
        
        if actions.count == 1 {
            viewProfileAction(nil)
        } else {
            actions.append(AlertHelper.cancelAction())
            let alert = AlertHelper.build(message: nil, title: nil, style: .actionSheet, actions: actions)
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = view
                popoverController.sourceRect = view.bounds
                popoverController.permittedArrowDirections = []
            }
            present(alert, animated: true)
        }
    }
    
    public func didTapMediaAttachmentItem(roomBubbleData: RoomBubbleData, view: UIView?, attachment: MXKAttachment?) {
        guard let attachment = attachment else {
            return
        }

        var ignore = false

        workingOverlay.message = "Loading attachment... tap to cancel".localize()
        workingOverlay.tapHandler = {
            self.workingOverlay.tapHandler = nil
            self.workingOverlay.isHidden = true
            ignore = true
        }

        workingOverlay.isHidden = false

        attachmentItem = AttachmentItem(attachment) { item in
            if ignore {
                return
            }

            self.workingOverlay.tapHandler = nil

            if let error = item.error {
                return AlertHelper.present(self, message: error.localizedDescription)
            }

            let showShare = {
                let vc = UIActivityViewController(activityItems: [item], applicationActivities: nil)

                if let view = view {
                    vc.popoverPresentationController?.sourceView = view
                    vc.popoverPresentationController?.sourceRect = view.bounds
                }

                self.present(vc, animated: true)
            }


            if attachment.type == MXKAttachmentTypeVideo {
                self.present(AttachmentViewController.instantiate(item), animated: true)
            }
            else if attachment.type == MXKAttachmentTypeFile {
                if #available(iOS 11.0, *), attachment.isHtml {
                    self.present(AttachmentViewController.instantiate(item), animated: true)
                }
                else if QLPreviewController.canPreview(item) {
                    let vc = QLPreviewController()
                    vc.dataSource = self
                    vc.delegate = self

                    self.present(vc, animated: true)
                }
                else {
                    showShare()
                }
            }
            else if (attachment.type == MXKAttachmentTypeAudio) {
                showShare()
            }
            else {
                // Show photo viewer
                guard let event = roomBubbleData.events.first, let room = self.room else {return}

                let photoStreamHandler = PhotoStreamHandler()
                photoStreamHandler.fetchImagesAsync(room: room, initialEvent: event, onStart: nil) { images, initialImage in
                    let browser = PhotosViewController(photoStreamHandler: photoStreamHandler, referenceView: view)
                    //browser.delegate = self
                    self.present(browser, animated: true)
                }
            }

            // Delay until after next scene is shown, to reduce flicker.
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.75, execute: {
                self.workingOverlay.isHidden = true
            })
        }
    }
    
    public func unknownDevicesVerify(roomBubbleData: RoomBubbleData, devices: [String:[String]]?, view: UIView?) {
        guard let devices = devices, devices.count > 0 else {return}
        if devices.count == 1 {
            guard let firstUserId = devices.keys.first, let friend = FriendsManager.shared.get(firstUserId) else {return}
            let devicesVC = DevicesViewController()
            devicesVC.friend = friend
            self.navigationController?.pushViewController(devicesVC, animated: true)
        } else {
            // More than 1 - Open room info in "show unknown devices" mode
            showSettings()
        }
    }
    
    public func requestRoomKeys(roomBubbleData: RoomBubbleData) {
        if let session = room?.mxSession,
            let event = roomBubbleData.events.first {
            session.crypto.reRequestRoomKey(for: event)
        }
    }


    // MARK: QLPreviewControllerDataSource

    public func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return attachmentItem == nil ? 0 : 1
    }

    public func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return attachmentItem!
    }


    // MARK: QLPreviewControllerDelegate

    public func previewControllerDidDismiss(_ controller: QLPreviewController) {
        // Trigger temp file destruction.
        // If user opens the same attachment again, there's going to be a race
        // condition otherwise and the QLPreviewController will show an empty file.
        attachmentItem = nil
    }
}

extension RoomViewController: RoomSettingsViewControllerDelegate {
    public func didArchiveRoom(_ roomSettingsViewController: RoomSettingsViewController) {
        if let room = roomSettingsViewController.room {
            room.summary.isArchived = true
        }
        let _ = UIApplication.shared.popToChatListViewController()
        dismiss(animated: true)
    }
    
    public func didLeaveRoom(_ roomSettingsViewController: RoomSettingsViewController) {
        self.room = nil
        let _ = UIApplication.shared.popToChatListViewController()
        dismiss(animated: true)
    }
}

extension RoomViewController: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
}

extension RoomViewController: StoryAddMediaViewControllerDelegate {
    public func didSelectMedia(_ media: [MediaInfo]) {
        for m in media {
            if m.isImage {
                sendImage(image: m.image)
            } else if m.isAudio {
                sendAudio(audioUrl: m.url)
            } else if m.isVideo {
                sendVideo(videoUrl: m.url)
            }
        }
    }
}

extension RoomViewController: StoryEditorViewControllerDelegate {
    public func onSaveStory(fileName: String, content: String) {
        guard let tempUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent(fileName) else { return }
        do {
            try content.write(to: tempUrl, atomically: false, encoding: .utf8)
            sendFile(url: tempUrl, mimeType: "text/html")
        }
        catch {}
    }
}
