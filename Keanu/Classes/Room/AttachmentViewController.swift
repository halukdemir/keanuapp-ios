//
//  AttachmentViewController.swift
//  Keanu
//
//  Created by N-Pex on 05.02.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import UIKit
import MatrixKit
import KeanuCore
import AVKit

enum WebErrors: Error {
    case RequestFailedError
}

// WKWebView steals focus, causing "back" button to not work... See https://stackoverflow.com/questions/56332558/uibutton-selector-not-working-after-button-tapped-within-wkwebview
class NonFirstRespondableWebView: WKWebView {
    override func becomeFirstResponder() -> Bool {
        return false
    }
}

class AttachmentViewController: UIViewController {
    
    public static func instantiate(_ item: AttachmentItem) -> UINavigationController {
        let vc = AttachmentViewController(item)
        
        let nc = UINavigationController(rootViewController: vc)
        nc.navigationBar.isTranslucent = true
        
        return nc
    }
    
    var item: AttachmentItem
    
    private lazy var workingOverlay: WorkingOverlay = {
        let overlay = WorkingOverlay()
        
        overlay.message = "Loading attachment... tap to cancel".localize()
        overlay.tapHandler = {
            self.done()
        }
        
        return overlay.addToSuperview(view)
    }()
    private var player: AVPlayer?    
    private var tempFile: URL?
    private var webView: WKWebView?
    private var webViewContentUrl: URL?
    private var pendingDownloads: [MXMediaLoader] = []
    init(_ item: AttachmentItem) {
        self.item = item
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, closure: { button in
            self.done()
        })
        
        navigationItem.title = item.previewItemTitle
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, closure: { button in
            let vc = UIActivityViewController(activityItems: [self.item],
                                              applicationActivities: nil)
            
            vc.popoverPresentationController?.barButtonItem = button
            
            self.present(vc, animated: true)
        })
        
        navigationController?.navigationBar.barStyle =
            item.attachment.type == MXKAttachmentTypeVideo ? .black : .default
        
        show()
    }
    
    func done() {
        pausePlayingMedia()
        
        // Abort pending downloads!
        for loader in pendingDownloads {
            loader.cancel()
        }
        pendingDownloads.removeAll()
        
        if let nav = self.navigationController {
            nav.dismiss(animated: true)
        } else {
            dismiss(animated: true)
        }
    }
    
    
    // MARK: Private Methods
    
    private func show() {
        guard let url = item.previewItemURL else {
            return setError(AttachmentError.fileNotFound)
        }
        
        if #available(iOS 11.0, *), item.attachment.isHtml {
            let preferences = WKPreferences()
            preferences.javaScriptEnabled = true
            
            // Create a configuration for the preferences
            let configuration = WKWebViewConfiguration()
            configuration.allowsAirPlayForMediaPlayback = true
            configuration.allowsInlineMediaPlayback = true
            configuration.mediaTypesRequiringUserActionForPlayback = []
            configuration.preferences = preferences
            
            // Apply default style sheet.
            // We do this by adding a user script to the page.
            let ssName = "StoryDefaultStyles"
            if let path = Bundle.main.path(forResource: ssName, ofType: "css") ??
                Bundle(for: AttachmentViewController.self).path(forResource: ssName, ofType: "css") {
                do {
                    let cssString = try String(contentsOfFile: path).components(separatedBy: .newlines).joined()
                    let source = """
                    var style = document.createElement('style');
                    style.innerHTML = '\(cssString)';
                    document.head.appendChild(style);
                    """
                    
                    let userScript = WKUserScript(source: source,
                                                  injectionTime: .atDocumentEnd,
                                                  forMainFrameOnly: true)
                    
                    let userContentController = WKUserContentController()
                    userContentController.addUserScript(userScript)
                    configuration.userContentController = userContentController
                } catch {}
            }
            
            
            let blockRules = """
[{
                    "trigger": {
                        "url-filter": ".*"
                    },
                    "action": {
                        "type": "block"
                    }
},
{
                    "trigger": {
                        "url-filter": ".*",
                        "resource-type": ["document","image","style-sheet","media"]
                    },
                    "action": {
                        "type": "ignore-previous-rules"
                    }
}]
"""
            // Instantiate the web view
            let webView = NonFirstRespondableWebView(frame: .zero, configuration: configuration)
            self.webView = webView
            
            let ruleListName = "KeanuStory-BlockExternalStoryResources"
            WKContentRuleListStore.default()?.removeContentRuleList(forIdentifier: ruleListName)
            { error in
                WKContentRuleListStore.default()?.compileContentRuleList(
                    forIdentifier: ruleListName, encodedContentRuleList: blockRules)
                { ruleList, error in
                    guard error == nil, let ruleList = ruleList else {
                        return self.setError(error)
                    }
                    
                    configuration.userContentController.add(ruleList)
                    
                    self.view.addSubview(webView)
                    webView.autoPinEdgesToSuperviewEdges()
                    do {
                        var content = try String(contentsOf: url)
                        if let storyCachePath = url.absoluteString.cachePathForStory() {
                            // Process the data, replacing media with cached versions (or adding to
                            // download queue as necessary).
                            var urlsNeedingDownload:[String] = []
                            if let homeserver = self.item.attachment.mediaManager.homeserverURL,
                                homeserver.starts(with: "https://"),
                                let urlReplacer = CashedMediaSourceReplacer(homeserver: homeserver) {
                                content = urlReplacer.stringByReplacingMatches(in: content, options: [], range: NSMakeRange(0, content.count), withTemplate: "")
                                urlsNeedingDownload = urlReplacer.urlsNeedingDownload
                            }
                            
                            // Save to cache path
                            let storyUrl = URL(fileURLWithPath: storyCachePath)
                            try content.write(to: storyUrl, atomically: true, encoding: .utf8)
                            self.loadWebViewContent(storyUrl: storyUrl, urlsNeedingDownload: urlsNeedingDownload)
                        }
                    } catch {
                        // TODO  - Handle error
                    }
                }
            }
        } else if item.attachment.type == MXKAttachmentTypeVideo {
            player = AVPlayer(url: url)
            
            let vc = AVPlayerViewController()
            vc.player = player
            
            addChild(vc)
            view.addSubview(vc.view)
            vc.view.frame = view.bounds
            vc.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            vc.didMove(toParent: self)
            
            player?.play()
        }
    }
    
    private func pausePlayingMedia() {
        player?.pause()
        
        // There is no obvious way to just "Stop all media playback", besides
        // iterating over all "audio" and "video" tags in JavaScript and calling "pause".
        // So we simply load a blank page.
        webView?.loadHTMLString("<html/>", baseURL: nil)
    }
    
    private func setError(_ error: Error?) {
        setError(error?.localizedDescription)
    }
    
    private func setError(_ text: String? = nil) {
        workingOverlay.isHidden = true
        
        let message = UILabel(frame: .zero)
        message.textColor = .red
        message.font = UIFont.boldSystemFont(ofSize: 19)
        message.numberOfLines = 0
        
        if let text = text {
            message.text = "Error: %".localize(value: text)
        }
        else {
            message.text = "An unknown error happened.".localize()
        }
        
        view.addSubview(message)
        message.autoCenterInSuperviewMargins()
    }
    
    private func loadWebViewContent(storyUrl:URL, urlsNeedingDownload:[String]) {
        webViewContentUrl = storyUrl
        guard let webView = webView else { return }
        if urlsNeedingDownload.count > 0 {
            workingOverlay.message = "Loading story... tap to cancel".localize()
            workingOverlay.tapHandler = { [weak self] in
                self?.done()
            }
            workingOverlay.isHidden = false
            
            for url in urlsNeedingDownload {
                guard let cachePath = url.cachePathForStoryMedia() else { continue }
                let loader = MXMediaLoader()
                loader.downloadMedia(fromURL: url, withIdentifier: url, andSaveAtFilePath: cachePath, success: { (path) in
                    self.onDownloadedOrFailed(loader: loader)
                }) { (error) in
                    self.onDownloadedOrFailed(loader: loader)
                }
            }
        } else {
            let dir = storyUrl.deletingLastPathComponent()
            webView.loadFileURL(storyUrl, allowingReadAccessTo: dir)
        }
    }
    
    
    /**
     When a download is done, check if ALL pending downloads are done. In that case load the story web page!
     */
    private func onDownloadedOrFailed(loader: MXMediaLoader?) {
        if let loader = loader {
            self.pendingDownloads.removeAll(where: { $0 == loader })
        }
        if pendingDownloads.count == 0,
            let url = webViewContentUrl,
            let webView = webView {
            let dir = url.deletingLastPathComponent()
            webView.loadFileURL(url, allowingReadAccessTo: dir)
            
            // Hide the progress overlay
            workingOverlay.isHidden = true
        }
    }
    
    class CashedMediaSourceReplacer: NSRegularExpression {
        
        public var urlsNeedingDownload:[String] = []
        
        init?(homeserver:String) {
            do {
                try super.init(pattern: "src=\"(\(homeserver)([^\"]*))\"", options: [.caseInsensitive])
            } catch {
                return nil
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        // Override just to reset the urlsNeedingDownload array...
        override func stringByReplacingMatches(in string: String, options: NSRegularExpression.MatchingOptions = [], range: NSRange, withTemplate templ: String) -> String {
            urlsNeedingDownload.removeAll()
            return super.stringByReplacingMatches(in: string, options: options, range: range, withTemplate: templ)
        }
        
        override func replacementString(for result: NSTextCheckingResult, in string: String, offset: Int, template templ: String) -> String {
            
            // Make sure to offset the range
            let hasSrcContentRange = result.numberOfRanges == 3
            var range = result.range(at: hasSrcContentRange ? 1 : 0)
            range.location += offset
            let s = (string as NSString).substring(with: range)
            
            // Already cached?
            if hasSrcContentRange,
                let storyMediaId = s.storyMediaId(),
                let cachePath = s.cachePathForStoryMedia() {
                if !FileManager.default.fileExists(atPath: cachePath) {
                    urlsNeedingDownload.append(s)
                }
                if s.hasSuffix("?jpg") {
                    return "src=\"media\(storyMediaId).jpg\""
                }
                return "src=\"media\(storyMediaId).m4a\""
            }
            return s
        }
    }
}

fileprivate extension String {
    func storyMediaId() -> String? {
        if let url = URL(string: self), url.deletingPathExtension().lastPathComponent.count == 24 {
            return url.deletingPathExtension().lastPathComponent
        }
        return nil
    }
    
    func cachePathForStory() -> String? {
        do {
            if let cachePath = MXMediaManager.getCachePath()
            {
                var cacheUrl = URL(fileURLWithPath: cachePath, isDirectory: true)
                cacheUrl.appendPathComponent("story", isDirectory: true)
                
                let fm = FileManager.default
                
                if !fm.fileExists(atPath: cacheUrl.path) {
                    try fm.createDirectory(at: cacheUrl, withIntermediateDirectories: true, attributes: nil)
                }
                return cacheUrl.appendingPathComponent("story.html", isDirectory: false).path
            }
        } catch {
            print("[\(String(describing: type(of: self)))] Error: \(error)")
        }
        
        return nil
    }
    
    func cachePathForStoryMedia() -> String? {
        do {
            if let cachePath = MXMediaManager.getCachePath(),
                let storyMediaId = self.storyMediaId()
            {
                var cacheUrl = URL(fileURLWithPath: cachePath, isDirectory: true)
                cacheUrl.appendPathComponent("story", isDirectory: true)
                
                let fm = FileManager.default
                
                if !fm.fileExists(atPath: cacheUrl.path) {
                    try fm.createDirectory(at: cacheUrl, withIntermediateDirectories: true, attributes: nil)
                }
                
                if self.hasSuffix("?jpg") {
                    return cacheUrl.appendingPathComponent("media\(storyMediaId).jpg", isDirectory: false).path
                }
                return cacheUrl.appendingPathComponent("media\(storyMediaId).m4a", isDirectory: false).path
            }
        } catch {
            print("[\(String(describing: type(of: self)))] Error: \(error)")
        }
        
        return nil
    }
}
