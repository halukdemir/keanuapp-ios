//
//  RoomViewControllerAttachmentPickerDelegate.swift
//  Keanu
//
//  Created by N-Pex on 30.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK

/**
 This protocol can be implemented to change/add attachment actions that are shown in a chat when the user taps the "+" button.
 */
public protocol RoomViewControllerAttachmentPickerDelegate {
    func addActions(attachmentPicker: AttachmentPicker, room: MXRoom?)
}
