//
//  UnknownDeviceCell.swift
//  Keanu
//
//  Created by N-Pex on 15.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK
import KeanuCore

/**
 A cell used to show that one or more members have unknown devices.
 */
open class UnknownDeviceCell: UITableViewCell, RoomBubbleDataRenderer {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    public class var defaultReuseId: String {
        return String(describing: self)
    }

    static let deviceMapUserDataKey = "devicesAdded"
    
    let maxNumberOfAvatarsShown = 5
    
    @objc @IBOutlet open weak var titleLabel:UILabel!
    @objc @IBOutlet open weak var avatarImageStackView:UIStackView!
    @IBOutlet var buttonContainer:UIStackView!
    @IBOutlet var verifyButton:UIButton!
    
    private weak var roomBubbleData:RoomBubbleData?
    private var delegate:RoomBubbleDataRendererDelegate?
    private var devicesAdded:[String:[String]]?
    
    public func render(roomBubbleData: RoomBubbleData, delegate:RoomBubbleDataRendererDelegate?) {
        self.roomBubbleData = roomBubbleData
        self.delegate = delegate
        
        selectionStyle = .none
        backgroundColor = UIColor(white: 0.95, alpha: 1)
        
        guard let devicesAdded = roomBubbleData.userData?[UnknownDeviceCell.deviceMapUserDataKey] as? [String:[String]] else {return}
        self.devicesAdded = devicesAdded
        
        var title = ""
        
        let count = devicesAdded.keys.count
        if count > 1 {
            // Label for send fail because of unknown devices when n > 1.
            title = "% people have unknown devices".localize(value: Formatters.format(int: count))
        } else {
            let userId = devicesAdded.keys.first ?? ""
            var memberName = userId
            if let friend = FriendsManager.shared.get(userId) {
                memberName = friend.name
            }
            // Label for send fail because of unknown devices when n = 1.
            title = "% has unknown devices".localize(value: memberName)
        }
        
        titleLabel.text = title
        
        for userId in devicesAdded.keys {
            if avatarImageStackView.arrangedSubviews.count <= maxNumberOfAvatarsShown {
                let avatarImageView = AvatarView()
                if let friend = FriendsManager.shared.get(userId) {
                    avatarImageView.load(friend: friend)
                } else if let session = roomBubbleData.dataSource.room.mxSession {
                    avatarImageView.load(session: session, id: userId, avatarUrl: nil, displayName: nil)
                }
                
                // Insert at 0 to get right Z-ordering
                avatarImageStackView.insertArrangedSubview(avatarImageView, at: 0)
                avatarImageView.autoSetDimensions(to: CGSize(width: 44, height: 44))
            }
        }
    }
    
    open override func prepareForReuse() {
        super.prepareForReuse()
        avatarImageStackView.subviews.filter{$0 is UIImageView}.forEach{
            avatarImageStackView.removeArrangedSubview($0)
            $0.removeFromSuperview()
        }
    }
    
    @IBAction func didTapVerify(_ sender: Any) {
        guard let roomBubbleData = self.roomBubbleData else {return}
        self.delegate?.unknownDevicesVerify(roomBubbleData: roomBubbleData, devices: self.devicesAdded, view: self.contentView)
    }
}


