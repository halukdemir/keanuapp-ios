//
//  MessageCell.swift
//  Keanu
//
//  Created by N-Pex on 03.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixKit
import KeanuCore

public class MessageCell: UITableViewCell {
    
    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    public class var defaultReuseId: String {
        return String(describing: self)
    }
    
    public static var iconSecure = ""
    public static var iconUnsecure = ""
    public static var iconSent = ""
    public static var iconSendFailed = ""
    public static var iconSendOther = ""
    public static var iconReceived = ""
    public static var linkDetector: NSDataDetector?
    public static func getLinkDetector() -> NSDataDetector? {
        if linkDetector == nil {
            do {
                linkDetector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            } catch {}
        }
        return linkDetector
    }
    
    @IBOutlet open weak var contentStack: UIStackView!
    @IBOutlet fileprivate var dateLabel: UILabel!
    @IBOutlet fileprivate var senderLabel: UILabel!
    @IBOutlet open weak var avatarImageView: AvatarView!
    @IBOutlet open var statusLabel: UILabel!
    
    // Content
    @IBOutlet open weak var messageContentContainerParent: UIView!
    @IBOutlet open weak var messageContentContainer: UIStackView!
    @IBOutlet open var messageTextView: UITextView!
    @IBOutlet open var messageMediaView: UIView!
    @IBOutlet open var messageButtonView: UIButton?
    fileprivate var supplementaryViews: [UIView]?
    
    /**
     Current data this cell is rendering.
     */
    var roomBubbleData: RoomBubbleData?
    var delegate: RoomBubbleDataRendererDelegate?
    var materialFont: UIFont?
    var dataUploader: MXMediaLoader?
    var dataUploaderProgressView: MXKPieChartView?
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        materialFont = UIFont(name: "MaterialIcons-Regular", size: statusLabel.font.pointSize)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapAvatarImage(_:)))
        tapGestureRecognizer.cancelsTouchesInView = true
        avatarImageView.isUserInteractionEnabled = true
        avatarImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.height / 2
        avatarImageView.layer.masksToBounds = true
    }
    
    open func setDate(date: NSAttributedString?) {
        dateLabel.attributedText = date
        if date == nil {
            dateLabel.isHidden = true
            contentStack.removeArrangedSubview(dateLabel)
        } else {
            dateLabel.isHidden = false
            contentStack.insertArrangedSubview(dateLabel, at: 0)
        }
    }
    
    open func setSender(sender: String?) {
        senderLabel.text = sender
        guard let sender = sender, sender.count > 0 else { return }
        senderLabel.isHidden = false
        contentStack.insertArrangedSubview(
            senderLabel, at: contentStack.arrangedSubviews.contains(dateLabel) ? 1 : 0)
    }
    
    /**
     Make a copy of the model text view, set the text and add to the content stack view.
     */
    open func addTextContent(text: NSAttributedString?) {
        guard let textView = messageTextView.copyView() as? UITextView else { return }
        textView.setBubbleView((roomBubbleData?.isIncoming ?? false) ? .incoming : .outgoing)
        textView.textContainerInset = messageTextView.textContainerInset
        
        var processedText = text
        
        // Make links clickable
        if let text = text, let linkDetector = MessageCell.getLinkDetector() {
            let matches = linkDetector.matches(in: text.string, options: [], range: NSRange(location: 0, length: text.length))
            if matches.count > 0 {
                let str = NSMutableAttributedString(attributedString: text)
                for match in matches {
                    guard let url = match.url else { continue }
                    str.addAttribute(.link, value: url, range: match.range)
                }
                processedText = str
            }
        }
        textView.attributedText = processedText
        messageContentContainer.addArrangedSubview(textView)
    }
    
    /**
     Make a copy of the model button view, set the text and add to the content stack view.
     */
    open func addButtonContent(text: String?, action: Selector) {
        guard let button = messageButtonView, let buttonView = button.copyView() as? UIButton else { return }
        buttonView.setTitle(text, for: .normal)
        buttonView.titleLabel?.font = button.titleLabel?.font
        buttonView.titleLabel?.textColor = button.titleLabel?.textColor
        buttonView.addTarget(self, action: action, for: .touchUpInside)
        messageContentContainer.addArrangedSubview(buttonView)
    }
    
    /**
     Make a copy of the model media container view, add the media view and add
     the whole thing to the content stack view.
     */
    open func addMediaContent(media: UIView?, aspect: CGFloat?, pinToBothSides: Bool) {
        let mediaView = messageMediaView.copyView()
        if let view = media {
            view.translatesAutoresizingMaskIntoConstraints = false
            mediaView.addSubview(view)
            view.autoPinEdgesToSuperviewEdges()
            if let imageView = view as? UIImageView {
                imageView.contentMode = .bottomRight
            }
        }
        let incoming = (roomBubbleData?.isIncoming ?? false)
        mediaView.setBubbleMask(incoming ? .incomingMask : .outgoingMask)
        if let aspect = aspect {
            mediaView.autoMatch(.height, to: .width, of: mediaView, withMultiplier: aspect)
        }
        messageContentContainer.addArrangedSubview(mediaView)
        if pinToBothSides {
            mediaView.autoPinEdge(toSuperviewEdge: .leading)
            mediaView.autoPinEdge(toSuperviewEdge: .trailing)
        } else if incoming {
            mediaView.autoPinEdge(toSuperviewEdge: .leading)
        } else {
            mediaView.autoPinEdge(toSuperviewEdge: .trailing)
        }
    }
    
    open func setSupplementaryViews(views: [UIView]?) {
        guard let views = views else { return }
        supplementaryViews = views
        for view in views {
            contentStack.addArrangedSubview(view)
            view.autoPinEdge(toSuperviewEdge: .leading)
            view.autoPinEdge(toSuperviewEdge: .trailing)
        }
    }
    
    // TODO: REMOVE! Please read doc of super method. This is a performance issue!
    // The `tableView(_:cellForRowAt:)` method should instead always set all fields
    // and not depend on stuff getting cleared here!
    public override func prepareForReuse() {
        super.prepareForReuse()
        for view in messageContentContainer?.arrangedSubviews ?? [] {
            view.subviews.forEach {
                if let audioPlayer = $0 as? AudioPlayer {
                    audioPlayer.reset()
                }
            }
            messageContentContainer?.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
        
        contentStack.removeArrangedSubview(senderLabel)
        setDate(date: nil)
        senderLabel.text = nil
        senderLabel.isHidden = true
        avatarImageView.image = nil
        statusLabel.text = nil
        for supplementaryView in supplementaryViews ?? [] {
            contentStack.removeArrangedSubview(supplementaryView)
            supplementaryView.removeFromSuperview()
        }
        supplementaryViews = nil
        
        stopListeners()
        
        self.roomBubbleData = nil
    }
    
    deinit {
        stopListeners()
    }
    
    /**
     Callback for taps. Overridden in incoming/outgoing subclasses.
     */
    @objc open func didTapAvatarImage(_ sender: UITapGestureRecognizer) {
    }
}

extension MessageCell: RoomBubbleDataRenderer {
    public func render(roomBubbleData: RoomBubbleData, delegate: RoomBubbleDataRendererDelegate?) {
        self.roomBubbleData = roomBubbleData
        self.delegate = delegate
        
        for view in messageContentContainer?.arrangedSubviews ?? [] {
            messageContentContainer?.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
        
        guard let event = roomBubbleData.events.first else {return}
        if let formatter = (roomBubbleData.isIncoming
            ? roomBubbleData.dataSource.incomingEventFormatter
            : roomBubbleData.dataSource.outgoingEventFormatter) {
            
            let senderName = roomBubbleData.getName(event.sender)
            
            let url = formatter.senderAvatarUrl(for: event, with: roomBubbleData.state)
            avatarImageView.load(session: roomBubbleData.dataSource.room.mxSession,
                                 id: event.sender, avatarUrl: url,
                                 displayName: senderName)
            
            setDate(date: NSAttributedString(string:
                formatter.dateString(from: event, withTime: true)))
            
            setSender(sender: senderName)
        }
        
        updateStatusLabel(statisticsDict: nil)
        
        for event in roomBubbleData.events {
            if !event.isMediaAttachment() {
                var err = MXKEventFormatterErrorNone
                if let formatter = (roomBubbleData.isIncoming
                    ? roomBubbleData.dataSource.incomingEventFormatter
                    : roomBubbleData.dataSource.outgoingEventFormatter) {
                    addTextContent(text:
                        formatter.attributedString(from: event,
                                                   with: roomBubbleData.state,
                                                   error: UnsafeMutablePointer(&err)))
                }
            } else {
                guard let attachment = MXKAttachment(
                    event: event, andMediaManager: roomBubbleData.dataSource.room.mxSession.mediaManager) else {
                        continue
                }
                guard let url = attachment.contentURL else {
                    continue
                }
                
                
                // Media attachment
                
                var isAudio = false
                if attachment.type == MXKAttachmentTypeAudio {
                    isAudio = true
                }
                else if attachment.type == MXKAttachmentTypeFile,
                    let mimeType = attachment.contentInfo?["mimetype"] as? String, mimeType.starts(with: "audio/") {
                    isAudio = true
                }
                
                if isAudio {
                    let audioPlayer = AudioPlayer(frame: .zero)
                    addMediaContent(media: audioPlayer, aspect: nil, pinToBothSides: true)
                    audioPlayer.setAttachment(attachment: attachment)
                    audioPlayer.onShareCallback = {
                        delegate?.didTapMediaAttachmentItem(roomBubbleData: roomBubbleData, view: audioPlayer.shareButton, attachment: attachment)
                    }
                } else if attachment.type == MXKAttachmentTypeSticker {
                    let v = MessageImageView(forAutoLayout: ())
                    v.autoSetDimension(.height, toSize: 150)
                    v.backgroundColor = .white
                    addMediaContent(media: v, aspect: 1, pinToBothSides: false)
                    v.setNeedsLayout()
                    v.layoutIfNeeded()
                    v.imageView.contentMode = .scaleAspectFit
                    v.setAttachment(attachment)
                } else if attachment.type == MXKAttachmentTypeVideo {
                    let v = MessageImageView(forAutoLayout: ())
                    addMediaContent(media: v, aspect: 0.75, pinToBothSides: true)
                    v.setNeedsLayout()
                    v.layoutIfNeeded()
                    v.imageView.contentMode = .scaleAspectFill
                    v.mediaFolder = attachment.eventRoomId
                    v.enableInMemoryCache = true
                    // Display video thumbnail, the video is played only when user selects this cell
                    v.setAttachmentThumb(attachment)
                    
                    let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapMediaView(recognizer:)))
                    tapRecognizer.numberOfTapsRequired = 1
                    v.addGestureRecognizer(tapRecognizer)
                } else if attachment.type == MXKAttachmentTypeFile,
                    let v = FileThumbnailView.nib.instantiate(withOwner: nil, options: nil)[0] as? FileThumbnailView
                {
                    v.filenameLabel.text = attachment.fileDisplayName
                    addMediaContent(media: v, aspect: 0.75, pinToBothSides: true)
                    v.setNeedsLayout()
                    v.layoutIfNeeded()
                    v.attachment = attachment
                    v.setBubbleView(roomBubbleData.isIncoming(event: event) ? .incoming : .outgoing)
                    
                    let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapMediaView(recognizer:)))
                    tapRecognizer.numberOfTapsRequired = 1
                    v.addGestureRecognizer(tapRecognizer)
                } else {
                    let v = MessageImageView(forAutoLayout: ())
                    addMediaContent(media: v, aspect: 0.75, pinToBothSides: true)
                    v.setNeedsLayout()
                    v.layoutIfNeeded()
                    v.imageView.contentMode = .scaleAspectFill
                    v.setAttachmentThumb(attachment)
                    
                    let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapMediaView(recognizer:)))
                    tapRecognizer.numberOfTapsRequired = 1
                    v.addGestureRecognizer(tapRecognizer)
                }
                
                // Do we need to add listeners?
                if !roomBubbleData.isIncoming(event: event) {
                    switch (event.sentState) {
                    case MXEventSentStatePreparing,
                         MXEventSentStateEncrypting,
                         MXEventSentStateUploading:
                        startUploadListener(url: url)
                        break
                    default: break
                    }
                }
            }
            
            //Handle decryption errors - i.e. show a "re-request keys" if user has other devices
            if event.decryptionError != nil {
                // Action to request keys from our other devices.
                addButtonContent(text: "Re-request encryption keys".localize(),
                                 action: #selector(didTapRequestKeys(sender:)))
            }
            
        }
        
        // Hide dates if too close together
        if let previousBubble = roomBubbleData.dataSource.roomBubbleData(before: roomBubbleData),
            let firstEvent = roomBubbleData.events.first,
            let firstEventPrevious = previousBubble.events.first {
            
            // BUGFIX: This can crash at runtime (at least) on iPhone 4s simulator with
            // "Thread 1: EXC_BAD_INSTRUCTION (code=EXC_I386_INVOP, subcode=0x0)"
            // or
            // "Fatal error: Not enough bits to represent the passed value"
            // if we don't convert NSUInteger to Int64, because the values
            // can get negative and too big for an Int on 32 Bit platforms...
            let timeDiff = Int64(firstEventPrevious.age) - Int64(firstEvent.age)
            
            if timeDiff < RoomViewController.kMessageSentDateShow {
                setDate(date: nil) // Set to nil, hiding it!
            }
        }
    }
    
    /**
     Update the status label. This has the form: "Encyption symbol"
     ["Send state symbol"] [progress %].
     
     The send state symbol is only shown for outgoing messages.
     The progress percentage is derived from the given statisticsDict, if available.
     */
    func updateStatusLabel(statisticsDict: NSDictionary?) {
        guard let roomBubbleData = roomBubbleData,
            let event = roomBubbleData.events.first,
            let font = materialFont else {
                return
        }
        
        var text = ""
        if event.isEncrypted {
            text = text + MessageCell.iconSecure
        } else {
            text = text + MessageCell.iconUnsecure
        }
        
        // If outgoing, show sending status
        if !roomBubbleData.isIncoming {
            switch event.sentState {
            case MXEventSentStateSent:
                if roomBubbleData.isReceived {
                    text = text + MessageCell.iconReceived
                } else {
                    text = text + MessageCell.iconSent
                }
                break
            case MXEventSentStateFailed:
                text = text + MessageCell.iconSendFailed
                break
            default:
                text = text + MessageCell.iconSendOther
                break
            }
        }
        
        let statusString = NSMutableAttributedString(string: text, attributes: [NSAttributedString.Key.font:font])
        
        if let statisticsDict = statisticsDict,
            let current = statisticsDict[kMXMediaLoaderCompletedBytesCountKey] as? Float,
            let total = statisticsDict[kMXMediaLoaderTotalBytesCountKey] as? Float {
            let percentage = Int((100.0 * current) / total)
            statusString.append(NSAttributedString(string: " \(percentage)%"))
        }
        
        statusLabel.attributedText = statusString
    }
    
    func stopListeners() {
        NotificationCenter.default.removeObserver(
            self, name: .mxMediaLoaderStateDidChange, object: self.dataUploader)
        self.dataUploader = nil
    }
    
    func startUploadListener(url: String?) {
        guard let url = url, let uploader = MXMediaManager.existingUploader(withId: url) else {
            stopListeners()
            return
        }
        if let existingUploader = self.dataUploader {
            if existingUploader != uploader {
                stopListeners()
            } else {
                return // Already listening
            }
        }
        self.dataUploader = uploader
        NotificationCenter.default.addObserver(
            self, selector: #selector(mediaLoaderStateChanged),
            name: .mxMediaLoaderStateDidChange, object: uploader)
    }
    
    /**
     Return MessageImageView that can be used to show upload progress (for video and image). Returns nil
     if no such view is found.
    */
    func getProgressImageView() -> MessageImageView? {
        for view in messageContentContainer.arrangedSubviews {
            if let messageImageView = view.subviews.first as? MessageImageView {
                return messageImageView
            }
        }
        return nil
    }
    
    @objc func mediaLoaderStateChanged(_ notification: Notification) {
        if let loader = notification.object as? MXMediaLoader, let dataUploader = self.dataUploader, loader == dataUploader {
            onLoaderUpdate(loader: loader)
        }
    }
    
    func updateProgressImageView(statisticsDict: NSDictionary?) {
        if let messageImageView = getProgressImageView() {
            if let statisticsDict = statisticsDict {
                if self.dataUploaderProgressView == nil {
                    let bounds = messageImageView.bounds
                    let piechartBounds = CGRect(origin: CGPoint(x: bounds.midX, y: bounds.midY), size: CGSize(width: 48, height: 48))
                    let piechart = MXKPieChartView(frame: piechartBounds)
                    piechart.progressColor = UIColor(red: 0, green: 0, blue: 1, alpha: 0.8)
                    piechart.unprogressColor = UIColor.clear
                    messageImageView.addSubview(piechart)
                    self.dataUploaderProgressView = piechart
                }
                if let progressView = self.dataUploaderProgressView,
                    let current = statisticsDict[kMXMediaLoaderCompletedBytesCountKey] as? Float,
                    let total = statisticsDict[kMXMediaLoaderTotalBytesCountKey] as? Float {
                    let progress = CGFloat(current / total)
                    progressView.progress = CGFloat(progress)
                }
            } else if let piechartView = self.dataUploaderProgressView {
                piechartView.removeFromSuperview()
                self.dataUploaderProgressView = nil
            }
        }
    }
    
    func onLoaderUpdate(loader: MXMediaLoader) {
        switch loader.state {
        case MXMediaLoaderStateUploadInProgress, MXMediaLoaderStateDownloadInProgress:
            updateStatusLabel(statisticsDict: loader.statisticsDict)
            updateProgressImageView(statisticsDict: loader.statisticsDict)
            break
        case MXMediaLoaderStateUploadFailed, MXMediaLoaderStateDownloadFailed:
            stopListeners()
            updateStatusLabel(statisticsDict: nil)
            updateProgressImageView(statisticsDict: nil)
            break
        case MXMediaLoaderStateUploadCompleted, MXMediaLoaderStateDownloadCompleted:
            stopListeners()
            updateStatusLabel(statisticsDict: nil)
            updateProgressImageView(statisticsDict: nil)
            break
        default:
            break
        }
    }
    
    @objc func didTapMediaView(recognizer: UIGestureRecognizer?) {
        guard let roomBubbleData = roomBubbleData else {return}
        if let view = recognizer?.view as? MessageImageView {
            delegate?.didTapMediaAttachmentItem(roomBubbleData: roomBubbleData, view: view, attachment: view.attachment)
        } else if let view = recognizer?.view as? FileThumbnailView {
            delegate?.didTapMediaAttachmentItem(roomBubbleData: roomBubbleData, view: view, attachment: view.attachment)
        }
    }
    
    @objc func didTapRequestKeys(sender: UIButton) {
        if let roomBubbleData = roomBubbleData {
            delegate?.requestRoomKeys(roomBubbleData: roomBubbleData)
        }
    }
}

extension UIView
{
    func copyView<T: UIView>() -> T {
        return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self)) as! T
    }
}
