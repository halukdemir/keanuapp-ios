//
//  AnimatedGIFImageView.swift
//  Keanu
//
//  Created by N-Pex on 17.01.19.
//  Copyright © 2019 GuardianProject. All rights reserved.
//

import MatrixKit
import FLAnimatedImage

/**
 An override of MXKImageView that can handle animated GIFs. This is done by reacting to when the (still) image is set. If the mime type of the attachment is then "image/gif" try to load it as an animated GIF.
 */
class MessageImageView: MXKImageView {
    
    var attachment: MXKAttachment?
    
    override func setAttachment(_ attachment: MXKAttachment!) {
        self.attachment = attachment
        super.setAttachment(attachment)
    }
    
    override func setAttachmentThumb(_ attachment: MXKAttachment!) {
        self.attachment = attachment
        super.setAttachmentThumb(attachment)
    }
    
    override var image: UIImage! {
        get {
            return super.image
        }
        set {
            super.image = newValue
            checkIfAnimatedGIF()
        }
    }
    
    private func checkIfAnimatedGIF() {
        guard let attachment = attachment,
            let mimeType = attachment.contentInfo?["mimetype"] as? String,
            mimeType == "image/gif" else { return }
        
        attachment.getData({ (data) in
            if let animatedImage = FLAnimatedImage(gifData: data) {
                let animatedImageView = FLAnimatedImageView(image: super.image)
                animatedImageView.animatedImage = animatedImage
                self.addSubview(animatedImageView)
                animatedImageView.autoPinEdgesToSuperviewEdges()
            }
        }) { (error) in
        }
    }
}
