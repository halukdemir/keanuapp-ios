//
//  StoryBaseCell.swift
//  Keanu
//
//  Created by N-Pex 28.05.19.
//

import UIKit
import MatrixSDK
import MatrixKit

/**
 Base class for story collection view cells.
 */
open class StoryBaseCell: UICollectionViewCell, RoomBubbleDataRenderer {
    /**
     Current data this cell is rendering.
     */
    var roomBubbleData: RoomBubbleData?
    var delegate: RoomBubbleDataRendererDelegate?
    var hasMediaFocus: Bool = false {
        didSet {
            didGetMediaFocus(hasMediaFocus)
        }
    }
    var temporaryFile: String?
    var loadedMediaUrl: URL?
    
    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    public class var defaultReuseId: String {
        return String(describing: self)
    }
    
    public func render(roomBubbleData: RoomBubbleData, delegate: RoomBubbleDataRendererDelegate?) {
        self.roomBubbleData = roomBubbleData
        self.delegate = delegate
    }
    
    /**
     Get an attachment out of the roomBubbleData, if any. It assumes that any attachment will be in the first event of the events contained in the bubble.
     */
    public func getAttachment() -> MXKAttachment? {
        guard
            let roomBubbleData = self.roomBubbleData,
            let event = roomBubbleData.events.first,
            event.isMediaAttachment(),
            event.decryptionError == nil,
            let attachment = MXKAttachment(event: event, andMediaManager: roomBubbleData.dataSource.room.mxSession.mediaManager),
            attachment.contentURL != nil else {
                return nil
        }
        return attachment
    }
    
    func loadAttachment() {
        guard let attachment = getAttachment() else { return }
        
        // Is the attachment loaded?
        if attachment.isEncrypted {
            attachment.decrypt(toTempFile: { (file) in
                guard let file = file else {return}
                self.temporaryFile = file
                self.loadedMediaUrl = URL(fileURLWithPath: file)
                self.downloadDidSucceed()
            }) { (error:Error?) in
                self.downloadDidFail(error: error)
            }
        } else {
            attachment.prepare({
                if let cacheFilePath = attachment.cacheFilePath, FileManager.default.fileExists(atPath: cacheFilePath) {
                    self.loadedMediaUrl = URL(fileURLWithPath: cacheFilePath)
                    self.downloadDidSucceed()
                }
            }) { (error) in
                self.downloadDidFail(error: error)
            }
        }
    }
    
    open func cleanup() {
        hasMediaFocus = false
        if let temporaryFile = temporaryFile {
            self.loadedMediaUrl = nil
            do {
                try FileManager.default.removeItem(at: URL(fileURLWithPath: temporaryFile))
            } catch {
                print(error)
            }
            self.temporaryFile = nil
        }
    }
    
    open func downloadDidSucceed() {
    }
    
    open func downloadDidFail(error:Error?) {
    }
    
    open func didGetMediaFocus(_ focus: Bool) {
        
    }
    
    @objc func didTapMediaView(recognizer: UIGestureRecognizer?) {
        guard let roomBubbleData = roomBubbleData else {return}
        delegate?.didTapMediaAttachmentItem(roomBubbleData: roomBubbleData, view: recognizer?.view, attachment: getAttachment())
    }
}
