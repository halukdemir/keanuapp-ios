//
//  AudioVisualizerView.swift
//  Keanu
//
//  Created by N-Pex on 04.06.19.
//

import UIKit
import AVKit

class AudioVisualizerView: UIView {
    @IBInspectable var barWidth: CGFloat = 5
    @IBInspectable var gapWidth: CGFloat = 1
    @IBInspectable var alphaPlayed: CGFloat = 1.0
    @IBInspectable var alphaUnplayed: CGFloat = 1.0
    
    var numBars: Int = 0
    var data:[Float] = []
    
    public func reset() {
        data.removeAll()
    }
    
    public func addSingleDataValue(_ value:Float) {
        data.append(value)
        if data.count > numBars {
            let toRemove = data.count - numBars
            data.removeSubrange(0...toRemove)
        }
        setNeedsDisplay()
    }
    
    public func loadAudioFile(url: URL) {
        reset()
        DispatchQueue.global(qos: .utility).async {
        let asset = AVURLAsset(url: url)
        do {
            let reader = try AVAssetReader(asset: asset)
            guard let track = asset.tracks.first else {return}
            
            let output = AVAssetReaderTrackOutput(track: track, outputSettings: [
                AVFormatIDKey: kAudioFormatLinearPCM,
                AVLinearPCMBitDepthKey: 16,
                AVLinearPCMIsBigEndianKey: false,
                AVLinearPCMIsFloatKey: false,
                AVLinearPCMIsNonInterleaved: false
                ])
            reader.add(output)

            var sampleRate:Float64 = 0
            var channels:Int = 0
            
            var values:[Int] = []
            
            for formatDesc in track.formatDescriptions {
                let desc = formatDesc as! CMAudioFormatDescription
                    let fmtDesc = CMAudioFormatDescriptionGetStreamBasicDescription(desc)
                    if let f = fmtDesc?.pointee {
                        sampleRate = f.mSampleRate
                        channels = Int(f.mChannelsPerFrame)
                    }
            }
            
            guard sampleRate > 0, channels > 0 else {return}
            var samplesPerSec = Int(sampleRate / 10)

            var temp:Int = 0
            var outputMax:Int = 0
            let sampleSizeInBytes = 2 * channels

            reader.startReading()
            while (reader.status == .reading) {
                let bufferRef = output.copyNextSampleBuffer()
                if let bufferRef = bufferRef, let blockBufferRef = CMSampleBufferGetDataBuffer(bufferRef) {
                    let length = CMBlockBufferGetDataLength(blockBufferRef)
                    guard let data = NSMutableData(length: length) else {return}
                    
                    var destUTF16 = Array<Int16>(repeating: 0, count: length / sampleSizeInBytes)
                    
                    CMBlockBufferCopyDataBytes(blockBufferRef, atOffset: 0, dataLength: length, destination: &destUTF16)
                    
                    //let shortBuffer = UnsafeMutableBufferPointer(start: UnsafeMutablePointer<UInt16>(data.mutableBytes), count: length)
                    
                    print("Read \(length) bytes into buffer")
                    
                    let samplesInBuffer = length / sampleSizeInBytes
                    var remainingSamples = samplesInBuffer
                    while remainingSamples > 0 {
                        if (temp + remainingSamples >= samplesPerSec) {
                            remainingSamples -= samplesPerSec - temp
                            
                            var ssum:Int = 0
                            
                            var index = (samplesInBuffer - remainingSamples - 1) * channels
                            for channel in 0..<channels {
                                let sample = abs(destUTF16[index])
                                index += 1
                                ssum += Int(sample)
                            }
                            ssum /= channels
                            outputMax = max(outputMax, ssum);
                            values.append(ssum);
                            temp = 0;
                        } else {
                            temp += remainingSamples
                            remainingSamples = 0
                        }
                    }
                }
            }
            if reader.status == .completed {
                // Normalize
                DispatchQueue.main.async {
                    for val in values {
                        self.data.append(Float(val) / Float(outputMax))
                    }
                    self.setNeedsDisplay()
                }
            } else {
                // Fail
                return
            }
        } catch {
        }
        }
    }

//
//    UInt32 bytesPerSample = 2 * channelCount;
//    SInt16 normalizeMax = 0;
//
//    NSMutableData * fullSongData = [[NSMutableData alloc] init];
//    [reader startReading];
//
//    UInt64 totalBytes = 0;
//    SInt64 totalLeft = 0;
//    SInt64 totalRight = 0;
//    NSInteger sampleTally = 0;
//
//    NSInteger samplesPerPixel = sampleRate / 50;
//
//    while (reader.status == AVAssetReaderStatusReading){
//
//    AVAssetReaderTrackOutput * trackOutput = (AVAssetReaderTrackOutput *)[reader.outputs objectAtIndex:0];
//    CMSampleBufferRef sampleBufferRef = [trackOutput copyNextSampleBuffer];
//
//    if (sampleBufferRef){
//    CMBlockBufferRef blockBufferRef = CMSampleBufferGetDataBuffer(sampleBufferRef);
//
//    size_t length = CMBlockBufferGetDataLength(blockBufferRef);
//    totalBytes += length;
//
//    NSAutoreleasePool *wader = [[NSAutoreleasePool alloc] init];
//
//    NSMutableData * data = [NSMutableData dataWithLength:length];
//    CMBlockBufferCopyDataBytes(blockBufferRef, 0, length, data.mutableBytes);
//
//    SInt16 * samples = (SInt16 *) data.mutableBytes;
//    int sampleCount = length / bytesPerSample;
//    for (int i = 0; i < sampleCount ; i ++) {
//
//    SInt16 left = *samples++;
//    totalLeft  += left;
//
//    SInt16 right;
//    if (channelCount==2) {
//    right = *samples++;
//    totalRight += right;
//    }
//
//    sampleTally++;
//
//    if (sampleTally > samplesPerPixel) {
//
//    left  = totalLeft / sampleTally;
//
//    SInt16 fix = abs(left);
//    if (fix > normalizeMax) {
//    normalizeMax = fix;
//    }
//
//    [fullSongData appendBytes:&left length:sizeof(left)];
//
//    if (channelCount==2) {
//    right = totalRight / sampleTally;
//
//    SInt16 fix = abs(right);
//    if (fix > normalizeMax) {
//    normalizeMax = fix;
//    }
//
//    [fullSongData appendBytes:&right length:sizeof(right)];
//    }
//
//    totalLeft   = 0;
//    totalRight  = 0;
//    sampleTally = 0;
//    }
//    }
//
//    [wader drain];
//
//    CMSampleBufferInvalidate(sampleBufferRef);
//    CFRelease(sampleBufferRef);
//    }
//    }
//
//    NSData * finalData = nil;
//
//    if (reader.status == AVAssetReaderStatusFailed || reader.status == AVAssetReaderStatusUnknown){
//    // Something went wrong. return nil
//
//    return nil;
//    }
//
//    if (reader.status == AVAssetReaderStatusCompleted){
//
//    NSLog(@"rendering output graphics using normalizeMax %d",normalizeMax);
//
//    UIImage *test = [self audioImageGraph:(SInt16 *)
//    fullSongData.bytes
//    normalizeMax:normalizeMax
//    sampleCount:fullSongData.length / 4
//    channelCount:2
//    imageHeight:100];
//
//    finalData = imageToData(test);
//    }
//
//    [fullSongData release];
//    [reader release];
//
//    return finalData;
//    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if (frame.width > 0) {
            numBars = Int(frame.width / (barWidth + gapWidth));
            print("Num bars set to \(numBars)")
        }
    }
    
    override func draw(_ rect: CGRect) {
    }
    
    fileprivate class VUViewLayer: CALayer {
        @NSManaged var animationFraction: Float
        
        override class func needsDisplay(forKey key: String) -> Bool {
            if key == #keyPath(animationFraction) {
                return true
            }
            return super.needsDisplay(forKey: key)
        }
        
        func drawBars(in ctx: CGContext, withAlpha alpha: CGFloat) {
            guard let view = self.delegate as? AudioVisualizerView else {return}
            guard view.numBars > 0 else {return}
            
            ctx.setFillColor(gray: 1.0, alpha: alpha)
            
            let numBars:Float = Float(view.numBars)
            let barWidth:Float = Float(view.barWidth)
            let gapWidth:Float = Float(view.gapWidth)
            
            var fIndex:Float = 0
            let fDelta:Float = Float.maximum(1.0, Float(view.data.count) / numBars)
            for i in 0..<view.numBars {
                // Calculate average
                var n:Float = 0
                var sum:Float = 0
                
                for j in Int(fIndex)..<Int(fIndex + fDelta) {
                    if j < view.data.count {
                        sum += view.data[j]
                        n += 1
                    }
                }
                if n > 0 {
                    sum = sum / n
                }
                fIndex += fDelta;
                
                let x = Float(i) * (barWidth + gapWidth) + barWidth / 2
                let halfHeight = Float(view.frame.height / 2)
                let yStart = halfHeight - sum * halfHeight
                let yEnd = halfHeight + sum * halfHeight
                
                let rect = CGRect(x: CGFloat(x - barWidth / 2), y: CGFloat(yStart), width: CGFloat(barWidth), height: CGFloat(yEnd - yStart))
                ctx.fill(rect)
            }
            let rect = CGRect(x: CGFloat(0), y: CGFloat(view.frame.height / 2) - CGFloat(barWidth / 2), width: CGFloat(bounds.width), height: CGFloat(barWidth))
            ctx.fill(rect)
        }
        
        override func draw(in ctx: CGContext) {
            guard let view = self.delegate as? AudioVisualizerView else {return}
            
            // Calc width
            let numBars:Float = Float(view.numBars)
            let barWidth:Float = Float(view.barWidth)
            let gapWidth:Float = Float(view.gapWidth)
            
            let n = min(numBars, Float(view.data.count))
            let w = n * barWidth + max(0, n - 1) * gapWidth;
            let x = 0.75 * w;
            
            UIGraphicsPushContext(ctx)
            ctx.saveGState()
            ctx.resetClip()
            ctx.clear(self.bounds)

            ctx.clip(to: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(x), height: self.bounds.height))
            drawBars(in: ctx, withAlpha: view.alphaPlayed)
            ctx.resetClip()
            ctx.clip(to: CGRect(x: CGFloat(x), y: CGFloat(0), width: self.bounds.width - CGFloat(x), height: self.bounds.height))
            drawBars(in: ctx, withAlpha: view.alphaUnplayed)
            
            ctx.restoreGState()
            UIGraphicsPopContext()
        }
    }
    
    override class var layerClass: AnyClass {
        get {
            return VUViewLayer.self
        }
    }
}

