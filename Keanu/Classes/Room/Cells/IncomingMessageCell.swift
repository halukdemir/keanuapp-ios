//
//  IncomingMessageCell.swift
//  Keanu
//
//  Created by N-Pex on 03.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit

public class IncomingMessageCell: MessageCell {
    
    public override func addTextContent(text: NSAttributedString?) {
        messageTextView.textContainerInset = UIEdgeInsets(top: 8, left: 12, bottom: 8, right: 8)
        super.addTextContent(text: text)
    }
    
    public override func didTapAvatarImage(_ sender: UITapGestureRecognizer) {
        super.didTapAvatarImage(sender)
        if let delegate = self.delegate, let roomBubbleData = self.roomBubbleData {
            delegate.didTapIncomingAvatar(roomBubbleData: roomBubbleData, view: self.avatarImageView)
        }
    }
}
