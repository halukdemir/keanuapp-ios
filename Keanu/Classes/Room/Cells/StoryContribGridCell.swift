//
//  StoryContribGridCell.swift
//  Keanu
//
//  Created by N-Pex 03.06.19.
//

import UIKit
import MatrixSDK
import MatrixKit

open class StoryContribGridCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
}
