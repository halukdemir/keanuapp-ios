//
//  FileThumbnailView.swift
//  Keanu
//
//  Created by N-Pex on 05.02.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import UIKit
import MatrixKit

open class FileThumbnailView: UIView {
    
    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    @IBOutlet weak var filenameLabel:UILabel!
    public var attachment:MXKAttachment?
}
