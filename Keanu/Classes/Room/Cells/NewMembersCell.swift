//
//  UsersJoinedCell.swift
//  Keanu
//
//  Created by N-Pex on 15.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK
import KeanuCore

/**
 A cell used as supplementary view to add group chat members you are not yet friends with.
 */
open class NewMembersCell: UITableViewCell, RoomBubbleDataRenderer {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    public class var defaultReuseId: String {
        return String(describing: self)
    }

    let maxNumberOfAvatarsShown = 5
    
    @objc @IBOutlet open weak var titleLabel:UILabel!
    @objc @IBOutlet open weak var avatarImageStackView:UIStackView!
    
    public func render(roomBubbleData: RoomBubbleData, delegate:RoomBubbleDataRendererDelegate?) {
        var membershipMap:[String:String] = [:]
        
        for event in roomBubbleData.events {
            if let sender = (event.stateKey ?? event.sender), let membership = event.content["membership"] as? String {
                membershipMap[sender] = membership
            }
        }

        var joined:[String] = []
        var left:[String] = []
        var invited:[String] = []

        for memberId in membershipMap.keys {
            if let membership = membershipMap[memberId] {
                switch membership {
                case kMXMembershipStringJoin:
                    joined.append(memberId)
                    break
                case kMXMembershipStringLeave, kMXMembershipStringBan:
                    left.append(memberId)
                    break
                case kMXMembershipStringInvite:
                    invited.append(memberId)
                    break
                default:
                    break
                }
            }
        }
        
        var title = ""

        if joined.count > 1 {
            // Label for usersJoinedCell when n > 1 users joining.
            title = "% people joined the room".localize(value: Formatters.format(int: joined.count))
        }
        else if joined.count == 1, let joined = joined.first {
            // Label for usersJoinedCell when n = 1 user joining.
            title = "% joined the room".localize(value: roomBubbleData.getName(joined))
        }

        if left.count > 0 {
            if title.count > 0 {
                title.append(", ".localize()) // Separator for usersJoinedCell.
            }
            if left.count > 1 {
                // Label for usersJoinedCell when n > 1 users leaving.
                title.append("% people left the room".localize(value: Formatters.format(int: left.count)))
            }
            else if let left = left.first {
                // Label for usersJoinedCell when n = 1 user leaving.
                title.append("% left the room".localize(value: roomBubbleData.getName(left)))
            }
        }

        if invited.count > 0 {
            if title.count > 0 {
                title.append(", ".localize()) // Separator for usersJoinedCell.
            }
            if invited.count > 1 {
                // Label for usersJoinedCell when n > 1 users invited.
                title.append("% people were invited".localize(value: Formatters.format(int: invited.count)))
            }
            else if let invited = invited.first {
                // Label for usersJoinedCell when n = 1 user invited.
                title.append("% was invited".localize(value: roomBubbleData.getName(invited)))
            }
        }

        titleLabel.text = title
        
        for member in membershipMap.keys {
            if avatarImageStackView.arrangedSubviews.count <= maxNumberOfAvatarsShown {
                let avatarImageView = AvatarView()

                if let member = roomBubbleData.state.members.member(withUserId: member) {
                    avatarImageView.load(friend: FriendsManager.shared.getOrCreate(
                            member, roomBubbleData.dataSource.room.mxSession))
                }
                
                // Insert at 0 to get right Z-ordering
                avatarImageStackView.insertArrangedSubview(avatarImageView, at: 0)
                avatarImageView.autoSetDimensions(to: CGSize(width: 44, height: 44))
            }
        }
    }
    

    // TODO: REMOVE! Please read doc of super method. This is a performance issue!
    // The `tableView(_:cellForRowAt:)` method should instead always set all fields
    // and not depend on stuff getting cleared here!
    open override func prepareForReuse() {
        super.prepareForReuse()
        
        for view in avatarImageStackView.subviews {
            if view is UIImageView {
                avatarImageStackView.removeArrangedSubview(view)
                view.removeFromSuperview()
            }
        }
    }
}


