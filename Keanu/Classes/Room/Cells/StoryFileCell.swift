//
//  StoryFileCell.swift
//  Keanu
//
//  Created by N-Pex 05.06.28.
//

import UIKit
import MatrixSDK
import MatrixKit
import AVKit

open class StoryFileCell: StoryBaseCell {
    @IBOutlet weak var filenameLabel:UILabel!
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        
        // Install tap recognizer on the image view
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapMediaView(recognizer:)))
        tapRecognizer.numberOfTapsRequired = 1
        self.addGestureRecognizer(tapRecognizer)
    }

    public override func render(roomBubbleData: RoomBubbleData, delegate: RoomBubbleDataRendererDelegate?) {
        super.render(roomBubbleData: roomBubbleData, delegate: delegate)
        
        guard let event = roomBubbleData.events.first, event.isMediaAttachment() else {return}
        guard event.decryptionError == nil else {return}
        guard let attachment = MXKAttachment(event: event, andMediaManager: roomBubbleData.dataSource.room.mxSession.mediaManager), attachment.contentURL != nil else {
            return
        }
        filenameLabel.text = attachment.fileDisplayName
    }
}

