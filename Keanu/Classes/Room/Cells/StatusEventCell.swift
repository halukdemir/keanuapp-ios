//
//  StatusEventCell.swift
//  Keanu
//
//  Created by N-Pex on 26.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK

/**
 A cell used to show status events, like room name and topic updates.
 */
open class StatusEventCell: UITableViewCell, RoomBubbleDataRenderer {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    public class var defaultReuseId: String {
        return String(describing: self)
    }

    @objc @IBOutlet open weak var titleLabel:UILabel!
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        for view in subviews {
            if let label = view as? UILabel {
                if label.numberOfLines == 0 {
                    label.preferredMaxLayoutWidth = label.bounds.width
                }
            }
        }
    }
    
    public func render(roomBubbleData: RoomBubbleData, delegate:RoomBubbleDataRendererDelegate?) {
        if let event = roomBubbleData.events.first {

            switch roomBubbleData.bubbleType {
            case .roomNameChange:
                titleLabel.text = "% changed the room name to \"%\"".localize(values:
                    roomBubbleData.getName(event.sender),
                    roomBubbleData.state.name ?? "")
                break
            case .roomTopicChange:
                titleLabel.text = "% changed the room topic to \"%\"".localize(values:
                    roomBubbleData.getName(event.sender),
                    roomBubbleData.state.topic ?? "")
                break
            default: break
            }
        }
    }
}


