//
//  StoryVideoCell.swift
//  Keanu
//
//  Created by N-Pex 28.05.19.
//

import UIKit
import MatrixSDK
import MatrixKit
import AVKit

open class StoryVideoCell: StoryBaseCell {
    var player: AVPlayer?
    var playerController: AVPlayerViewController?
    
    public override func render(roomBubbleData: RoomBubbleData, delegate: RoomBubbleDataRendererDelegate?) {
        super.render(roomBubbleData: roomBubbleData, delegate: delegate)
    }

    open override func downloadDidSucceed() {
        super.downloadDidSucceed()
        print("Success downloading!")
        updatePlayer(hasMediaFocus)
    }

    open override func didGetMediaFocus(_ focus: Bool) {
        super.didGetMediaFocus(focus)
        print("Got focus \(focus)")
        if focus, loadedMediaUrl == nil {
            print("Loading...")
            loadAttachment()
        } else {
            print("Call update")
            updatePlayer(focus)
        }
    }
    
    open override func prepareForReuse() {
        print("Prepare for reuse")
        super.prepareForReuse()
        cleanup()
    }
    
    open override func cleanup() {
        print("Cleanup")
        super.cleanup()
        if let _ = self.player {
            print("Reelase player")
            self.player = nil
        }
    }
    
    func updatePlayer(_ install: Bool) {
        guard let playerController = self.playerController else {return}
        if !install {
            print("updatePlayer - remove")
            if playerController.player == self.player {
                print("UpdatePlayer - clean old views")
                self.contentView.subviews.forEach({$0.removeFromSuperview()})
                playerController.player = nil
            }
        } else {
            print("updatePlayer - add")
            if player == nil, let url = loadedMediaUrl {
                print("updatePlayer - create player")
                player = AVPlayer(url: url)
            }
            if let player = self.player, playerController.player != player {
                print("updatePlayer - install views")
                self.contentView.addSubview(playerController.view)
                playerController.view.autoPinEdgesToSuperviewEdges()
                playerController.player = player
                playerController.view.alpha = 0
                UIView.animate(withDuration: 0.3) {
                    playerController.view.alpha = 1
               }
                player.play()
            } else {
                print("updatePlayer - already installed")
            }
        }
    }
}

