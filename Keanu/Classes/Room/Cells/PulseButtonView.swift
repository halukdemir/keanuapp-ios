//
//  PulseButtonView.swift
//  Keanu
//
//  Created by N-Pex on 04.06.19.
//

import UIKit

open class PulseButtonView: UIView, CAAnimationDelegate {
    @IBInspectable public var viewColor:UIColor = UIColor.white
    @IBInspectable public var minSize:Float = 0

    open override func layoutSubviews() {
        super.layoutSubviews()
        
        // Remove all sublayers
        //
        if let sublayers = self.layer.sublayers {
            for layer in sublayers.reversed() {
                layer.removeAllAnimations()
                layer.removeFromSuperlayer()
            }
        }
        
        //addCircle()
        addCircle(startIn: 0)
        addCircle(startIn: 0.8)
        addCircle(startIn: 1.6)
    }
    
    func addCircle() {
        let circle = CAShapeLayer()
        self.layer.insertSublayer(circle, at: 0)
        let rect = self.bounds.insetBy(dx: self.bounds.size.width / 2 - 3, dy: self.bounds.size.height / 2 - 3)
        circle.path = UIBezierPath(ovalIn: rect).cgPath
        //circle.fillColor = viewColor.cgColor
        circle.strokeColor = viewColor.cgColor
        circle.lineWidth = 2
    }
    
    func addCircle(startIn ms:Float) {
        let circle = CAShapeLayer()
        self.layer.insertSublayer(circle, at: 0)
        circle.path = nil
        //circle.fillColor = viewColor.cgColor
        circle.strokeColor = viewColor.cgColor
        circle.lineWidth = 2
        
        let scaleAnimation = CABasicAnimation(keyPath: "path")
        scaleAnimation.fromValue = UIBezierPath(ovalIn: CGRect(x: self.bounds.midX - CGFloat(minSize / 2), y: self.bounds.midY - CGFloat(minSize / 2), width: CGFloat(minSize), height: CGFloat(minSize))).cgPath
        scaleAnimation.toValue = UIBezierPath(ovalIn: self.bounds).cgPath
        
        let alphaAnimation = CABasicAnimation(keyPath: "opacity")
        alphaAnimation.fromValue = 1.0
        alphaAnimation.toValue = 0
        
        let group = CAAnimationGroup()
        group.animations = [scaleAnimation, alphaAnimation]
        group.beginTime = CACurrentMediaTime() + Double(ms)
        group.timeOffset = 0
        group.duration = 2.4
        group.repeatCount = Float.greatestFiniteMagnitude
        group.autoreverses = false
        group.fillMode = .both
        group.isRemovedOnCompletion = true
        group.delegate = self
        circle.add(group, forKey: "Circle\(ms)")
    }
}

