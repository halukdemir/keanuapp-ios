//
//  OutgoingMessageCell.swift
//  Keanu
//
//  Created by N-Pex on 05.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit

public class OutgoingMessageCell: MessageCell {
    public override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapStatusLabel(_:)))
        tap.cancelsTouchesInView = false
        self.statusLabel.addGestureRecognizer(tap)
        self.statusLabel.isUserInteractionEnabled = true
    }
    
    public override func addTextContent(text: NSAttributedString?) {
        messageTextView.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 12)
        super.addTextContent(text: text)
    }
    
    public override func didTapAvatarImage(_ sender: UITapGestureRecognizer) {
        super.didTapAvatarImage(sender)
        if let delegate = self.delegate, let roomBubbleData = self.roomBubbleData {
            delegate.didTapOutgoingAvatar(roomBubbleData: roomBubbleData, view: self.avatarImageView)
        }
    }
    
    @objc public func didTapStatusLabel(_ sender: UITapGestureRecognizer) {
        // This is the same as tapping the avatar image
        didTapAvatarImage(sender)
    }
}
