//
//  StoryGalleryViewController
//  Keanu
//
//  Created by N-Pex on 04.06.19.
//

import UIKit
import MatrixKit
import KeanuCore
import AVKit
import Photos
import MobileCoreServices

public protocol StoryGalleryViewControllerDelegate : class {
    func didPick(image:UIImage)
}

open class StoryGalleryViewController: UIViewController, UITabBarDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public weak var delegate:StoryGalleryViewControllerDelegate?
    
    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var collectionView: UICollectionView!
    var assets:[PHAsset]?
    var imageManager: PHCachingImageManager?
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        imageManager = PHCachingImageManager()
        let materialFont = UIFont(name: "MaterialIcons-Regular", size: 24)
        for item in tabBar?.items ?? [] {
            item.setTitleTextAttributes([.font: materialFont as Any], for: .normal)
        }
        tabBar.delegate = self
        updateDataSource()
    }
    
    func updateDataSource() {
        if PHPhotoLibrary.authorizationStatus() != PHAuthorizationStatus.authorized {
            PHPhotoLibrary.requestAuthorization({ (status: PHAuthorizationStatus) -> Void in
                if status != PHAuthorizationStatus.authorized {
                    self.onNoPermission()
                } else {
                    self.onPermission()
                }
            })
        } else {
            self.onPermission()
        }
    }
    
    func onNoPermission() {
        //TODO
    }
    
    func onPermission() {
        var allowedTypes:[PHAssetMediaType] = []
        if self.tabSelectedIndex == 0 {
            allowedTypes.append(.image)
        } else if self.tabSelectedIndex == 1 {
            allowedTypes.append(.video)
        } else if self.tabSelectedIndex == 2 {
            allowedTypes.append(.audio)
        }
        
        let options = PHFetchOptions()
        options.includeAssetSourceTypes = [.typeUserLibrary, .typeCloudShared, .typeiTunesSynced]
        let assets = PHAsset.fetchAssets(with: options)
        if assets.count > 0 {
            self.assets = []
            assets.enumerateObjects { (asset, index, stop) in
                if allowedTypes.contains(asset.mediaType) {
                    self.assets?.append(asset)
                }
            }
        } else {
            self.assets = nil
        }
        DispatchQueue.main.async {
            self.collectionView.dataSource = self
        }
    }
    
    var tabSelectedIndex:Int {
        if let item = tabBar.selectedItem, let items = tabBar.items, let idx = items.firstIndex(of: item) {
            return idx
        }
        return 0
    }
    
    func useAudioLayout() -> Bool {
        if tabSelectedIndex == 2 {
            return true
        }
        return false
    }
    
    public func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        updateDataSource()
        self.collectionView.reloadData()
    }
    
    open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.assets?.count ?? 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if useAudioLayout() {
            return CGSize(width: collectionView.frame.width, height: 100)
        } else {
            return CGSize(width: 72, height: 72)
        }
    }
    
    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if useAudioLayout() {
            let cell:UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "audioCell", for: indexPath)
            if let cell = cell as? StoryContribGridCell, let assets = self.assets {
                let asset = assets[indexPath.item]
                cell.titleLabel.text = asset.burstIdentifier
                if let date = asset.creationDate {
                    cell.detailLabel.text = Formatters.format(date: date)
                }
            }
            return cell
        } else {
            let cell:UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath)
            if let cell = cell as? StoryContribGridCell, let assets = self.assets, let manager = self.imageManager {
                let asset = assets[indexPath.item]
                manager.requestImage(for: asset, targetSize: cell.imageView.frame.size, contentMode: .aspectFill, options: nil) { (image, options) in
                    cell.imageView.image = image
                }
            }
            return cell
        }
        return UICollectionViewCell() // Shold never get here
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let assets = self.assets {
            let asset = assets[indexPath.item]
            if asset.mediaType == .image, let manager = self.imageManager {
                // Show that we are working here...
                workingOverlay.isHidden = false
                manager.requestImageData(for: asset, options: nil) { (data, string, orientation, options) in
                    if let image = data.flatMap({ UIImage(data: $0) }) {
                        self.delegate?.didPick(image: image)
                    }
                    self.workingOverlay.isHidden = true
                    self.navigationController?.popViewController(animated: true)
                }
            } else if asset.mediaType == .audio {
                //TODO
            }
        }
    }
    
    func pickDocument() {
                    let documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: [
                        kUTTypePDF as String,
                        kUTTypeVideo as String,
                        kUTTypeImage as String,
                        kUTTypeAudio as String
                        ], in: UIDocumentPickerMode.import)
//                    documentPicker.delegate = self
                    documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                    self.present(documentPicker, animated: true, completion: nil)
    }
}

extension StoryGalleryViewController: UIDocumentPickerDelegate {
    private func documentPicker(controller: UIDocumentPickerViewController, didPickDocumentAtURL url: NSURL) {
        if controller.documentPickerMode == UIDocumentPickerMode.import {
            print("Import URL: \(url)")
        }
    }
}
