//
//  StoryAddMediaViewController
//  Keanu
//
//  Created by N-Pex on 31.05.19.
//

import UIKit
import MatrixKit
import KeanuCore
import AVKit
import Photos

/**
 A small class to keep track of media.
 */
public class MediaInfo: NSObject {
    public var url:URL?
    public var mime:String?
    public var image:UIImage?
    
    init(url:URL?, mime:String?, image:UIImage?) {
        super.init()
        self.url = url
        self.mime = mime
        self.image = image
    }
    
    public var isImage:Bool {
        guard image == nil else { return true }
        if let mime = mime, mime.starts(with: "image/") {
            return true
        }
        return false
    }
    
    public var isAudio:Bool {
        if let mime = mime, mime.starts(with: "audio/") {
            return true
        }
        return false
    }
    
    public var isVideo:Bool {
        if let mime = mime, mime.starts(with: "video/") {
            return true
        }
        return false
    }

}

public protocol StoryAddMediaViewControllerDelegate: class {
    func didSelectMedia(_ media:[MediaInfo])
}

open class StoryAddMediaViewController: UIViewController, AVCapturePhotoCaptureDelegate, StoryGalleryViewControllerDelegate, AVAudioRecorderDelegate, AVCaptureFileOutputRecordingDelegate {
    
    public weak var delegate: StoryAddMediaViewControllerDelegate?
    
    @IBOutlet weak var cameraPreviewView: UIView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var cameraPulseButton: PulseButtonView!
    @IBOutlet weak var micButton: UIButton!
    @IBOutlet weak var micPulseButton: PulseButtonView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var flipCameraButton: UIButton!
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var audioPreview: UIView!
    @IBOutlet weak var audioPreviewVisualizer: AudioVisualizerView!
    @IBOutlet weak var audioPreviewPlayer: AudioPlayer!
    @IBOutlet weak var audioRecordingVisualizer: AudioVisualizerView!
    @IBOutlet weak var videoPreview: UIView!
    var videoPreviewPlayer: AVPlayer?
    var videoPreviewPlayerController: AVPlayerViewController?

    @IBOutlet var cancelButton: UIBarButtonItem! // Don't make weak!
    
    var cameraCaptureSession: AVCaptureSession?
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    var audioRecorder:AVAudioRecorder?
    
    var currentImage:MediaInfo?
    var currentAudio:MediaInfo?
    var currentVideo:MediaInfo?
    
    var timerStartVideoRecording: Timer? = nil {
        willSet {
            timerStartVideoRecording?.invalidate()
        }
    }
    
    var timerStartAudioRecording: Timer? = nil {
        willSet {
            timerStartAudioRecording?.invalidate()
        }
    }
    
    var timerAudioRecordingDecibel: Timer? = nil {
        willSet {
            timerAudioRecordingDecibel?.invalidate()
        }
    }
    
    /**
     Instantiate the `StoryAddMediaViewController`from the "StoryAddMediaViewController" storyboard.
     */
    class func instantiate(delegate:StoryAddMediaViewControllerDelegate?) -> StoryAddMediaViewController? {
        if  let vc = UIStoryboard.overriddenOrLocal(name: "StoryAddMediaViewController")
            .instantiateInitialVCWithDelegate() as? StoryAddMediaViewController {
            vc.delegate = delegate
            return vc
        }
        return nil
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        imagePreview.isHidden = true
        audioPreview.isHidden = true
        videoPreview.isHidden = true
        cameraButton.isEnabled = false
        cameraPreviewView.isHidden = true
        cameraPulseButton.isHidden = true
        micPulseButton.isHidden = true
        audioRecordingVisualizer.isHidden = true
        setViewerMode(false)
        requestShowCamera()
    }
    
    func getDevice(front:Bool) -> AVCaptureDevice? {
        var captureDevice: AVCaptureDevice?
        for device in AVCaptureDevice.devices(for: AVMediaType.video) {
            if device.position == (front ? AVCaptureDevice.Position.front : AVCaptureDevice.Position.back) {
                captureDevice = device
                break
            }
        }
        return captureDevice
    }
    
    func startCameraPreview() {
        var captureDevice = getDevice(front: true)
        if captureDevice == nil {
            captureDevice = getDevice(front: false)
        }
        guard let device = captureDevice else { return }
        do {
            let input = try AVCaptureDeviceInput(device: device)
            var output:AVCaptureOutput?
            if #available(iOS 10.0, *) {
                output = AVCapturePhotoOutput()
                if let out = output as? AVCapturePhotoOutput {
                    out.isHighResolutionCaptureEnabled = true
                    out.isLivePhotoCaptureEnabled = false //out.isLivePhotoCaptureSupported
                }
            } else {
                output = AVCaptureStillImageOutput()
                if let out = output as? AVCaptureStillImageOutput {
                    out.isHighResolutionStillImageOutputEnabled = true
                }
            }
            if let output = output {
                cameraCaptureSession = AVCaptureSession()
                if let cameraCaptureSession = cameraCaptureSession {
                    cameraCaptureSession.addInput(input)
                    cameraCaptureSession.addOutput(output)
                    cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: cameraCaptureSession)
                    cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
                    cameraPreviewLayer?.frame = cameraPreviewView.bounds
                    cameraPreviewView.layer.addSublayer(cameraPreviewLayer!)
                    cameraCaptureSession.startRunning()
                    self.cameraButton.isEnabled = true
                    self.cameraPreviewView.isHidden = false
                }
            }
        } catch {
            print(error)
        }
    }
    
    public func requestShowCamera() {
        self.checkCameraAuthorization { authorized in
            if authorized {
                DispatchQueue.main.async {
                    self.startCameraPreview()
                }
            } else {
                print("Permission to use camera denied.")
                // TODO
            }
        }
    }
    
    func checkCameraAuthorization(_ completionHandler: @escaping ((_ authorized: Bool) -> Void)) {
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .authorized:
            //The user has previously granted access to the camera.
            completionHandler(true)
            
        case .notDetermined:
            // The user has not yet been presented with the option to grant video access so request access.
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { success in
                completionHandler(success)
            })
            
        case .denied:
            // The user has previously denied access.
            completionHandler(false)
            
        case .restricted:
            // The user doesn't have the authority to request access e.g. parental restriction.
            completionHandler(false)
        @unknown default:
            completionHandler(false)
        }
    }
    
    func takeSnapshot() {
        guard let capturePhotoOutput = self.cameraCaptureSession?.outputs.first as? AVCaptureOutput else { return }
        guard let videoPreviewLayerOrientation = cameraPreviewLayer?.connection?.videoOrientation else { return }
        DispatchQueue.main.async {
            // Update the photo output's connection to match the video orientation of the video preview layer.
            if let photoOutputConnection = capturePhotoOutput.connection(with: AVMediaType.video) {
                photoOutputConnection.videoOrientation = videoPreviewLayerOrientation
            }
            
            if #available(iOS 10.0, *) {
                let photoSettings = AVCapturePhotoSettings()
                photoSettings.isAutoStillImageStabilizationEnabled = true
                photoSettings.isHighResolutionPhotoEnabled = true
                //photoSettings.flashMode = .auto
                (capturePhotoOutput as? AVCapturePhotoOutput)?.capturePhoto(with: photoSettings, delegate: self)
            } else {
                // Fallback on earlier versions
                if let connection = capturePhotoOutput.connection(with: AVMediaType.video) {
                    (capturePhotoOutput as? AVCaptureStillImageOutput)?.captureStillImageAsynchronously(from: connection, completionHandler: { (sampleBuffer, error) -> Void in
                        
                        if let sampleBuffer = sampleBuffer {
                            let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                            let dataProvider = CGDataProvider(data: imageData! as CFData)
                            let cgImageRef = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: .defaultIntent)
                            let image = UIImage(cgImage: cgImageRef!, scale: 1, orientation: .right)
                            self.processCapturedImage(image: image, flipX: self.usingFrontCamera())
                        }
                    })
                }
            }
        }
    }
    
    @IBAction func didPressSendButton(_ sender: AnyObject) {
        var addedMedia:[MediaInfo] = []
        addedMedia.appendOptional(currentImage)
        addedMedia.appendOptional(currentAudio)
        addedMedia.appendOptional(currentVideo)
        delegate?.didSelectMedia(addedMedia)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didPressSwitchCameraButton(_ sender: AnyObject) {
        if let input = cameraCaptureSession?.inputs.first as? AVCaptureDeviceInput {
            let position = input.device.position
            if let newCaptureDevice = getDevice(front: (position == .back)) {
                do {
                    let newInput = try AVCaptureDeviceInput(device: newCaptureDevice)
                    cameraCaptureSession?.removeInput(input)
                    cameraCaptureSession?.addInput(newInput)
                } catch {
                    print(error)
                }
            }
        }
    }
    
    func usingFrontCamera() -> Bool {
        return ((self.cameraCaptureSession?.inputs.first as? AVCaptureDeviceInput)?.device.position == .front)
    }
    
    @available(iOS 10.0, *)
    public func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        if let error = error {
            print(error.localizedDescription)
        }
        if let sampleBuffer = photoSampleBuffer, let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: nil) {
            processCapturedImage(image: UIImage(data: dataImage, scale: 1.0), flipX: usingFrontCamera())
        }
    }
    
    func processCapturedImage(image:UIImage?, flipX:Bool) {
        
        guard let image = image?.fixOrientation() else {
            //hideCamera()
            return
        }
        cameraCaptureSession?.stopRunning()
        currentImage = MediaInfo(url: nil, mime: "image/png", image: image)
        setViewerMode(true)
    }
    
    @IBAction func cameraButtonTouchDown(_ sender: Any) {
        timerStartVideoRecording =  Timer.scheduledTimer(
            timeInterval: TimeInterval(0.5),
            target      : self,
            selector    : #selector(StoryAddMediaViewController.timerStartVideoRecordingElapsed),
            userInfo    : nil,
            repeats     : false)
    }
    
    @IBAction func cameraButtonTouchUp(_ sender: Any) {
        cameraPulseButton.isHidden = true
        if timerStartVideoRecording == nil {
            if let videoOutput = cameraCaptureSession?.outputs[1] as? AVCaptureMovieFileOutput {
                videoOutput.stopRecording()
            } else {
                setViewerMode(false)
            }
        } else {
            timerStartVideoRecording = nil
            takeSnapshot()
        }
    }
    
    @objc func timerStartVideoRecordingElapsed() {
        timerStartVideoRecording = nil
        cameraPulseButton.isHidden = false
        
        let videoOutput = AVCaptureMovieFileOutput()
        cameraCaptureSession?.addOutput(videoOutput)
        videoOutput.startRecording(to: getAudioFileURL(), recordingDelegate: self)
    }
    
    @IBAction func cameraButtonTouchUpOutside(_ sender: Any) {
        timerStartVideoRecording = nil
    }
    
    @IBAction func micButtonTouchDown(_ sender: Any) {
        timerStartAudioRecording =  Timer.scheduledTimer(
            timeInterval: TimeInterval(0.5),
            target      : self,
            selector    : #selector(StoryAddMediaViewController.timerStartAudioRecordingElapsed),
            userInfo    : nil,
            repeats     : false)
    }
    
    @IBAction func micButtonTouchUp(_ sender: Any) {
        micPulseButton.isHidden = true
        if timerStartAudioRecording == nil {
            // Stop audio recording
            finishRecording(success: true)
        } else {
            timerStartAudioRecording = nil
            
            // Tap. Open gallery
            performSegue(withIdentifier: "showGallery", sender: self)
        }
    }
    
    @objc func timerStartAudioRecordingElapsed() {
        timerStartAudioRecording = nil
        startAudioRecordingIfPermitted()
    }
    
    @IBAction func micButtonTouchUpOutside(_ sender: Any) {
        timerStartAudioRecording = nil
        finishRecording(success: false)
    }
    
    @IBAction func didClickCancelButton(_ sender: Any) {
        setViewerMode(false)
    }
    
    func showCancelButton(_ show:Bool) {
        DispatchQueue.main.async {
            self.navigationItem.rightBarButtonItem = (show ? self.cancelButton : nil)
        }
    }
    
    func setViewerMode(_ viewerMode:Bool) {
        if viewerMode {
            imagePreview.image = currentImage?.image
            imagePreview.isHidden = false
            if currentVideo != nil {
                imagePreview.isHidden = true
                videoPreview.isHidden = false
                micButton.isHidden = true
                audioPreview.isHidden = true
                showVideoPreview(url: currentVideo?.url)
            } else {
                videoPreview.isHidden = true
                stopVideoPreview()
                if currentAudio != nil {
                    micButton.isHidden = true
                    audioPreview.isHidden = false
                
                } else {
                    micButton.isHidden = false
                    audioPreview.isHidden = true
                }
            }
            sendButton.isHidden = false
            flipCameraButton.isHidden = true
            showCancelButton(true)
        } else {
            currentAudio = nil
            currentImage = nil
            self.imagePreview.image = nil
            self.imagePreview.isHidden = true
            self.audioPreview.isHidden = true
            self.videoPreview.isHidden = true
            micButton.isHidden = false
            sendButton.isHidden = true
            flipCameraButton.isHidden = false
            showCancelButton(false)
            cameraCaptureSession?.startRunning()
        }
    }
    
    open override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let gallery = segue.destination as? StoryGalleryViewController {
            gallery.delegate = self
        }
        super.prepare(for: segue, sender: sender)
    }
    
    public func didPick(image: UIImage) {
        currentImage = MediaInfo(url: nil, mime: "image/png", image: image)
        setViewerMode(true)
    }
    
    //MARK: Audio recording
    func getAudioFileURL() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0].appendingPathComponent("recording.m4a")
    }

    func startAudioRecordingIfPermitted() {
        let recordingSession = AVAudioSession.sharedInstance()
        do {
            if #available(iOS 10.0, *) {
                try recordingSession.setCategory(.playAndRecord, mode: .default)
            }
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [weak self] allowed in
                guard let strongSelf = self else { return }
                DispatchQueue.main.async {
                    if !allowed {
                        strongSelf.micButton.isEnabled = false
                    } else {
                        strongSelf.startAudioRecording()
                    }
                }
            }
        } catch {
            // failed to record!
            self.micButton.isEnabled = false
        }
    }
    
    func startAudioRecording() {
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 16000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: getAudioFileURL(), settings: settings)
            audioRecorder?.delegate = self
            audioRecorder?.isMeteringEnabled = true
            timerAudioRecordingDecibel = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(decibelTimerUpdate(_:)), userInfo: nil, repeats: true)
            audioRecorder?.record()
            micPulseButton.isHidden = false
            audioRecordingVisualizer.reset()
            audioRecordingVisualizer.isHidden = false
        } catch {
            finishRecording(success: false)
        }
    }
    
    func finishRecording(success: Bool) {
        audioRecordingVisualizer.isHidden = true
        micPulseButton.isHidden = true
        timerAudioRecordingDecibel = nil
        let recordingTime = audioRecorder?.currentTime ?? TimeInterval(0)
        audioRecorder?.stop()
        audioRecorder = nil
        if success, recordingTime > 0.5 {
            currentAudio = MediaInfo(url: getAudioFileURL(), mime: "audio/x-m4a", image: nil)
            audioPreviewVisualizer.loadAudioFile(url: getAudioFileURL())
            audioPreviewPlayer.loadAudioFile(url: getAudioFileURL())
            setViewerMode(true)
        } else {
        }
    }
    
    public func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    @objc func decibelTimerUpdate(_ sender: Any) {
        if let audioRecorder = audioRecorder {
            audioRecorder.updateMeters()
            let decibel = audioRecorder.averagePower(forChannel: 0)
            
            var scale:Float = 0
            
            //Values for human speech range quiet to loud
            let mindB:Float = -80
            let maxdB:Float = -10
            if decibel >= maxdB {
                //too loud
                scale = 1
            } else if decibel >= mindB,decibel <= maxdB {
                //normal voice
                let powerFactor:Float = 20
                let mindBScale:Float = pow(10, mindB / powerFactor);
                let maxdBScale:Float = pow(10, maxdB / powerFactor);
                let linearScale:Float = pow (10, decibel / powerFactor);
                let scaleMin:Float = 0;
                let scaleMax:Float = 1;
                //Get a value between 0 and 1 for mindB & maxdB values
                scale = ( ((scaleMax - scaleMin) * (linearScale - mindBScale)) / (maxdBScale - mindBScale)) + scaleMin;
            }
            setAudioLevel(scale)
            //let color = UIColor(red: 1, green: CGFloat(1-scale), blue: CGFloat(1-scale), alpha: 1)
            //view.backgroundColor = color
        }
    }
    
    private func setAudioLevel(_ scale:Float) {
        audioRecordingVisualizer.addSingleDataValue(scale)
    }
    
    public func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        if error == nil {
            currentVideo = MediaInfo(url: getAudioFileURL(), mime: "video/mpeg", image: nil)
        }
        setViewerMode(true)
    }
    
    func showVideoPreview(url: URL?) {
        if let videoPreviewPlayer = videoPreviewPlayer {
            videoPreviewPlayer.pause()
        }
        guard let url = url else { return }
        videoPreviewPlayer = AVPlayer(url: url)
        if videoPreviewPlayerController == nil {
            videoPreviewPlayerController = AVPlayerViewController()
            if let controller = videoPreviewPlayerController {
                self.addChild(controller)
                controller.view.frame = videoPreview.frame
                videoPreview.addSubview(controller.view)
            }
        }
        guard let player = videoPreviewPlayer, let controller = videoPreviewPlayerController else { return }
        controller.player = player
        player.play()
    }
    
    func stopVideoPreview() {
        videoPreviewPlayer?.pause()
    }
}



fileprivate extension UIImage {
    func fixOrientation() -> UIImage {
        
        // No-op if the orientation is already correct
        if ( self.imageOrientation == .up ) {
            return self;
        }
        
        // We need to calculate the proper transformation to make the image upright.
        // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
        var transform: CGAffineTransform = .identity
        
        if ( self.imageOrientation == .down || self.imageOrientation == .downMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: self.size.height)
            transform = transform.rotated(by: .pi)
        }
        
        if ( self.imageOrientation == .left || self.imageOrientation == .leftMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.rotated(by: .pi/2)
        }
        
        if ( self.imageOrientation == .right || self.imageOrientation == .rightMirrored ) {
            transform = transform.translatedBy(x: 0, y: self.size.height);
            transform = transform.rotated(by: -.pi/2);
        }
        
        if ( self.imageOrientation == .upMirrored || self.imageOrientation == .downMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        }
        
        if ( self.imageOrientation == .leftMirrored || self.imageOrientation == .rightMirrored ) {
            transform = transform.translatedBy(x: self.size.height, y: 0);
            transform = transform.scaledBy(x: -1, y: 1);
        }
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx: CGContext = CGContext(data: nil, width: Int(self.size.width), height: Int(self.size.height),
                                       bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0,
                                       space: self.cgImage!.colorSpace!,
                                       bitmapInfo: self.cgImage!.bitmapInfo.rawValue)!;
        
        ctx.concatenate(transform)
        
        if ( self.imageOrientation == .left ||
            self.imageOrientation == .leftMirrored ||
            self.imageOrientation == .right ||
            self.imageOrientation == .rightMirrored ) {
            ctx.draw(self.cgImage!, in: CGRect(x: 0.0,y: 0.0,width: self.size.height,height: self.size.width))
        } else {
            ctx.draw(self.cgImage!, in: CGRect(x: 0.0,y: 0.0,width: self.size.width,height: self.size.height))
        }
        
        // And now we just create a new UIImage from the drawing context and return it
        return UIImage(cgImage: ctx.makeImage()!)
    }
}
