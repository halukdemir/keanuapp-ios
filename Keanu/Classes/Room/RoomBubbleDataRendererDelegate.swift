//
//  RoomBubbleDataRendererDelegate.swift
//  Keanu
//
//  Created by N-Pex on 16.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK
import MatrixKit

/**
 This protocol may be implemented to listen to events from bubble renders (=cells)
 */
public protocol RoomBubbleDataRendererDelegate {
    func didTapIncomingAvatar(roomBubbleData:RoomBubbleData, view:UIView)
    func didTapOutgoingAvatar(roomBubbleData:RoomBubbleData, view:UIView)
    func didTapMediaAttachmentItem(roomBubbleData:RoomBubbleData, view:UIView?, attachment: MXKAttachment?)
    
    /**
     Callback for when the user has selected "verify" from an unknown devices bubble.
     */
    func unknownDevicesVerify(roomBubbleData:RoomBubbleData, devices:[String:[String]]?, view:UIView?)
    
    /**
     Callback for when the user has tapped "Re-request keys"
     */
    func requestRoomKeys(roomBubbleData:RoomBubbleData)
}

extension RoomBubbleDataRendererDelegate {
    func unknownDevicesVerify(roomBubbleData:RoomBubbleData, devices:[String:[String]]?, view:UIView?) {
        // Default implementation does nothing
    }
    
    func requestRoomKeys(roomBubbleData:RoomBubbleData) {
        // Default implementation does nothing
    }
}
