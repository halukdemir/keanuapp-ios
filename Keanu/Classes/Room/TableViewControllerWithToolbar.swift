//
//  TableViewControllerWithToolbar.swift
//  Keanu
//
//  Created by N-Pex on 02.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import PureLayout

/**
 A base UITableViewController that listens for keyboard events, and that (optionally) displays a toolbar on top of the keyboard view.
 */
open class TableViewControllerWithToolbar:UITableViewController {
    private var tableViewBottomLayoutConstraint:NSLayoutConstraint?
    private var bottomLayoutConstraint:NSLayoutConstraint?
    private var _toolbar:UIView?
    
    private var originalTableView :UITableView?
    override open var tableView: UITableView! {
        get {
            if originalTableView == nil {
                // View not created yet, create it now
                let _ = super.tableView
            }
            return originalTableView
        }
        set {
            originalTableView = newValue
        }
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        originalTableView = super.tableView
        let view = UIView()
        view.frame = tableView.frame
        self.view = view
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.autoPinEdgesToSuperviewSafeArea(with: UIEdgeInsets.zero, excludingEdge: .bottom)
        tableViewBottomLayoutConstraint = tableView.autoPinEdge(toSuperviewSafeArea: .bottom)
        self.bottomLayoutConstraint = self.tableViewBottomLayoutConstraint
        
        // Keyboard handling from here:
        // https://stackoverflow.com/questions/43332308/ios-tableview-controller-keyboard-offset-example-project-inside
        registerForKeyboardEvents()
    }
    
    open var toolbarView:UIView? {
        get {
            return _toolbar
        }
        set {
            if newValue != _toolbar {
                if let oldToolbar = _toolbar {
                    oldToolbar.removeFromSuperview()
                }
                _toolbar = newValue
                
                // Make sure view is created
                let _ = self.view
                
                // Remove whatever the tableView bottom was pinned to
                if let bottomConstraint = self.tableViewBottomLayoutConstraint {
                    bottomConstraint.autoRemove()
                }
                
                if let toolbar = _toolbar {
                    view.addSubview(toolbar)
                    self.bottomLayoutConstraint = toolbar.autoPinEdge(toSuperviewSafeArea: .bottom)
                    toolbar.autoPinEdge(.leading, to: .leading, of: self.view, withOffset: 0)
                    toolbar.autoPinEdge(.trailing, to: .trailing, of: self.view, withOffset: 0)
                    self.tableViewBottomLayoutConstraint = tableView.autoPinEdge(.bottom, to: .top, of: toolbar, withOffset: 0)
                } else {
                    // Remove toolbar
                    self.tableViewBottomLayoutConstraint = tableView.autoPinEdge(.bottom, to: .bottom, of: self.view, withOffset: 0)
                    self.bottomLayoutConstraint = self.tableViewBottomLayoutConstraint
                }
            }
        }
    }
    
    private func registerForKeyboardEvents() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowNotification(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHideNotification(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerForKeyboardEvents()
    }
    
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // remove the observers so the code won't be called all the time
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShowNotification(notification: NSNotification) {
        updateBottomLayoutConstraintWithNotification(notification:notification)
    }
    
    @objc func keyboardWillHideNotification(notification: NSNotification) {
        updateBottomLayoutConstraintWithNotification(notification:notification)
    }
    
    func updateBottomLayoutConstraintWithNotification(notification: NSNotification) {
        let userInfo = notification.userInfo!
        
        // get data from the userInfo
        let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let convertedKeyboardEndFrame = view.convert(keyboardEndFrame, from: view.window)
        let rawAnimationCurve = (notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as! NSNumber).uint32Value << 16
        let animationCurve = UIView.AnimationOptions(rawValue: UInt(rawAnimationCurve))
        
        let newValue = view.bounds.maxY - convertedKeyboardEndFrame.minY
        var delta:CGFloat = 0
        if let bottomLayoutConstraint = bottomLayoutConstraint {
            delta = newValue + bottomLayoutConstraint.constant
            bottomLayoutConstraint.constant = -newValue
        }
        
        // animate the changes
        UIView.animate(withDuration: animationDuration, delay: 0.0, options: [.beginFromCurrentState, animationCurve], animations: {
            self.view.layoutIfNeeded()
            self.tableView.contentOffset.y += delta
        }, completion: nil)
    }
    
    // MARK: Actions
    
    /**
     Dismisses ourselves, animated.
     Handles being in a `UINavigationController` gracefully.
     
     Can be a callback for simple "Cancel", "Done" etc. buttons.
     */
    @IBAction func dismiss() {
        if let nav = navigationController {
            nav.dismiss(animated: true)
        }
        else {
            dismiss(animated: true)
        }
    }
}
