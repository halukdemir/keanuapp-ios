//
//  AddAccountViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 01.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixKit

open class AddAccountViewController: UIViewController, MXKAuthenticationViewControllerDelegate {

    class var storyboard: UIStoryboard {
        return UIStoryboard.overriddenOrLocal(name: "Onboarding")
    }

    /**
     Instantiate the `AddAccountViewController`from the "Onboarding" storyboard.

     If called like this, the ChooseFriendsViewController will be configured
     with `calledInline = true`.

     - returns: The `UINavigationController` containing the `AddAccountViewController` as its top view controller.
     */
    open class func instantiate() -> UINavigationController? {
        if let navC = storyboard.instantiateInitialViewController() as? UINavigationController,
            let addAccountVc = storyboard.instantiateViewController(withIdentifier: "addAccount")
                as? AddAccountViewController {

            addAccountVc.calledInline = true

            navC.pushViewController(addAccountVc, animated: false)

            return navC
        }

        return nil
    }

    var calledInline = false
    
    var lastType = MXKAuthenticationTypeLogin
    
    lazy var signInVc: AuthenticationViewController = {
        let vc = AuthenticationViewController(MXKAuthenticationTypeLogin)
        vc.delegate = self
        
        return vc
    }()
    
    lazy var signUpVc: AuthenticationViewController = {
        let vc = AuthenticationViewController(MXKAuthenticationTypeRegister)
        vc.delegate = self
        
        return vc
    }()
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        navigationController?.setNavigationBarHidden(!calledInline, animated: animated)
    }
    
    // MARK: Actions

    /**
     Callback for "Create an Account" button.
    */
    @IBAction func signUp() {
        lastType = MXKAuthenticationTypeRegister
        navigationController?.pushViewController(signUpVc, animated: true)
    }
    
    /**
     Callback for "Sign into Existing Account" button.
    */
    @IBAction func signIn() {
        lastType = MXKAuthenticationTypeLogin
        navigationController?.pushViewController(signInVc, animated: true)
    }

    /**
     If we were called inline, dismiss ourselves.

     If we were started from the app delegate, call that to jump to the main
     flow.
    */
    @IBAction func done() {
        if calledInline {
            self.navigationController?.dismiss(animated: true)
        }
        else {
            navigationController?.popToRootViewController(animated: true)

            // Leave the Onboarding scene flow, switch to the main flow instead.
            UIApplication.shared.startMainFlow(forwardIfNoFriends: true)
        }
    }

    // MARK: MXKAuthenticationViewControllerDelegate

    public func authenticationViewController(_ authenticationViewController: MXKAuthenticationViewController?,
                                      didLogWithUserId userId: String?) {
        
        // If we don't have push permissions, ask for them.
        PushManager.shared.hasPushPermissions { (hasPermissions) in
            if !hasPermissions {
                self.performSegue(withIdentifier: "enablePushSegue", sender: self)
            } else {
                self.done()
            }
        }
    }

    override open func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let enablePushVC = segue.destination as? EnablePushViewController {
            // Add ourselves as delegate. When the push view controller is done, we decide whether to go to the "congrats" view or just dismiss ourselves.
            enablePushVC.delegate = self
        }
        super.prepare(for: segue, sender: sender)
    }
}

extension AddAccountViewController: EnablePushViewControllerDelegate {
    public func done(enablePushViewController: EnablePushViewController) {
        done()
    }
}
