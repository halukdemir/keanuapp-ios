//
//  EnablePushViewController.swift
//  Keanu
//
//  Created by N-Pex on 16.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import UIKit

/**
 Delegate called by the EnablePushViewController when it needs to close.
 */
public protocol EnablePushViewControllerDelegate {
    func done(enablePushViewController: EnablePushViewController)
}

open class EnablePushViewController: UIViewController {

    public var delegate: EnablePushViewControllerDelegate?
    var authObserver: NSObjectProtocol?
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        authObserver = NotificationCenter.default.addObserver(forName: PushManager.didGetPushManagerAuthorizationResult, object: nil, queue: OperationQueue.main) { (notification) in
            let granted = notification.userInfo?["granted"] as? Bool ?? false
            if granted {
                self.close()
            }
        }
    }
    
    override open func viewDidDisappear(_ animated: Bool) {
        if let observer = authObserver {
            NotificationCenter.default.removeObserver(observer)
            authObserver = nil
        }
        super.viewDidDisappear(animated)
    }
    
    func close() {
        if let delegate = delegate {
            delegate.done(enablePushViewController: self)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    /// Will show a prompt to bring user into system settings
    func showPromptForSystemSettings() {
        let alert = UIAlertController(title: "Enable Push in Settings".localize(), message: nil, preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings".localize(), style: .default, handler: { (action: UIAlertAction) -> Void in
            let appSettings = URL(string: UIApplication.openSettingsURLString)
            UIApplication.shared.openURL(appSettings!)
        })
        let cancelAction = UIAlertAction(title: "Cancel".localize(), style: .cancel, handler: nil)
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: Actions

    @IBAction func enablePushPressed(_ sender: AnyObject) {
        PushManager.shared.hasAskedForPushPermissions { (asked) in
            if !asked {
                PushManager.shared.registerUserNotificationSettings()
            } else {
                // We have already asked user, so show a prompt to go to settings
                self.showPromptForSystemSettings()
            }
        }
    }
    
    @IBAction func skipButtonPressed(_ sender: AnyObject) {
        self.close()
    }
}
