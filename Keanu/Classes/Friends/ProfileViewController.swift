//
//  ProfileViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 14.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK
import KeanuCore
import Eureka
import ViewRow

open class ProfileViewController: FormViewController {

    open var friend: Friend?

    private let grayBgImg = UIColor(hexString: "#3F000000").asImage() ?? UIImage()
    private var navBarBgImg: UIImage?
    private var navBarShadowImg: UIImage?
    private var navBarTintColor: UIColor?

    public let avatarRow = ViewRow<AvatarView>() {
        let avatar = AvatarView()
        avatar.contentMode = .scaleAspectFill
        avatar.clipsToBounds = true
        avatar.rounded = false
        avatar.size = 640

        $0.cell.view = avatar
        $0.cell.contentView.addSubview(avatar)

        $0.cell.viewRightMargin = 0
        $0.cell.viewLeftMargin = 0
        $0.cell.viewTopMargin = 0
        $0.cell.viewBottomMargin = 0

        $0.cell.height = { 200 }
    }

    public let nameRow = NameRow() {
        $0.placeholder = "Name of Your Friend".localize()

        $0.cell.accessoryView = UIImageView(image: UIImage(
            named: "ic_edit", in: Bundle(for: ProfileViewController.self), compatibleWith: nil))
        $0.cell.accessoryView?.frame = CGRect(x: 0, y: 0, width: 15, height: 15)

        $0.add(rule: RuleRequired())
    }

    public let shareRow = ButtonRow() {
        $0.title = "Share Profile".localize()
        }
        .cellUpdate { cell, _ in
            cell.textLabel?.textAlignment = .natural
    }

    public let devicesRow = ButtonRow() {
        $0.title = "View Devices and Keys".localize()
        }
        .cellUpdate { cell, _ in
            cell.textLabel?.textAlignment = .natural
    }


    public init() {
        super.init(style: .plain)
    }

    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override open func viewDidLoad() {
        super.viewDidLoad()

        // Don't inset scroll view, so avatar image is behind status and nav bar.
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }

        // Inset for the tab bar, though, if there is one.
        if let tabBarC = tabBarController {
            let insets = UIEdgeInsets(top: tableView.contentInset.top,
                                      left: tableView.contentInset.left,
                                      bottom: tabBarC.tabBar.bounds.size.height,
                                      right: tableView.contentInset.right)

            tableView.contentInset = insets
            tableView.scrollIndicatorInsets = insets
        }

        if let friend = friend, !friend.isStored {
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                barButtonSystemItem: .add, target: self, action: #selector(addFriend))
        }

        // On iOS 11 and 12, the title text color of the preceding scene becomes
        // white, if we don't do this hack.
        // Unfortunately, we need to hardcode the color black here, which may
        // become a problem after a redesign. Unfortunately I couldn't find a
        // better solution until now.
        if #available(iOS 11.0, *) {
            if let bar = navigationController?.navigationBar {
                if let attr = bar.titleTextAttributes {
                    if attr[.foregroundColor] == nil {
                        bar.titleTextAttributes?[.foregroundColor] = UIColor.black
                    }
                }
                else {
                    bar.titleTextAttributes = [.foregroundColor: UIColor.black]
                }
            }
        }

        tableView.backgroundColor = .groupTableViewBackground
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        tableView.separatorStyle = .none

        createForm()
    }

    override open func viewWillAppear(_ animated: Bool) {

        // Make the navigation bar transparent.
        if let bar = navigationController?.navigationBar {
            if navBarBgImg == nil {
                navBarBgImg = bar.backgroundImage(for: .default)
            }

            // Replaces white translucent background with a black very light translucent background.
            bar.setBackgroundImage(grayBgImg, for: .default)

            if navBarShadowImg == nil {
                navBarShadowImg = bar.shadowImage
            }
            bar.shadowImage = UIImage() // Removes thin shadow at bottom.

            if navBarTintColor == nil {
                navBarTintColor = bar.tintColor
            }
            bar.tintColor = .white // White "back" button.

            bar.barStyle = .black // Forces white status bar.
        }

        super.viewWillAppear(true)
    }

    override open func viewWillDisappear(_ animated: Bool) {

        // All of this needs to be reset, otherwise the preceding scene will
        // change its appearance, too!
        if let bar = navigationController?.navigationBar {
            bar.setBackgroundImage(navBarBgImg, for: .default)
            bar.shadowImage = navBarShadowImg
            bar.tintColor = navBarTintColor
            bar.barStyle = .default
        }

        super.viewWillDisappear(true)
    }

    // MARK: UITableViewDelegate

    public func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as? UITableViewHeaderFooterView)?.contentView.backgroundColor = .groupTableViewBackground
    }

    /**
     Creates a very small light gray divider line for the last section.
     */
    public func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        if section == tableView.numberOfSections - 2 {
            (view as? UITableViewHeaderFooterView)?.contentView.backgroundColor = .groupTableViewBackground
        }
    }

    /**
     Creates a very small light gray divider line for the last section.
     */
    open override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == tableView.numberOfSections - 2 {
            return 1
        }

        return super.tableView(tableView, heightForFooterInSection: section)
    }


    // MARK: Public Methods

    open func createForm() {
        avatarRow.view?.load(friend: friend)
        nameRow.value = friend?.name

        form
            +++ avatarRow

            <<< nameRow.cellUpdate { cell, row in
                row.value = self.friend?.name
                cell.textField?.font = .boldSystemFont(ofSize: 25)
                }
                .onChange {
                    if let value = $0.value {
                        self.friend?.name = value
                    }
            }

            <<< LabelRow() {
                $0.title = friend?.matrixId
            }

            +++ ButtonRow() {
                $0.title = "Chat with %".localize(value: friend?.name ?? "Your Friend".localize())
                }
                .cellUpdate { cell, _ in
                    cell.textLabel?.textAlignment = .natural
                }
                .onCellSelection { _, _ in
                    if let friend = self.friend {
                        UIApplication.shared.openRoom(friend: friend)
                    }
            }

            <<< shareRow.onCellSelection { cell, _ in
                if let user = self.friend?.user {
                    self.present(ProfileViewController.shareProfiles([user], cell), animated: true)
                }
            }

            <<< devicesRow.onCellSelection { _, _ in
                let vc = DevicesViewController()
                vc.friend = self.friend

                self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    @objc open func addFriend() {
        if let friend = friend {
            FriendsManager.shared.add(friend)

            let title = "Friend Added".localize()
            let message = "% was added to your friends list!".localize(value: friend.name)

            // Remove button, lost its meaning.
            navigationItem.rightBarButtonItem = nil

            AlertHelper.present(self, message: message, title: title)
        }
    }


    // MARK: Class Methods

    /**
     - parameter user: A list of valid `MXUser` objects.
     - parameter sourceView: The view the user touched to present the activity next to it. Needed for iPads!
     - returns: a share/activity dialog for the given `MXUser`.
    */
    open class func shareProfiles(_ users: [MXUser], _ sourceView: UIView) -> UIActivityViewController {
        let activityVc = UIActivityViewController(activityItems: users,
                                                  applicationActivities: [ShowQrActivity()])
        activityVc.excludedActivityTypes = [.print, .saveToCameraRoll, .addToReadingList]

        activityVc.popoverPresentationController?.sourceView = sourceView
        activityVc.popoverPresentationController?.sourceRect = sourceView.bounds

        return activityVc
    }
}
