//
//  DevicesViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 19.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixKit
import KeanuCore

/**
 A scene showing all devices (resp. `MXDeviceInfo`s), their verification status,
 their master key fingerprint some overview and allows the user to verify or
 block certain devices.
 */
open class DevicesViewController: UITableViewController, DeviceCellDelegate {
        
    open var friend: Friend?
    open var devices = [MXDeviceInfo]()

    /**
     Logout action for table list row. Logs out of an account after confirmation.
     */
    private lazy var verifyAction: UITableViewRowAction = {
        let action = UITableViewRowAction(
            style: .destructive,
            title: "Verify".localize())
        { (action, indexPath) in
            if self.devices.count > indexPath.row,
                let dvm = MXKAccountManager.shared()?.activeAccounts?.first?.mxSession?.crypto?.deviceVerificationManager,
                let friend = self.friend {

                let device = self.devices[indexPath.row]

                dvm.beginKeyVerification(withUserId: friend.matrixId,
                                         andDeviceId: device.deviceId,
                                         method: MXKeyVerificationMethodSAS,
                                         success: { transaction in
                                            print("[\(String(describing: type(of: self)))]#beginKeyVerification transaction=\(transaction)")
                }) { error in
                    print("[\(String(describing: type(of: self)))]#beginKeyVerification error=\(error)")
                }

            }
        }

        return action
    }()

    override open func viewDidLoad() {
        super.viewDidLoad()

        // For an unkown reason, this needs to be set programatically. The setting
        // In the XIB doesn't do anything.
        tableView.allowsSelection = false

        if let friend = friend {
            devices = friend.devices.sorted(by: deviceSorter(_:_:))

            friend.loadDevices() { devices in
                self.devices = devices.sorted(by: self.deviceSorter(_:_:))

                self.tableView.reloadData()
            }

            navigationItem.title = friend.name
        }

        // Register cell types.
        tableView.register(DeviceHeaderCell.nib, forCellReuseIdentifier: DeviceHeaderCell.defaultReuseId)
        tableView.register(DeviceCell.nib, forCellReuseIdentifier: DeviceCell.defaultReuseId)
    }


    // UITableViewDelegate

    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 190 : 140
    }


    // UITableViewDataSource

    override open func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : devices.count
    }

    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if let friend = friend,
                let cell = dequeueHeaderCell(indexPath) {

                return cell.apply(friend, countKeys())
            }
        }
        else {
            if let cell = dequeueDeviceCell(indexPath) {

                return cell.apply(device: devices[indexPath.row],
                                  delegate: self)
            }
        }

        return UITableViewCell()
    }

    open override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if !Config.useNewVerificationBeta || indexPath.section < 1 {
            return []
        }

        // This feature is far from finished.
        return [verifyAction]
    }


    // MARK: DeviceCellDelegate

    open func verificationChanged(_ device: MXDeviceInfo) {
        friend?.storeDevice(device)

        // Refresh header and device rows.
        tableView.reloadData()
    }


    // MARK: Helper Methods

    open func dequeueHeaderCell(_ indexPath: IndexPath) -> DeviceHeaderCell? {
        return tableView.dequeueReusableCell(withIdentifier: DeviceHeaderCell.defaultReuseId,
                                             for: indexPath) as? DeviceHeaderCell
    }

    open func dequeueDeviceCell(_ indexPath: IndexPath) -> DeviceCell? {
        return tableView.dequeueReusableCell(withIdentifier: DeviceCell.defaultReuseId,
                                             for: indexPath) as? DeviceCell
    }

    /**
     Sets the style of a badge to either be red and display a shield with an
     exclamation mark or be green and display a shield with a check mark.

     - parameter label: The badge label to change.
     - parameter ok: Set true if the device is verified.
     */
    open class func setBadge(_ label: UILabel, _ ok: Bool) {
        if ok {
            label.backgroundColor = UIColor.green
            label.text = "" // Shield with check mark
        }
        else {
            label.backgroundColor = UIColor.red
            label.text = "" // Shield with exclamation mark
        }
    }

    /**
     Sorter for `MXDeviceInfo` classes.

     - Sort order:
        - "Unkown" (new) devices
        - Verified devices
        - Unverified devices
        - Blocked devices
     - Second sort order:
        - Ascending by device ID
     */
    open func deviceSorter(_ a: MXDeviceInfo, _ b: MXDeviceInfo) -> Bool {
        if a.verified == b.verified {
            return a.deviceId < b.deviceId
        }

        if a.verified == MXDeviceUnknown {
            return true
        }

        if b.verified == MXDeviceUnknown {
            return false
        }

        if a.verified == MXDeviceVerified {
            return true
        }

        if b.verified == MXDeviceVerified {
            return false
        }

        if a.verified == MXDeviceUnverified {
            return true
        }

        // Verification states must here be:
        //   a.verified == MXDeviceBlocked
        //   b.verified == MXDeviceUnverified

        return false
    }

    /**
     Counts devices' verification status.

     - returns: A named tuple with number of `MXDeviceUnknown` and number of
        `MXDeviceVerified` keys.
     */
    open func countKeys() -> (unverifiedNew: Int, verified: Int) {
        var unverifiedNew = 0
        var verified = 0

        for device in devices {
            if device.verified == MXDeviceUnknown {
                unverifiedNew += 1
            }
            else if device.verified == MXDeviceVerified {
                verified += 1
            }
        }

        return (unverifiedNew, verified)
    }
}
