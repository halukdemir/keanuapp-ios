//
//  VerificationManager.swift
//  Keanu
//
//  Created by Benjamin Erhart on 24.06.19.
//  Copyright © 2019 Guardian Project. All rights reserved.

import UIKit
import KeanuCore
import MatrixSDK

/**
 https://docs.google.com/document/d/1SXmyjyNqClJ5bTHtwvp8tT1Db4pjlGVxfPQNdlQILqU/edit#
 https://github.com/matrix-org/matrix-doc/pull/2072
 */
open class VerificationManager {
    
    /**
     Singleton instance.
     */
    public static let shared = VerificationManager()

    lazy var navC: UINavigationController? = {
        return VerificationViewController.instantiate()
    }()

    var verificationVc: VerificationViewController? {
        return navC?.topViewController as? VerificationViewController
    }

    init() {
        if Config.useNewVerificationBeta {
            NotificationCenter.default.addObserver(
                self, selector: #selector(verification),
                name: .MXDeviceVerificationTransactionDidChange, object: nil)
        }
    }

    /**
     Callback for `MXDeviceVerificationTransactionDidChange` notification.

     Handles all

     - parameter notification: The notification object.
     */
    @objc open func verification(notification: Notification) {
        print("[MXKeyVerification][\(String(describing: type(of: self)))]#verification notification=\(notification)")

        if let transaction = notification.object as? MXSASTransaction {

            if let navC = navC,
                navC.presentingViewController == nil,
                let rootVc = UIApplication.shared.delegate?.window??.rootViewController {

                rootVc.present(navC, animated: true)
            }

            verificationVc?.render(transaction)
        }
    }
}
