//
//  DeviceHeaderCell.swift
//  Keanu
//
//  Created by Benjamin Erhart on 22.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK
import KeanuCore

open class DeviceHeaderCell: UITableViewCell {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    open class var defaultReuseId: String {
        return String(describing: self)
    }

    @IBOutlet weak var avatar: AvatarView!
    @IBOutlet weak var badgeLb: UILabel!
    @IBOutlet weak var subtitleLb: UILabel!
    @IBOutlet weak var descriptionLb: UILabel!

    open func apply(_ friend: Friend, _ keys: (unverifiedNew: Int, verified: Int)) -> DeviceHeaderCell {
        avatar.load(friend: friend)

        apply(friend.name, keys,
              "Make sure the keys match your friend's latest keys on his or her devices."
                .localize())

        return self
    }

    open func apply(_ user: MXUser, _ session: MXSession, _ keys: (unverifiedNew: Int, verified: Int)) -> DeviceHeaderCell {
        avatar.load(user: user, session: session)

        apply(user.friendlyName, keys,
            "Make sure the keys match the latest keys on your devices.".localize())

        return self
    }

    private func apply(_ name: String, _ keys: (unverifiedNew: Int, verified: Int), _ description: String) {
        DevicesViewController.setBadge(badgeLb, keys.verified > 0 && keys.unverifiedNew < 1)

        if keys.verified > 0 || keys.unverifiedNew > 0 {
            subtitleLb.text = "% Untrusted New Devices for %".localize(values:
                                     Formatters.format(int: keys.unverifiedNew), name)

            descriptionLb.isHidden = false

            descriptionLb.text = description
        }
        else {
            subtitleLb.text = "No Devices for %".localize(value: name)

            descriptionLb.isHidden = true
        }

    }
}
