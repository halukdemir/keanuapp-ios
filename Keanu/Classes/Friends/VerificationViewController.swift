//
//  VerificationManager.swift
//  Keanu
//
//  Created by Benjamin Erhart on 01.07.19.
//  Copyright © 2019 Guardian Project. All rights reserved.

import UIKit
import MatrixSDK

open class VerificationViewController: UIViewController {

    private var transaction: MXSASTransaction?

    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var subtitleLb: UILabel!
    @IBOutlet weak var messageTv: UITextView!
    @IBOutlet weak var actionBt: UIButton!


    /**
     Instantiate the `VerificationViewController` from its storyboard.

     This is a complete storyboard with `UINavigationController`, because we
     want a top bar with a title and a cancel button.

     - parameter transaction: The verification transaction to react to.
     - returns: The `UINavigationController` containing the `ShowQrViewController` as its top view controller.
     */
    class func instantiate() -> UINavigationController? {
        if let navC = UIStoryboard.overriddenOrLocal(name: String(describing: self))
            .instantiateInitialViewController() as? UINavigationController {

            if UIDevice.current.userInterfaceIdiom == .pad {
                navC.modalPresentationStyle = .formSheet
            }

            return navC
        }

        return nil
    }

    open override func viewDidLoad() {
        super.viewDidLoad()

        // Finally render a transaction which was given before the view was loaded.
        if let transaction = transaction {
            render(transaction)
        }
    }

    func render(_ transaction: MXSASTransaction) {
        print("[\(String(describing: type(of: self)))] transaction=\(transaction)")

        self.transaction = transaction

        guard isViewLoaded else {
            return
        }

        var title: String?
        var subtitle: String?
        var message: String?
        var action: String?

        switch transaction.state {
        case MXSASTransactionStateIncomingShowAccept:
            title = "Incoming Verification Request".localize()
            message = "% wants to verify their device % with you.\n\nDo you want to start the verification process?"
                .localize(values: transaction.otherUserId, transaction.otherDeviceId)
            action = "Accept".localize()

        case MXSASTransactionStateOutgoingWaitForPartnerToAccept:
            title = "Waiting".localize()
            subtitle = "Waiting on friend to accept…".localize()

        case MXSASTransactionStateWaitForPartnerKey:
            title = "Waiting".localize()
            subtitle = "Waiting on challenge negotiation…".localize()

        case MXSASTransactionStateWaitForPartnerToConfirm:
            title = "Waiting".localize()
            subtitle = "Waiting on friend to confirm…".localize()

        case MXSASTransactionStateShowSAS:
            title = "Verification Request".localize()
            subtitle = "Verify device % of user % by comparing these emoji:".localize(values:
                transaction.otherDeviceId, transaction.otherUserId)
            message = transaction.sasEmoji?.map { e -> String in "\(e.emoji) \(e.name)" }.joined(separator: "\n\n")
            action = "Verify".localize()

        case MXSASTransactionStateVerified:
            title = "Verified!".localize()
            subtitle = "You have verified user % successfully!".localize(values:
                transaction.otherUserId)
            message = "Secure messages with this user are end-to-end encrypted and cannot be read by third parties."
                .localize()
            action = "OK".localize()

        case MXSASTransactionStateCancelled:
            title = "Cancelled".localize()
            subtitle = "The friend cancelled the request".localize()
            message = "Reason: %".localize(value:
                transaction.reasonCancelCode?.humanReadable ?? "No reason available.".localize())
            action = "OK".localize()

        case MXSASTransactionStateError:
            title = "Error".localize()
            message = transaction.error?.localizedDescription ?? "No description available".localize()
            action = "OK".localize()

        default:
            break
        }

        titleLb.text = title
        titleLb.isHidden = title == nil

        subtitleLb.text = subtitle
        subtitleLb.isHidden = subtitle == nil

        messageTv.text = message
        messageTv.isHidden = message == nil

        actionBt.setTitle(action, for: .normal)
        actionBt.isHidden = action == nil
    }


    // MARK: Actions

    @IBAction func dismiss() {
        dismiss(animated: true)

        transaction?.cancel(with: .user())
    }

    @IBAction func action() {
        switch transaction?.state {
        case MXSASTransactionStateIncomingShowAccept:
            (transaction as? MXIncomingSASTransaction)?.accept()

        case MXSASTransactionStateVerified,
             MXSASTransactionStateCancelled,
             MXSASTransactionStateError:
            dismiss(animated: true)

        case MXSASTransactionStateShowSAS:
            transaction?.confirmSASMatch()

        default:
            break
        }
    }
}
