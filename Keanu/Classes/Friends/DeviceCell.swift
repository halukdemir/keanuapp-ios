//
//  KeyCell.swift
//  Keanu
//
//  Created by Benjamin Erhart on 19.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK

public protocol DeviceCellDelegate {

    /**
     Called, when the user changed the verification status of the currently bound
     `MXDeviceInfo` object.

     - parameter device: The `MXDeviceInfo` object, that has changed its verification status.
    */
    func verificationChanged(_ device: MXDeviceInfo)
}

open class DeviceCell: UITableViewCell {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    open class var defaultReuseId: String {
        return String(describing: self)
    }

    @IBOutlet weak var badgeLb: UILabel!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var idLb: UILabel!
    @IBOutlet weak var verifiedLb: UILabel!
    @IBOutlet weak var fingerprintLb: UILabel!
    @IBOutlet weak var verifiedSw: UISwitch!


    var device: MXDeviceInfo?
    var delegate: DeviceCellDelegate?

    /**
     Applies a device info to this cell.

     - parameter device: The `MXDeviceInfo` for a device.
     - parameter delegate: The delegate to call back, when the user changed the `verifiedSw`.
     - parameter isThisDevice: true, if that is the device info of the device we're running on.
     - returns: self for convenience.
     */
    open func apply(device: MXDeviceInfo?, delegate: DeviceCellDelegate, isThisDevice: Bool = false) -> DeviceCell {
        self.device = device
        self.delegate = delegate

        DevicesViewController.setBadge(badgeLb, device?.verified == MXDeviceVerified)

        if isThisDevice {
            let text = NSMutableAttributedString()

            if let name = device?.displayName {
                text.append(NSAttributedString(string: "\(name) – "))
            }

            text.append(NSAttributedString(
                string: "This Device".localize().localizedUppercase,
                attributes: [.font: UIFont.boldSystemFont(ofSize: nameLb.font.pointSize)]))

            nameLb.attributedText = text
        }
        else {
            nameLb.text = device?.displayName
        }
        idLb.text = device?.deviceId

        switch device?.verified {
        case MXDeviceUnknown:
            verifiedLb.text = "new".localize()
        case MXDeviceUnverified:
            verifiedLb.text = "unverified".localize()
        case MXDeviceVerified:
            verifiedLb.text = "verified".localize()
        default:
            verifiedLb.text = "blocked".localize()
        }

        setFingerprint(fingerprintLb, text: device?.fingerprint,
                       alignment: .natural, bold: true)

        verifiedSw.isOn = device?.verified == MXDeviceVerified
        verifiedSw.isEnabled = !isThisDevice

        return self
    }


    // MARK: Actions

    @IBAction func changeVerification() {
        if let device = device {
            let old = device.verified

            device.verified = verifiedSw.isOn ? MXDeviceVerified : MXDeviceBlocked

            DevicesViewController.setBadge(badgeLb, device.verified == MXDeviceVerified)

            if old != device.verified {
                delegate?.verificationChanged(device)
            }
        }
    }


    // MARK: Private Methods
    
    /**
     Set text content of label which should contain a fingerprint.

     Copious amounts of letter and line spacing are used.

     - parameter label: The `UILabel` to use.
     - parameter text: Falls back to an error message, if nil.
     - parameter alignment: Alignment used.
     - parameter bold: If bold system font should be used.
     */
    private func setFingerprint(_ label: UILabel, text: String?, alignment: NSTextAlignment, bold: Bool) {
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        style.alignment = alignment

        var attributes: [NSAttributedString.Key : Any] = [.kern: 1, .paragraphStyle: style]

        if bold {
            attributes[.font] = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        }

        let string = text?.split(by: 4).joined(separator: " ")
            // Error message instead of key fingerprint which could not be read.
            ?? "CODE ERROR".localize()

        label.attributedText = NSAttributedString(string: string,
                                                  attributes: attributes)
    }
}
