//
//  AddFriendViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 26.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MessageUI
import AVFoundation
import MatrixKit
import KeanuCore
import BarcodeScanner
import Regex
import PhoneNumberKit
import SearchTextField

open class AddFriendViewController: BaseViewController, UITextFieldDelegate,
    MFMessageComposeViewControllerDelegate, BarcodeScannerCodeDelegate, BarcodeScannerDismissalDelegate {

    @IBOutlet weak open var searchTf: SearchTextField!
    @IBOutlet weak var ownIdLb: UILabel!
    @IBOutlet weak var whatsAppBt: UIButton!
    @IBOutlet weak var iMessageBt: UIButton!
    @IBOutlet weak var qrCodeBt: UIButton!

    private static let shareLink = Config.inviteLinkFormat
    private static let whatsAppLink = "whatsapp://send?text=%@"
    
    private static let matrixIdRegex = kMXToolsRegexStringForMatrixUserIdentifier.r
    private static let emailRegex = kMXToolsRegexStringForEmailAddress.r
    
    private var account: MXKAccount?
    private var myUsers = [MXMyUser]()
    
    private var shareText = ""
    private var whatsAppUrl: URL?
    
    /**
     Current search operation, if any
     */
    private var searchOperation: MXHTTPOperation?
    
    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()

    private lazy var phoneNumberKit: PhoneNumberKit = {
        return PhoneNumberKit()
    }()

    /**
     Whether or not a chat room will be opened to newly added friends. Defaults to "true".
     */
    public var startChatWithAddedFriend: Bool = true

    /**
     Optional callback for when a friend has been added.
     */
    public var friendAddedCallback: ((String) -> ())? = nil
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        account = MXKAccountManager.shared()?.activeAccounts.first
        for account in MXKAccountManager.shared()?.activeAccounts ?? [] {
            if let user = account.mxSession?.myUser {
                myUsers.append(user)
            }
        }

        shareText = String(format: AddFriendViewController.shareLink, myUsers.first?.userId ?? "")
        whatsAppUrl = URL(string: String(
            format: AddFriendViewController.whatsAppLink,
            shareText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
        
        hideKeyboardOnOutsideTap()
        
        // Remove the title from the back button. It's rather long and moves this
        // scene's title to the right which the designers don't like.
        navigationController?.navigationBar.topItem?.title = ""
        
        ownIdLb.text = "Enter your friend's Matrix ID. Yours is %".localize(value: myUsers.first?.userId ?? "")
        
        // Only show WhatsApp button, if WhatsApp is installed.
        whatsAppBt.isHidden = whatsAppUrl == nil || !UIApplication.shared.canOpenURL(whatsAppUrl!)
        
        // Only show iMessage button, if we can send SMS/iMessages. (Happens on simulators and on
        // iPads/iPods without iMessage configured.
        iMessageBt.isHidden = !MFMessageComposeViewController.canSendText()
        
        // Only show scan button if we can actually scan QR codes.
        let device = AVCaptureDevice.default(for: .video)
        qrCodeBt.isHidden = device == nil || (try? AVCaptureDeviceInput(device: device!)) == nil
        
        // Setup directory search
        searchTf.theme.bgColor = UIColor.white
        searchTf.userStoppedTypingHandler = {
            if let term = self.searchTf.text {
                if term.count > 1, let account = self.account {
                    
                    // Cancel old op, if any
                    if let operation = self.searchOperation {
                        operation.cancel()
                    }
                    
                    // Show the loading indicator
                    self.searchTf.showLoadingIndicator()
                    
                    self.searchOperation = account.mxRestClient.searchUsers(
                        term, limit: 10,
                        success: { response in
                            self.searchOperation = nil
                            DispatchQueue.main.async {
                                if let users = response?.results, users.count > 0 {
                                    
                                    // Used to dedup results
                                    var userIds:[String] = []
                                    let uniqueUsers = users.filter({ (user) -> Bool in
                                        if !userIds.contains(user.userId) {
                                            userIds.append(user.userId)
                                            return true
                                        }
                                        return false
                                    })
                                    
                                    let userNames = uniqueUsers.map { SearchTextFieldItem(title: $0.friendlyName, subtitle: $0.userId) }
                                    self.searchTf.filterItems(userNames)
                                } else {
                                    self.searchTf.filterItems([])
                                    self.searchTf.hideResultsList()
                                }
                                self.searchTf.stopLoadingIndicator()
                            }
                    },
                        failure: { error in
                            self.searchOperation = nil
                            DispatchQueue.main.async {
                                self.searchTf.filterItems([])
                                self.searchTf.hideResultsList()
                                self.searchTf.stopLoadingIndicator()
                            }
                    })
                }
            }
        }
        searchTf.itemSelectionHandler = { items, position in
            let item = items[position]
            self.searchTf.text = item.subtitle
        }
    }
    
    
    // MARK: UITextFieldDelegate
    
    /**
     Callback for return button press in `userIdTf`.
    */
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        addFriend()
        
        return false
    }
    
    // MARK: Actions

    /**
     Add friend with Matrix ID from searchTf.
     */
    @IBAction open func addFriend() {
        if let term = searchTf.text,
            !term.isEmpty {
            
            workingOverlay.isHidden = false

            if let matrixId = extractMatrixId(term) {
                addFriendAndDismiss(matrixId)
            }
            else if let email = extractEmail(term) {
                addFriendAndDismiss(MX3PID(medium: .email, address: email))
            }
            else if let phone = try? phoneNumberKit.parse(term) {
                addFriendAndDismiss(MX3PID(medium: .msisdn, address: phoneNumberKit.format(phone, toType: .e164)))
            }
            else {
                account?.mxRestClient.searchUsers(
                    term, limit: 1,
                    success: { response in
                        self.workingOverlay.isHidden = true

                        if let user = response?.results?.first {
                            FriendsManager.shared.add(user.userId)
                            self.dismiss()
                            
                            // If the startChatWithAddedFriend flag is set, open a chat room
                            if self.startChatWithAddedFriend, let addedFriend = FriendsManager.shared.get(user.userId) {
                                DispatchQueue.main.async {
                                    UIApplication.shared.openRoom(friend: addedFriend)
                                }
                            }
                        }
                        else {
                            self.notFoundError()
                        }
                    },
                    failure: { error in
                        self.workingOverlay.isHidden = true

                        self.notFoundError()
                    })
            }
        }
    }

    /**
     Send invite link directly to WhatsApp.
     */
    @IBAction func sendWhatsAppInvite() {
        if let whatsAppUrl = whatsAppUrl {
            UIApplication.shared.openURL(whatsAppUrl)
        }
    }
    
    /**
     Send invite link via SMS or iMessage. Will show system's message compose scene.
     */
    @IBAction func sendSmsInvite() {
        let composeVc = MFMessageComposeViewController()
        composeVc.body = shareText
        composeVc.messageComposeDelegate = self
        present(composeVc, animated: true)
    }

    /**
     Show QR code scan camera view.
     */
    @IBAction func scanQrCode() {
        let vc = BarcodeScannerViewController()
        vc.metadata = [.qr]
        vc.codeDelegate = self
        vc.dismissalDelegate = self
        vc.isOneTimeSearch = true
        vc.cameraViewController.barCodeFocusViewType = .twoDimensions
        vc.cameraViewController.showsCameraButton = true
        vc.title = "Scan QR Code".localize()
        vc.headerViewController.titleLabel.text = vc.title
        vc.messageViewController.messages.scanningText
            = "Place the QR code within the frame.".localize()
        vc.messageViewController.messages.unathorizedText
            = "To scan QR codes, please go to Settings and allow camera access.".localize()
        vc.messageViewController.messages.processingText
            = "Checking QR code…".localize()
        vc.messageViewController.messages.notFoundText
            = "That QR code didn't contain a valid Matrix ID!".localize()
        
        present(vc, animated: true)
    }

    /**
     Show system's modal share dialog. This is used from the three-dots button ("...") AND
     the AirDrop button. There is no specific way to leverage AirDrop. It's all in the share
     dialog.
     
     Using two different buttons here for the same functionality, is just to get the user thinking
     about AirDrop.
     */
    @IBAction func showShareDialog(_ sender: UIButton) {
        present(ProfileViewController.shareProfiles(myUsers, sender), animated: true)
    }
    
    
    // MARK: MFMessageComposeViewControllerDelegate
    
    public func messageComposeViewController(_ controller: MFMessageComposeViewController,
                                      didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true)
    }

    
    // MARK: BarcodeScanner Delegates
    
    public func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        // Give the "processing..." animation some time.
        // The error will not be shown, otherwise, and the scene pop looks nicer, too.
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let matrixId = self.extractMatrixId(code) {
                controller.dismiss(animated: true)

                self.searchTf.text = matrixId
            }
            else {
                controller.resetWithError()
            }
        }
    }

    public func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true)
    }
    
    
    // MARK: Private Methods
    
    /**
     Extracts the first found thing which looks like a Matrix ID from the given haystack.
     
     - parameter haystack: The string to search the Matrix ID in.
     - returns: the first found Matrix ID or `nil` if none found.
    */
    private func extractMatrixId(_ haystack: String) -> String? {
        return AddFriendViewController.matrixIdRegex?.findFirst(in: haystack)?.matched
    }

    /**
     Extracts the first found thing which looks like an E-Mail address from the given haystack.

     - parameter haystack: The string to search the E-Mail address in.
     - returns: the first found E-Mail address or `nil` if none found.
     */
    private func extractEmail(_ haystack: String) -> String? {
        return AddFriendViewController.emailRegex?.findFirst(in: haystack)?.matched
    }

    /**
     Loads friendly name and avatar URL for a given Matrix ID, adds that user
     as a friend and dismisses the scene, if successful.

     Otherwise, does an error animation on `searchTf`.

     - parameter matrixId: The profile's Matrix ID
    */
    private func addFriendAndDismiss(_ matrixId: String) {
        account?.mxRestClient.profile(forUser: matrixId) { response in
            self.workingOverlay.isHidden = true

            if response.isSuccess,
                let (name, avatarUrl) = response.value {

                FriendsManager.shared.add(matrixId, name, avatarUrl)
                
                // If callback is set, notify that one
                self.friendAddedCallback?(matrixId)
                
                self.dismiss()
                
                // If the startChatWithAddedFriend flag is set, open a chat room
                if self.startChatWithAddedFriend, let addedFriend = FriendsManager.shared.get(matrixId) {
                    DispatchQueue.main.async {
                        UIApplication.shared.openRoom(friend: addedFriend)
                    }
                }
            }
            else {
                self.notFoundError()
            }
        }
    }

    /**
     Discover Matrix ID of user using a third-party ID.

     If found, loads friendly name and avatar URL for a given Matrix ID, adds
     that user as a friend and dismisses the scene, if successful.

     Otherwise, does an error animation on `searchTf`.

     - parameter tpid: The third-party ID, e.g. email or MSISDN
     */
    private func addFriendAndDismiss(_ tpid: MX3PID) {
        account?.mxSession?.identityService?.lookup3PIDs([tpid]) { response in
            if response.isSuccess, let (_, matrixId) = response.value?.first {
                self.addFriendAndDismiss(matrixId)
            }
            else {
                self.workingOverlay.isHidden = true
                self.notFoundError()
            }
        }
    }
    
    /**
     Animates the background color of the `searchTf` clear -> red -> clear
     for 2 seconds.
    */
    private func notFoundError() {
        UIView.animate(withDuration: 1,
                       animations: {
                        self.searchTf.backgroundColor = UIColor.red.withAlphaComponent(0.8)
        },
                       completion: { finished in
                        UIView.animate(withDuration: 1, animations: {
                            self.searchTf.backgroundColor = .clear
                        })
        })
    }
}
