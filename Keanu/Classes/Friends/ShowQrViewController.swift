//
//  ShowQrViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 06.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import QRCode

/**
 Scene to show a QR code image and its contents underneath.
 */
class ShowQrViewController: UIViewController {

    /**
     Instantiate the `ShowQrViewController` from its storyboard.

     This is a complete storyboard with `UINavigationController`, because
     everything else is not completely compatible from our supported iOS 9 - 12.

     - parameter qrCode: The QR code content.
     - returns: The `UINavigationController` containing the `ShowQrViewController` as its top view controller.
     */
    class func instantiate(_ qrCode: String) -> UINavigationController? {
        if let navC = UIStoryboard.overriddenOrLocal(name: String(describing: self))
            .instantiateInitialViewController() as? UINavigationController {

            if let showQrVc = navC.topViewController as? ShowQrViewController {
                showQrVc.qrCode = qrCode
            }

            if UIDevice.current.userInterfaceIdiom == .pad {
                navC.modalPresentationStyle = .formSheet
            }

            return navC
        }

        return nil
    }

    @IBOutlet weak var qrCodeIv: UIImageView!
    @IBOutlet weak var qrCodeTv: UITextView!
    
    private var qrCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        qrCodeIv.image = QRCode(qrCode)?.image
        
        qrCodeTv.text = qrCode
    }


    // MARK: Actions

    @IBAction func dismiss() {
        navigationController?.dismiss(animated: true)
    }
}
