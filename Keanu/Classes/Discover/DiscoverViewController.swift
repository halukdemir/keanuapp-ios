//
//  DiscoverViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 05.12.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixKit

public protocol DiscoverViewControllerDelegate {
    /**
     Called by delegate to setup the rows to display.
     - parameter identifiers: An array of identifiers that make up the default rows to show.
     - returns: An array of rows to show. You can add your own, remove the default ones etc.
     */
    func initializeRows(identifiers:[String]) -> [String]

    /**
     Called by delegate to create a cell for the given identifier.
     - parameter identifier: The row identifiers to create a cell for.
     - returns: True if cell was setup, false for default behavior.
     */
    func setupRow(identifier: String, cell:ActionButtonCell) -> Bool
    
    /**
     Called by delegate to handle taps on a row.
     - parameter identifier: The row identifier of the selected row.
     - returns: true if handled, false if default handling should occur.
     */
    func didSelectRow(identifier:String) -> Bool
}

open class DiscoverViewController: UITableViewController {

    public let identifierPhotoStream = "cellPhotoStream"
    public let identifierCreateGroup = "cellCreateGroup"
    
    open var delegate: DiscoverViewControllerDelegate?
    open var rows:[String] = []
    
    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    override open func viewDidLoad() {
        super.viewDidLoad()

        // Don't show "Discover" on button to get back here...
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        
        // Register cell types.
        tableView.register(ActionButtonCell.nib, forCellReuseIdentifier: ActionButtonCell.defaultReuseId)
        
        rows = [
            identifierPhotoStream,
            identifierCreateGroup
        ]
        rows = delegate?.initializeRows(identifiers: rows) ?? rows
    }

    // MARK: UITableViewDataSource

    override open func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }

    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ActionButtonCell.defaultReuseId, for: indexPath)

        if let cell = cell as? ActionButtonCell {
            let identifier = rows[indexPath.row]
            let handled = delegate?.setupRow(identifier: identifier, cell: cell) ?? false
            if !handled {
                switch identifier {
                    case identifierPhotoStream: return cell.createPhotoStream()
                    case identifierCreateGroup: return cell.createRoom()
                    default: break
                }
            }
        }
        return cell
    }

    // MARK: UITableViewDelegate

    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }

    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let identifier = rows[indexPath.row]
        let handled = delegate?.didSelectRow(identifier: identifier) ?? false
        if !handled {
            switch identifier {
            case identifierPhotoStream:
                if let vc = PhotoStreamViewController.instantiate(room: nil) {
                    present(vc, animated: true, completion: nil)
                }
                break
            case identifierCreateGroup:
                // Create (empty) Room in the first (default) account.
                if let session = MXKAccountManager.shared()?.activeAccounts?.first?.mxSession {
                    UIApplication.shared.createEmptyRoom(session: session)
                }
                break
            default: break
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
