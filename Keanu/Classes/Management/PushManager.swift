//
//  PushManager.swift
//  Keanu
//
//  Created by N-Pex on 19.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import MatrixKit
import KeanuCore
import UserNotifications

/**
 Central manager to handle all push related:
 
 - Register for APNS pushes.
 -
 */
open class PushManager: NSObject, UNUserNotificationCenterDelegate {
    
    /**
     A notification that is sent when the user has either accepted or rejected permissions for push. The "userInfo" array will contain a boolean value for the key "granted", either true or false.
     */
    public static let didGetPushManagerAuthorizationResult = NSNotification.Name("didGetPushManagerAuthorizationResult")
    
    static let categoryRoomInviteIdentifier = "roomInvite"
    static let actionAcceptInvite = "acceptInvite"
    static let actionDeclineInvite = "declineInvite"
    
    static let missedEventsNotificationIdentifier = "missed_events_notification"
    
    public enum RegistrationState {
        case none
        case registeringSettings
        case settingsRegistered
        case registering
        case registered
    }
    
    /**
     Singleton instance.
     */
    public static let shared: PushManager = {
        return PushManager()
    }()
    
    private var registrationState: RegistrationState = .none
    
    var currentMissedEventsNotificationBody: String?
    var pendingNotificationEventMap:[String:[MXEvent]] = [:]
    
    open func setupPush() {
        if registrationState == .none {
            // MatrixSDK configuration is done via UserDefaults.
            UserDefaults.standard.set(Config.pushAppIdDev, forKey: "pusherAppIdDev")
            UserDefaults.standard.set(Config.pushAppIdRelease, forKey: "pusherAppIdProd")
            hasPushPermissions { (hasPermissions) in
                if hasPermissions {
                    DispatchQueue.main.async {
                        self.registerUserNotificationSettings()
                    }
                }
            }
        } else if registrationState == .settingsRegistered {
            registerForPush()
        }
    }
    
    /**
     Check if we have already asked user for push permissions. Use the provided callback to provider the result. True if we have asked, false if not.
     */
    public func hasAskedForPushPermissions(_ callback:@escaping (Bool)->()) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                DispatchQueue.main.async {
                    callback(settings.authorizationStatus != UNAuthorizationStatus.notDetermined)
                }
            }
        } else {
            // Fallback on earlier versions
            callback(false)
        }
    }
    
    /**
     Check if user has allowed notifications. Use the provided callback to provider the result. True if we have permissions, false if not.
     */
    public func hasPushPermissions(_ callback:@escaping (Bool)->()) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                DispatchQueue.main.async {
                    callback(settings.authorizationStatus == UNAuthorizationStatus.authorized)
                }
            }
        } else {
            // Fallback on earlier versions
            if let settings = UIApplication.shared.currentUserNotificationSettings {
                callback(!settings.types.isEmpty) // TODO - check all the various badge, audio, message...?
            } else {
                callback(false)
            }
        }
    }
    
    public func registerForPush() {
        registrationState = .settingsRegistered
        
        // Immediately switch to registering
        registrationState = .registering
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    public func registerUserNotificationSettings() {
        if registrationState == .none {
            registrationState = .registeringSettings
            
            if #available(iOS 10.0, *) {
                // Register the "room invite" category, so that the user can accept or decline the invite straight from the notification.
                // Action for accepting an invite.
                let acceptAction = UNNotificationAction(identifier: PushManager.actionAcceptInvite,
                                                        title: "Accept".localize(),
                                                        options: UNNotificationActionOptions(rawValue: 0))
                
                // Action for declining an invite.
                let declineAction = UNNotificationAction(identifier: PushManager.actionDeclineInvite,
                                                         title: "Decline".localize(),
                                                         options: UNNotificationActionOptions(rawValue: 0))
                let roomInviteCategory =
                    UNNotificationCategory(identifier: PushManager.categoryRoomInviteIdentifier,
                                           actions: [acceptAction, declineAction],
                                           intentIdentifiers: [],
                                           options: .customDismissAction)
                UNUserNotificationCenter.current().setNotificationCategories([roomInviteCategory])
                UNUserNotificationCenter.current().delegate = self
                
                UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                    if granted {
                        DispatchQueue.main.async {
                            self.registerForPush()
                            NotificationCenter.default.post(
                                name: PushManager.didGetPushManagerAuthorizationResult,
                                object: self,
                                userInfo: ["granted" : granted])
                        }
                    } else {
                        self.registrationState = .none
                    }
                }
            } else {
                // Action for accepting an invite.
                let acceptAction = UIMutableUserNotificationAction()
                acceptAction.title = "Accept".localize()
                acceptAction.identifier = PushManager.actionAcceptInvite
                acceptAction.activationMode = .foreground
                
                // Action for declining an invite.
                let declineAction = UIMutableUserNotificationAction()
                declineAction.title = "Decline".localize()
                declineAction.identifier = PushManager.actionDeclineInvite
                declineAction.activationMode = .foreground
                let roomInviteCategory = UIMutableUserNotificationCategory()
                roomInviteCategory.setActions([acceptAction, declineAction], for: .default)
                
                // Registration on iOS 8 and later
                let settings = UIUserNotificationSettings(types: [.alert,.badge,.sound], categories: [roomInviteCategory])
                UIApplication.shared.registerUserNotificationSettings(settings)
            }
        } else if registrationState == .settingsRegistered {
            self.registerForPush()
        }
    }
    
    open func clearPushToken() {
        registrationState = .none
        
        // Clear existing token
        MXKAccountManager.shared()?.setPushDeviceToken(nil, withPushOptions: nil)
    }
    
    open func didRegister(token: Data) {
        registrationState = .registered
        for account in MXKAccountManager.shared()?.activeAccounts ?? [] {
            account.pushGatewayURL = "https://\(Config.pushServer)/_matrix/push/v1/notify"
        }
        MXKAccountManager.shared()?.apnsDeviceToken = token
    }
    
    open func didFailToRegister(error: Error) {
        // Need to try later
        registrationState = .settingsRegistered
    }
    
    func updateNotificationAndBadge(_ alertUser: Bool) {
        var missedNotifications:UInt = 0
        var missedDiscussions:UInt = 0
        for account in MXKAccountManager.shared()?.activeAccounts ?? [] {
            if let session = account.mxSession {
                missedNotifications += session.missedNotificationsCount()
                missedDiscussions += session.missedDiscussionsCount()
            }
        }
        updateBadge(missedNotifications)
        updateNotification(alertUser, missedNotifications, missedDiscussions)
    }
    
    func updateBadge(_ missedNotifications: UInt) {
        DispatchQueue.main.async {
            UIApplication.shared.applicationIconBadgeNumber = Int(missedNotifications)
        }
    }
    
    func updateNotification(_ alertUser: Bool, _ missedNotifications: UInt, _ missedDiscussions: UInt) {
        if #available(iOS 10.0, *) {
            let nc = UNUserNotificationCenter.current()
            
            if missedNotifications > 0 {
                nc.getNotificationSettings { (settings) in
                    // Do not schedule notifications if not authorized.
                    guard settings.authorizationStatus == .authorized else {
                        self.currentMissedEventsNotificationBody = nil
                        nc.removeAllDeliveredNotifications()
                        return
                    }
                    
                    // Build the notification
                    //
                    let localNotification = UNMutableNotificationContent()
                    localNotification.title = "New events".localize()
                    localNotification.threadIdentifier = PushManager.missedEventsNotificationIdentifier
                    if missedDiscussions <= 1 {
                        if missedNotifications == 1 {
                            localNotification.body = "You have 1 unseen event".localize(value: "\(missedNotifications)")
                        } else {
                            localNotification.body = "You have % unseen events".localize(value: "\(missedNotifications)")
                        }
                    } else {
                        localNotification.body = "You have % unseen events in % groups".localize(values: "\(missedNotifications)", "\(missedDiscussions)")
                    }
                    
                    if alertUser {
                        localNotification.sound = UNNotificationSound.default
                    }
                    
                    DispatchQueue.main.async {
                        if let existing = self.currentMissedEventsNotificationBody, existing == localNotification.body {
                            // If we have an existing notification with same text, avoid showing this one
                            return
                        }
                        self.currentMissedEventsNotificationBody = localNotification.body
                        
                        let request = UNNotificationRequest(identifier: PushManager.missedEventsNotificationIdentifier, content: localNotification, trigger: nil) // Schedule the notification.
                        nc.add(request, withCompletionHandler: { (error) in
                            if let error = error as NSError? {
                                #if DEBUG
                                NSLog("Error scheduling notification! %@", error)
                                #endif
                            }
                        })
                    }
                    self.clearRemoteNotifications()
                }
            } else {
                // Remove existing, if any
                self.currentMissedEventsNotificationBody = nil
                nc.removeAllDeliveredNotifications()
            }
        }
    }
    
    /**
     We don't want remote notifications to stay in the Notification Center, so we remove them (after a slight delay, so as not to remove then while they are still being animated/played...
     */
    func clearRemoteNotifications() {
        if #available(iOS 10.0, *) {
            let nc = UNUserNotificationCenter.current()
            nc.getDeliveredNotifications { (notifications) in
                let remoteNotifications = notifications.filter {
                    $0.request.identifier != PushManager.missedEventsNotificationIdentifier
                }
                guard remoteNotifications.count > 0 else {return}
                
                var toRemove:[String] = []
                
                for remote in remoteNotifications {
                    if remote.date.timeIntervalSinceNow < -10 {
                        toRemove.append(remote.request.identifier)
                    }
                }
                
                if toRemove.count > 0 {
                    nc.removeDeliveredNotifications(withIdentifiers: toRemove)
                }
                if toRemove.count != remoteNotifications.count {
                    // At least one of them was not old enough to be removed. Schedule ourselves in a few seconds again.
                    DispatchQueue.main.asyncAfter(deadline: .now() + 10, execute: {
                        self.clearRemoteNotifications()
                    })
                }
            }
        }
    }
    
    open func handleRemoteNotification(userInfo:[AnyHashable : Any], completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let roomId = userInfo["room_id"] as? String {
            var result = UIBackgroundFetchResult.newData
            
            let dispatchGroup = DispatchGroup()
            
            // Launch a background sync for all existing matrix sessions
            for account in MXKAccountManager.shared()?.activeAccounts ?? [] {
                // Check the current session state
                if account.mxSession.state == MXSessionStatePaused {
                    dispatchGroup.enter()
                    account.backgroundSync(20000, success: {
                        dispatchGroup.leave()
                    }) { (error) in
                        result = UIBackgroundFetchResult.failed
                        dispatchGroup.leave()
                    }
                }
            }
            dispatchGroup.notify(queue: DispatchQueue.global()) {
                DispatchQueue.main.async {
                    // If we are in the background, call update with "false" to update badge and notification, but without playing the sound (since we already played a sound for this remote notification!)
                    if UIApplication.shared.applicationState != .active {
                        self.updateNotificationAndBadge(false)
                    } else if roomId != RoomManager.shared.currentlyViewedRoomId {
                        self.updateNotificationAndBadge(true)
                    }
                    completionHandler(result)
                }
            }
        } else {
            self.clearRemoteNotifications()
            completionHandler(UIBackgroundFetchResult.noData)
        }
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(
        _ center: UNUserNotificationCenter, willPresent notification: UNNotification,
        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        // Is it remote notification? We get these here if we are in the foreground, but in that case we don't want to show them. We only want them when in the background. For foregroun we show a local notification instead.
        if notification.request.identifier != PushManager.missedEventsNotificationIdentifier {
            // Suppress
            completionHandler([])
            return
        }
        
        if notification.request.content.sound == nil {
            // Don't play sound
            completionHandler([.alert])
        } else {
            completionHandler([.alert, .sound])
        }
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(
        _ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if let roomId = response.notification.request.content.userInfo["room_id"] as? String {
            
            switch response.actionIdentifier {
            case PushManager.actionAcceptInvite:
                UIApplication.shared.joinRoom(roomId) { success, room in
                    if success,
                        let chatListViewController = UIApplication.shared.popToChatListViewController() {
                        chatListViewController.openRoom(room)
                    }
                }
                
                break
            case PushManager.actionDeclineInvite:
                UIApplication.shared.declineRoomInvite(roomId: roomId, callback: nil)
                
                break
            case UNNotificationDefaultActionIdentifier:
                UIApplication.shared.openRoom(roomId: roomId)
                
                completionHandler()
            default:
                break;
            }
        }
    }
    
    func showLocalNotificationFor(event:MXEvent, session:MXSession) {
        // Do nothing, let remote notifications trigger "updateNotificationAndBadge" instead, because at that time the "missedNotifications" count will be correct.
    }
}
