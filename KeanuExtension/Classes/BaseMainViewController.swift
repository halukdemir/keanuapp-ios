//
//  BaseMainViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 19.02.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import UIKit
import KeanuCore
import MatrixKit
import Localize

open class BaseMainViewController: UINavigationController {

    override open func viewDidLoad() {
        super.viewDidLoad()

        KeanuCore.setUpMatrix()
        KeanuCore.loadFonts()

        // Initialize *before* MatrixSDK, so it registers early and
        // can catch all the necessary notifications.
        _ = FriendsManager.shared

        MXKAccountManager.shared().prepareSessionForActiveAccounts()

        self.pushViewController(ShareViewController(), animated: true)
    }
}
