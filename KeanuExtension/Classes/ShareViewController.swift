//
//  ShareViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 19.02.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK
import KeanuCore
import Social
import MobileCoreServices
import MBProgressHUD

/**
 Scene showing the current list of rooms.
 
 When a room is selected, posts all provided items to that room.
 
 When done, leaves the scene again.
 */
open class ShareViewController: SelectRoomViewController {
    
    private var progress = Progress()
    
    private lazy var hud: MBProgressHUD = {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.minShowTime = 1
        hud.label.text = "Posting…".localize()
        hud.mode = .determinate
        hud.progressObject = progress
        
        return hud
    }()
    
    public init() {
        super.init(callback: {
            (viewController, room) -> Void in
            if let strongSelf = viewController as? ShareViewController {
                strongSelf.onRoomSelected(viewController, room)
            }
        })
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    open func onRoomSelected(_ viewController: SelectRoomViewController, _ room: MXRoom?) {
        if let room = room, let items = extensionContext?.inputItems as? [NSExtensionItem] {
            var event: MXEvent?
            
            hud.show(animated: true)
            
            for item in items {
                guard let attachments = item.attachments else {
                    continue
                }
                
                for provider in attachments {
                    if process(provider, UtiHelper.typeText, rawCallback: { data in
                        if let text = data as? String {
                            
                            return room.sendTextMessage(
                                text, localEcho: &event, completion: self.onCompletion)
                        }
                        
                        print("[\(String(describing: type(of: self)))] error=data is no String!, data=\(data)")
                        
                        self.onCompletion(error: "Couldn't acquire item!".localize())
                        
                        return nil
                    }) {
                        // Everything already done.
                    }
                    else if process(provider, UtiHelper.typeImage, { url in
                        
                        // Do *not* use UIImage(contentsOfFile:), as this will break,
                        // due to the file belonging to a file provider.
                        if let raw = try? Data(contentsOf: url),
                            // Reduce size to 1080 x 1080, if bigger.
                            let image = UIImage(data: raw)?.reduce(),
                            let data = image.jpegData() {
                            
                            return room.sendImage(data: data, size: image.size,
                                                  mimeType: UtiHelper.jpegMimeType,
                                                  thumbnail: nil, localEcho: &event,
                                                  completion: self.onCompletion)
                        }
                        
                        print("[\(String(describing: type(of: self)))] error=Couldn't create UIImage from data at URL!, url=\(url)")
                        
                        self.onCompletion(error: "Couldn't acquire item!".localize())
                        
                        return nil
                    }) {
                        // Everything already done.
                    }
                    else if process(provider, UtiHelper.typeAudio, { url in
                        return room.sendAudioFile(
                            localURL: url, mimeType: UtiHelper.mimeType(for: url),
                            localEcho: &event, completion: self.onCompletion)
                    }) {
                        // Everything already done.
                    }
                    else if process(provider, UtiHelper.typeMovie, { url in
                        return room.sendVideo(
                            localURL: url, thumbnail: UIImage.thumbnailForVideo(at: url),
                            localEcho: &event, completion: self.onCompletion)
                        
                    }) {
                        // Everything already done.
                    }
                    else if process(provider, UtiHelper.typeData, { url in
                        return room.sendFile(
                            localURL: url, mimeType: UtiHelper.mimeType(for: url),
                            localEcho: &event, completion: self.onCompletion)
                        
                    }) {
                        // Everything already done.
                    }
                        // URL needs to come after everything else, otherwise, a local
                        // file URL will be shared instead of the document file itself.
                    else if process(provider, UtiHelper.typeUrl, { url in
                        return room.sendTextMessage(
                            url.absoluteString,
                            formattedText: "<a href=\"\(url.absoluteString)\">\(url.absoluteString)</a>",
                            localEcho: &event, completion: self.onCompletion)
                        
                    }) {
                        // Everything already done.
                    }
                }
            }
            
        } else {
            extensionContext!.completeRequest(returningItems: nil)
        }
    }
    
    // MARK: Private Methods
    
    /**
     Boilerplate reducer.
     
     - Checks, if item conforms to the given UTI.
     - If so, increase `progress.totalUnitCount`.
     - Loads item.
     - Prints an error description, shows user an error and increases progress if error happened.
     - Calls callback, if no error and adds subprogress from `MXHTTPOperation` to main progress.
     
     - parameter provider: The `NSItemProvider` holding an item.
     - parameter uti: The UTI, the item should have to handle it.
     - parameter rawCallback: Callback, if no error happened during loading.
     - parameter data: The raw data from `NSItemProvider.loadItem`.
     - returns: true, if item is of correct UTI and will be handled.
     */
    private func process(_ provider: NSItemProvider, _ uti: String, rawCallback: @escaping (_ data: NSSecureCoding) -> MXHTTPOperation?) -> Bool {
        if provider.hasItemConformingToTypeIdentifier(uti) {
            progress.totalUnitCount += 1
            
            provider.loadItem(forTypeIdentifier: uti, options: nil) { data, error in
                if error == nil,
                    let data = data {
                    
                    let operation = rawCallback(data)
                    
                    if let operation = operation?.operation {
                        if #available(iOSApplicationExtension 11.0, *) {
                            self.progress.addChild(operation.progress, withPendingUnitCount: 1)
                        }
                    }
                }
                else {
                    print("[\(String(describing: type(of: self)))] error=\(error?.localizedDescription ?? "Empty data!"), data=\(data.debugDescription)")
                    
                    self.onCompletion(error: error?.localizedDescription ?? "Couldn't acquire item!".localize())
                }
                
            }
            
            return true
        }
        
        return false
    }
    
    /**
     Boilerplate reducer.
     
     - Checks, if item conforms to the given UTI.
     - If so, increase `progress.totalUnitCount`.
     - Loads item.
     - Prints an error description, shows user an error and increases progress if error happened.
     - Calls callback, if no error and adds subprogress from `MXHTTPOperation` to main progress.
     
     - parameter provider: The `NSItemProvider` holding an item.
     - parameter uti: The UTI, the item should have to handle it.
     - parameter callback: Callback, if no error happened during loading.
     - parameter url: The URL from `NSItemProvider.loadItem`, if the provided object was one.
     - returns: true, if item is of correct UTI and will be handled.
     */
    private func process(_ provider: NSItemProvider, _ uti: String, _ callback: @escaping (_ url: URL) -> MXHTTPOperation?) -> Bool {
        return process(provider, uti, rawCallback: { data in
            if let url = data as? URL {
                return callback(url)
            }
            
            print("[\(String(describing: type(of: self)))] error=data is no URL!, data=\(data)")
            
            self.onCompletion(error: "Couldn't acquire item!".localize())
            
            return nil
        })
    }
    
    /**
     Completion callback for MXRoom#send[...] operations.
     
     - Show error message, if any.
     - Increase progress count.
     - Leave ShareViewController, if done.
     - Delay leave by 5 seconds, when error happened.
     
     - parameter error: An optional localized error string to show to the user.
     */
    private func onCompletion(response: MXResponse<String?>) {
        onCompletion(error: response.error?.localizedDescription)
    }
    
    /**
     Callback for when done with handling an item.
     
     - Show error message, if any.
     - Increase progress count.
     - Leave ShareViewController, if done.
     - Delay leave by 5 seconds, when error happened.
     
     - parameter error: An optional localized error string to show to the user.
     */
    private func onCompletion(error: String? = nil) {
        if let error = error {
            DispatchQueue.main.async {
                self.hud.detailsLabel.text = error
            }
        }
        
        progress.completedUnitCount += 1
        
        if progress.completedUnitCount == progress.totalUnitCount {
            DispatchQueue.main.asyncAfter(deadline: .now() + (hud.detailsLabel.text != nil ? 5 : 0)) {
                self.extensionContext!.completeRequest(returningItems: nil)
            }
        }
    }
}
