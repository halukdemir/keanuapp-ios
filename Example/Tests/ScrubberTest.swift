//
//  ScrubberTest.swift
//  Keanu_Tests
//
//  Created by Benjamin Erhart on 12.03.19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import XCTest
import KeanuCore

class ScrubberTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFolders() {
//        for folder in Scrubber.folders {
//            print("[\(String(describing: type(of: self))) folder=\(folder)]")
//        }

        assert(contains(Scrubber.folders, "AppGroup"), "Contains AppGroup folder")
        assert(contains(Scrubber.folders, "Documents/"), "Contains Documents folder")
        assert(contains(Scrubber.folders, "Library/"), "Contains Library folder")
        assert(contains(Scrubber.folders, "tmp/"), "Contains tmp folder")
    }

    func testScrub() {
        XCTAssertNoThrow(Scrubber.scrub())
    }

    private func contains(_ urls: [URL], _ needle: String) -> Bool {
        for url in urls {
            if url.absoluteString.contains(needle) {
                return true
            }
        }

        return false
    }
}
