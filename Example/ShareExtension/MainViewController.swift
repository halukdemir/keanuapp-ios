//
//  MainViewController.swift
//  ShareExtension
//
//  Created by Benjamin Erhart on 19.02.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import UIKit
import KeanuCore
import KeanuExtension

@objc(MainViewController)
class MainViewController: BaseMainViewController {

    override func viewDidLoad() {
        KeanuCore.setUp(with: Config.self)

        // We use the provided localization by the Pod. Use your own bundle here,
        // (e.g. Bundle.main - the default arg), if you want to inject your own localization.
        KeanuCore.setUpLocalization(fileName: "Localizable", bundle: Bundle(for: BaseMainViewController.self))

        super.viewDidLoad()
    }
}
